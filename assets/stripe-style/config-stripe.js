$( document ).ready(function() {
    var stripe = Stripe('pk_test_l66rZX9bMJ6JeY62u2nWPoRo');
    monto = $('.monto_compra').val();
    var elements = stripe.elements();

    var card = elements.create('card', {
      style: {
        base: {
          color: "#32325D",
          fontWeight: 500,
          fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
          fontSize: "16px",
          fontSmoothing: "antialiased",

          "::placeholder": {
            color: "#CFD7DF"
          }
        },
        invalid: {
          color: "#E25950"
        }
      }
    });

    card.mount("#example4-card");

    var promise = stripe.createToken(card);
    promise.then(function(result) {
      // result.token is the card token.
    });  
});