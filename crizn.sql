-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-02-2018 a las 23:07:56
-- Versión del servidor: 5.5.8-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `crizn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente`
--

CREATE TABLE IF NOT EXISTS `agente` (
  `codAg` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` char(30) DEFAULT NULL,
  `Grado` char(10) DEFAULT NULL,
  `Credencial` char(10) DEFAULT NULL,
  PRIMARY KEY (`codAg`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `agente`
--

INSERT INTO `agente` (`codAg`, `Nombre`, `Grado`, `Credencial`) VALUES
(1, 'Plácido Emilio Maidana', 'Ayte 3ra.', '376009'),
(2, 'Jorge Abarrategui', 'Subayudant', '44545'),
(3, 'Pendientes', 'ayudante', '3333');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE IF NOT EXISTS `novedades` (
  `CodNovedad` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` char(255) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Hora` char(10) DEFAULT NULL,
  `Videoconferencia` char(10) DEFAULT NULL,
  `codAg` int(11) NOT NULL,
  PRIMARY KEY (`CodNovedad`),
  KEY `RefAgente4` (`codAg`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`CodNovedad`, `Descripcion`, `Fecha`, `Hora`, `Videoconferencia`, `codAg`) VALUES
(1, 'Reparacion de impresora seccion asistencia social', '2018-10-11', '10:00', 'Si', 1),
(9, 'Pedir el Pendrive a gonzales', '2018-01-12', '10', 'NO', 1),
(3, 'Problemas de conexion de red', '2018-01-19', '12:20', 'no', 1),
(4, 'Reinstalar Maquina de judiciales', '2018-01-12', '00', 'no', 3),
(5, 'Poner el cable de la conexion MPLS', '2018-01-12', '00', 'NO', 3),
(6, 'BUSCAR LOS PARTES', '2018-01-12', '00', 'NO', 3),
(10, 'Reconexión de impresora: Fondos de Terceros. Se cambio ubicación de impresora y se configuro de nuevo en la red.', '2018-01-16', '08:30', 'NO', 2),
(8, 'Armar licencias', '2018-01-12', '13', 'NO', 3),
(11, 'Conexión de la PC de Suministros al Swicht de la División Administrativa', '2018-01-16', '09:00', 'NO', 2),
(12, 'reconexión de la PC de Sociales al Swicht', '2018-01-17', '09:00', 'NO', 2),
(13, 'Instalación driver de impresora multiplicación HP en Sección Seguridad Electrónica', '2018-01-17', '09:30', 'NO', 2),
(14, 'Revisión y puesta en funcionamiento de conexiones de internet para la División Trabajo', '2018-01-17', '11:00', 'NO', 2),
(15, 'Re-instalación y recuperación de datos de la PC de Sección Seguridad Electrónica.', '2018-01-17', '08:30', 'NO', 1),
(16, 'Cambio de fuente de PC del Jefe de División Administrativa', '0008-01-17', '11:00', 'NO', 1),
(17, 'Cambio de fuente de PC del Jefe de Sección Tesorería de la División Administrativa', '0000-00-00', '11:30', 'NO', 1),
(18, 'Envío del PARTE DIARIO DIGITAL', '2018-01-17', '13:00', 'NO', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videoconferencias`
--

CREATE TABLE IF NOT EXISTS `videoconferencias` (
  `CodVideo` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date DEFAULT NULL,
  `Interno` char(30) DEFAULT NULL,
  `Juzgado` char(30) DEFAULT NULL,
  `HoraConexion` char(10) DEFAULT NULL,
  `HoraIngresoInterno` char(10) DEFAULT NULL,
  `HoraConexionJuzgado` char(10) DEFAULT NULL,
  `HoraFinalizacion` char(10) DEFAULT NULL,
  `Detalles` char(255) DEFAULT NULL,
  `CodNovedad` int(11) NOT NULL,
  PRIMARY KEY (`CodVideo`),
  KEY `RefNovedades3` (`CodNovedad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `videoconferencias`
--

INSERT INTO `videoconferencias` (`CodVideo`, `Fecha`, `Interno`, `Juzgado`, `HoraConexion`, `HoraIngresoInterno`, `HoraConexionJuzgado`, `HoraFinalizacion`, `Detalles`, `CodNovedad`) VALUES
(1, '2018-01-02', 'Almada Joaquin', 'TOC34', '10:02', '10:30', '10:15', '11:00', 'la videoconferencia se realizo sin novedades', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
