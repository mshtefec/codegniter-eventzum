<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories_shops extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Categories_shops_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'categories_shops/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'categories_shops/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'categories_shops/index.html';
            $config['first_url'] = base_url() . 'categories_shops/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Categories_shops_model->total_rows($q);
        $categories_shops = $this->Categories_shops_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'categories_shops_data' => $categories_shops,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('categories_shops/categories_shops_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Categories_shops_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
	    );
            $this->load->view('categories_shops/categories_shops_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categories_shops'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('categories_shops/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	);
        $this->load->view('categories_shops/categories_shops_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
	    );

            $this->Categories_shops_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('categories_shops'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Categories_shops_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('categories_shops/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
	    );
            $this->load->view('categories_shops/categories_shops_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categories_shops'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
	    );

            $this->Categories_shops_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('categories_shops'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Categories_shops_model->get_by_id($id);

        if ($row) {
            $this->Categories_shops_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('categories_shops'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('categories_shops'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "categories_shops.xls";
        $judul = "categories_shops";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");

	foreach ($this->Categories_shops_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=categories_shops.doc");

        $data = array(
            'categories_shops_data' => $this->Categories_shops_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('categories_shops/categories_shops_doc',$data);
    }

}

/* End of file Categories_shops.php */
/* Location: ./application/controllers/Categories_shops.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-26 16:03:14 */
/* http://harviacode.com */