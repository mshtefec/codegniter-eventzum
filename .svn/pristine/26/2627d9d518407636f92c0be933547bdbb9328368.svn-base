<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_reset extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_reset_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_reset/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_reset/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_reset/index.html';
            $config['first_url'] = base_url() . 'users_reset/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_reset_model->total_rows($q);
        $users_reset = $this->Users_reset_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_reset_data' => $users_reset,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_reset/users_reset_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_reset_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'token' => $row->token,
		'expiration' => $row->expiration,
	    );
            $this->load->view('users_reset/users_reset_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_reset'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_reset/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'token' => set_value('token'),
	    'expiration' => set_value('expiration'),
	);
        $this->load->view('users_reset/users_reset_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'token' => $this->input->post('token',TRUE),
		'expiration' => $this->input->post('expiration',TRUE),
	    );

            $this->Users_reset_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_reset'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_reset_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_reset/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'token' => set_value('token', $row->token),
		'expiration' => set_value('expiration', $row->expiration),
	    );
            $this->load->view('users_reset/users_reset_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_reset'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'token' => $this->input->post('token',TRUE),
		'expiration' => $this->input->post('expiration',TRUE),
	    );

            $this->Users_reset_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_reset'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_reset_model->get_by_id($id);

        if ($row) {
            $this->Users_reset_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_reset'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_reset'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('token', 'token', 'trim|required');
	$this->form_validation->set_rules('expiration', 'expiration', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "users_reset.xls";
        $judul = "users_reset";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User");
	xlsWriteLabel($tablehead, $kolomhead++, "Token");
	xlsWriteLabel($tablehead, $kolomhead++, "Expiration");

	foreach ($this->Users_reset_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user);
	    xlsWriteLabel($tablebody, $kolombody++, $data->token);
	    xlsWriteNumber($tablebody, $kolombody++, $data->expiration);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users_reset.doc");

        $data = array(
            'users_reset_data' => $this->Users_reset_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('users_reset/users_reset_doc',$data);
    }

}

/* End of file Users_reset.php */
/* Location: ./application/controllers/Users_reset.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-26 16:03:16 */
/* http://harviacode.com */