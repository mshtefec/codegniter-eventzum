<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empresas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Empresas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'empresas/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'empresas/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'empresas/index.html';
            $config['first_url'] = base_url() . 'empresas/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Empresas_model->total_rows($q);
        $empresas = $this->Empresas_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'empresas_data' => $empresas,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

         $this->load->view('global/header');
         $this->load->view('global/menulateral',$data);
         $this->load->view('empresas/empresas_list', $data);
         $this->load->view('global/footer');
    }

    public function read($id) 
    {
        $row = $this->Empresas_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nif' => $row->nif,
		'nombre' => $row->nombre,
		'direccion' => $row->direccion,
		'telefono' => $row->telefono,
		'correo' => $row->correo,
		'activo' => $row->activo,
		'logo' => $row->logo,
	    );

        $this->load->view('global/header');
        $this->load->view('global/menulateral',$data);
        $this->load->view('empresas/empresas_read', $data);
        $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('empresas'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('empresas/create_action'),
	    'id' => set_value('id'),
	    'nif' => set_value('nif'),
	    'nombre' => set_value('nombre'),
	    'direccion' => set_value('direccion'),
	    'telefono' => set_value('telefono'),
	    'correo' => set_value('correo'),
	    'activo' => set_value('activo'),
	    'logo' => set_value('logo'),
	);

        $this->load->view('global/header');
        $this->load->view('global/menulateral',$data);
        $this->load->view('empresas/empresas_form', $data);
        $this->load->view('global/footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'nombre' => $this->input->post('nombre',TRUE),
		'direccion' => $this->input->post('direccion',TRUE),
		'telefono' => $this->input->post('telefono',TRUE),
		'correo' => $this->input->post('correo',TRUE),
		'activo' => $this->input->post('activo',TRUE),
		'logo' => $this->input->post('logo',TRUE),
	    );

            $this->Empresas_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('empresas'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Empresas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('empresas/update_action'),
		'id' => set_value('id', $row->id),
		'nif' => set_value('nif', $row->nif),
		'nombre' => set_value('nombre', $row->nombre),
		'direccion' => set_value('direccion', $row->direccion),
		'telefono' => set_value('telefono', $row->telefono),
		'correo' => set_value('correo', $row->correo),
		'activo' => set_value('activo', $row->activo),
		'logo' => set_value('logo', $row->logo),
	    );

        $this->load->view('global/header');
        $this->load->view('global/menulateral',$data);
        $this->load->view('empresas/empresas_form', $data);
        $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('empresas'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'nombre' => $this->input->post('nombre',TRUE),
		'direccion' => $this->input->post('direccion',TRUE),
		'telefono' => $this->input->post('telefono',TRUE),
		'correo' => $this->input->post('correo',TRUE),
		'activo' => $this->input->post('activo',TRUE),
		'logo' => $this->input->post('logo',TRUE),
	    );

            $this->Empresas_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('empresas'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Empresas_model->get_by_id($id);

        if ($row) {
            $this->Empresas_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('empresas'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('empresas'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nif', 'nif', 'trim|required');
	$this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
	$this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
	$this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
	$this->form_validation->set_rules('correo', 'correo', 'trim|required');
	$this->form_validation->set_rules('activo', 'activo', 'trim|required');
	$this->form_validation->set_rules('logo', 'logo', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "empresas.xls";
        $judul = "empresas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
    	xlsWriteLabel($tablehead, $kolomhead++, "NIF");
    	xlsWriteLabel($tablehead, $kolomhead++, "Nombre");
    	xlsWriteLabel($tablehead, $kolomhead++, "Direccion");
    	xlsWriteLabel($tablehead, $kolomhead++, "Telefono");
    	xlsWriteLabel($tablehead, $kolomhead++, "Correo");
    	xlsWriteLabel($tablehead, $kolomhead++, "Activo");
    	xlsWriteLabel($tablehead, $kolomhead++, "Logo");

	foreach ($this->Empresas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
        xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nif);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nombre);
	    xlsWriteLabel($tablebody, $kolombody++, $data->direccion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->telefono);
	    xlsWriteLabel($tablebody, $kolombody++, $data->correo);
	    xlsWriteLabel($tablebody, $kolombody++, $data->activo);
	    xlsWriteLabel($tablebody, $kolombody++, $data->logo);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=empresas.doc");

        $data = array(
            'empresas_data' => $this->Empresas_model->get_all(),
            'start' => 0
        );


        $this->load->view('global/header');
        $this->load->view('global/menulateral',$data);
        $this->load->view('empresas/empresas_doc',$data); 
        $this->load->view('global/footer');
    }

}

/* End of file Empresas.php */
/* Location: ./application/controllers/Empresas.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-26 16:03:14 */
/* http://harviacode.com */