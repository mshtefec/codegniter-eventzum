<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zonasestadio extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Zonasestadio_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'zonasestadio/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'zonasestadio/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'zonasestadio/index.html';
            $config['first_url'] = base_url() . 'zonasestadio/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Zonasestadio_model->total_rows($q);
        $zonasestadio = $this->Zonasestadio_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'zonasestadio_data' => $zonasestadio,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('zonasestadio/zonasestadio_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Zonasestadio_model->get_by_id($id);
        if ($row) {
            $data = array(
		'idZonasEstadio' => $row->idZonasEstadio,
		'ZonasEstadioDescripcion' => $row->ZonasEstadioDescripcion,
		'Capacidad' => $row->Capacidad,
		'AsientoInicial' => $row->AsientoInicial,
		'AsientoFinal' => $row->AsientoFinal,
		'Precio' => $row->Precio,
		'Estadio_idEstadio' => $row->Estadio_idEstadio,
	    );
            $this->load->view('zonasestadio/zonasestadio_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zonasestadio'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('zonasestadio/create_action'),
	    'idZonasEstadio' => set_value('idZonasEstadio'),
	    'ZonasEstadioDescripcion' => set_value('ZonasEstadioDescripcion'),
	    'Capacidad' => set_value('Capacidad'),
	    'AsientoInicial' => set_value('AsientoInicial'),
	    'AsientoFinal' => set_value('AsientoFinal'),
	    'Precio' => set_value('Precio'),
	    'Estadio_idEstadio' => set_value('Estadio_idEstadio'),
	);
        $this->load->view('zonasestadio/zonasestadio_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ZonasEstadioDescripcion' => $this->input->post('ZonasEstadioDescripcion',TRUE),
		'Capacidad' => $this->input->post('Capacidad',TRUE),
		'AsientoInicial' => $this->input->post('AsientoInicial',TRUE),
		'AsientoFinal' => $this->input->post('AsientoFinal',TRUE),
		'Precio' => $this->input->post('Precio',TRUE),
		'Estadio_idEstadio' => $this->input->post('Estadio_idEstadio',TRUE),
	    );

            $this->Zonasestadio_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('zonasestadio'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Zonasestadio_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('zonasestadio/update_action'),
		'idZonasEstadio' => set_value('idZonasEstadio', $row->idZonasEstadio),
		'ZonasEstadioDescripcion' => set_value('ZonasEstadioDescripcion', $row->ZonasEstadioDescripcion),
		'Capacidad' => set_value('Capacidad', $row->Capacidad),
		'AsientoInicial' => set_value('AsientoInicial', $row->AsientoInicial),
		'AsientoFinal' => set_value('AsientoFinal', $row->AsientoFinal),
		'Precio' => set_value('Precio', $row->Precio),
		'Estadio_idEstadio' => set_value('Estadio_idEstadio', $row->Estadio_idEstadio),
	    );
            $this->load->view('zonasestadio/zonasestadio_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zonasestadio'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('idZonasEstadio', TRUE));
        } else {
            $data = array(
		'ZonasEstadioDescripcion' => $this->input->post('ZonasEstadioDescripcion',TRUE),
		'Capacidad' => $this->input->post('Capacidad',TRUE),
		'AsientoInicial' => $this->input->post('AsientoInicial',TRUE),
		'AsientoFinal' => $this->input->post('AsientoFinal',TRUE),
		'Precio' => $this->input->post('Precio',TRUE),
		'Estadio_idEstadio' => $this->input->post('Estadio_idEstadio',TRUE),
	    );

            $this->Zonasestadio_model->update($this->input->post('idZonasEstadio', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('zonasestadio'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Zonasestadio_model->get_by_id($id);

        if ($row) {
            $this->Zonasestadio_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('zonasestadio'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zonasestadio'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ZonasEstadioDescripcion', 'zonasestadiodescripcion', 'trim|required');
	$this->form_validation->set_rules('Capacidad', 'capacidad', 'trim|required');
	$this->form_validation->set_rules('AsientoInicial', 'asientoinicial', 'trim|required');
	$this->form_validation->set_rules('AsientoFinal', 'asientofinal', 'trim|required');
	$this->form_validation->set_rules('Precio', 'precio', 'trim|required|numeric');
	$this->form_validation->set_rules('Estadio_idEstadio', 'estadio idestadio', 'trim|required');

	$this->form_validation->set_rules('idZonasEstadio', 'idZonasEstadio', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "zonasestadio.xls";
        $judul = "zonasestadio";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "ZonasEstadioDescripcion");
	xlsWriteLabel($tablehead, $kolomhead++, "Capacidad");
	xlsWriteLabel($tablehead, $kolomhead++, "AsientoInicial");
	xlsWriteLabel($tablehead, $kolomhead++, "AsientoFinal");
	xlsWriteLabel($tablehead, $kolomhead++, "Precio");
	xlsWriteLabel($tablehead, $kolomhead++, "Estadio IdEstadio");

	foreach ($this->Zonasestadio_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ZonasEstadioDescripcion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Capacidad);
	    xlsWriteNumber($tablebody, $kolombody++, $data->AsientoInicial);
	    xlsWriteNumber($tablebody, $kolombody++, $data->AsientoFinal);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Precio);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Estadio_idEstadio);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=zonasestadio.doc");

        $data = array(
            'zonasestadio_data' => $this->Zonasestadio_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('zonasestadio/zonasestadio_doc',$data);
    }

}

/* End of file Zonasestadio.php */
/* Location: ./application/controllers/Zonasestadio.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-03-16 20:43:29 */
/* http://harviacode.com */