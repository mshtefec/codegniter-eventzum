<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CampoVirtualController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->load->model('evento');
    }

    public function index() {

      

        /* $data = array(
            'events_data' => $eventos,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ); */
        $data = array(
            'time' => 1
        );
        // $this->twig->display('front/campo-virtual/index');
        $this->twig->display('front/campo-virtual/index', array( 
            'time' => 1
        ));
    }
}