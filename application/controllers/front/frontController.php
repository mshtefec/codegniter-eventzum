<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FrontController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->load->model('evento');
    }

    public function home() {

        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'eventos/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'eventos/index.html?q='. urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'eventos/index.html';
            $config['first_url'] = base_url() . 'eventos/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->evento->total_rows($q);
        $eventos = $this->evento->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_data' => $eventos,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        $this->twig->display('front/index', array( 
            'data' => $data,
            'events' => $eventos
        ));
    }
}