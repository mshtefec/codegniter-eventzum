<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ConfigController extends CI_Controller {

    public $isNew = false;
    
    function __construct() {
        parent::__construct();

        //require_once('application/vendor/stripe/stripe-php/init.php');
        
        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');
        

        $this->twig->addGlobal('session', $this->session); 

        $this->load->model('usuario');
        $this->load->model('perfil');

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);

        if ( !$this->session->userdata('isLogin')) {
            redirect(site_url('login'));
        }

        //$this->output->enable_profiler(TRUE);
    }

    public function index() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $perfil = $this->perfil->findByIdUser($usuario->id);
        
        if ($perfil) {
            $this->perfil_editar();
        } else {
            $this->perfil_nuevo();
        }
        
    }

    public function perfil_nuevo() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);

        $data = array(
            'action' => base_url('front/configController/perfil_nuevo_action'),
            'id' => set_value('id'),
            'usuario_id' => set_value('usuario_id'),
            'nombre' => set_value('nombre'),
            'apellido' => set_value('apellido'),
            'pais'=> set_value('pais'),
            'telefono' => set_value('telefono'),
        );

        $this->twig->display('front/config/perfil', array( 
            'data' => $data
        ));
        
    }
    public function perfil_nuevo_action() {
        
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('apellido', 'apellido', 'trim|required');
        $this->form_validation->set_rules('pais', 'pais', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        if ($this->form_validation->run() == FALSE) {
            $this->perfil_nuevo();
        } else {

            $email = $this->session->userdata('email');
            $usuario = $this->usuario->findByEmail($email);

            $data = array(
                'usuario_id' => $usuario->id,
                'nombre' => $this->input->post('nombre',TRUE),
                'apellido' => $this->input->post('apellido',TRUE),
                'pais' => $this->input->post('pais',TRUE),
                'telefono' => $this->input->post('telefono',TRUE),
            );

            $this->perfil->insert($data);
            
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('configuracion'));
        }
    }

    public function perfil_editar() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $perfil = $this->perfil->findByIdUser($usuario->id);

        $data = array(
            'action' => base_url('front/configController/perfil_editar_action'),
            'id' => set_value('id', $perfil->id),
            'usuario_id' => set_value('usuario_id', $usuario->id),
            'nombre' => set_value('nombre', $perfil->nombre),
            'apellido' => set_value('apellido', $perfil->apellido),
            'pais'=> set_value('pais', $perfil->pais),
            'telefono' => set_value('telefono', $perfil->telefono),
        );

        $this->twig->display('front/config/perfil', array( 
            'data' => $data
        ));
        
    }

    public function perfil_editar_action() {
        
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('apellido', 'apellido', 'trim|required');
        $this->form_validation->set_rules('pais', 'pais', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        if ($this->form_validation->run() == FALSE) {
            $this->perfil_editar();
        } else {

            $email = $this->session->userdata('email');
            $usuario = $this->usuario->findByEmail($email);
            $perfil = $this->perfil->findByIdUser($usuario->id);

            $data = array(
                'usuario_id' => $usuario->id,
                'nombre' => $this->input->post('nombre',TRUE),
                'apellido' => $this->input->post('apellido',TRUE),
                'pais' => $this->input->post('pais',TRUE),
                'telefono' => $this->input->post('telefono',TRUE),
            );

            $this->perfil->update($perfil->id, $data);
            
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('configuracion'));
        }
    }

    public function avatar() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $perfil = $this->perfil->findByIdUser($usuario->id);

        if ($perfil) {

            $data = array(
                'action' => base_url('front/configController/avatar_action'),
                'id' => set_value('id', $perfil->id),
                'avatar_url' => set_value('avatar_url'),
                'avatar_file' => set_value('avatar_file'),
                'perfil_url' => $perfil->avatar_url,
            );

            $this->twig->display('front/config/avatar', array( 
                'data' => $data
            ));
        } else {
            $this->session->set_flashdata('message', 'Debe cargar primero sus datos personales!');

            redirect(site_url('configuracion'));
        }
    }

    public function avatar_action() {

        $this->form_validation->set_rules('id', 'id', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->avatar();
        } else {

            $email = $this->session->userdata('email');
            $usuario = $this->usuario->findByEmail($email);
            $perfil = $this->perfil->findByIdUser($usuario->id);

            $extension = pathinfo(strtolower($_FILES['avatar_url']['name']), PATHINFO_EXTENSION);
            $filename = "perfil_" . $perfil->id . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/config/perfiles/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('avatar_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'avatar_url' => $targetFile,
                'avatar_file' => $filename,
            );

            $this->perfil->update($this->input->post('id', TRUE), $data);
            
            
            redirect(site_url('configuracion/avatar'));
        }
    }

    public function cuenta() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);

        if ($usuario) {
            $data = array(
                'action' => base_url('front/configController/cuenta_action'),
                'id' => set_value('id', $usuario->id),
                'username' => set_value('username', $usuario->username),
                'email' => set_value('email', $usuario->email),
            );

            $this->twig->display('front/config/cuenta', array( 
                'data' => $data
            ));
        }
    }

    public function cuenta_action() {

        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->cuenta();
        } else {
            $data = array(
                'username' => $this->input->post('username',TRUE),
                'email' => $this->input->post('email',TRUE),
            );

            $this->usuario->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('configuracion/cuenta'));
        }
    }

    public function acceso() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);

        if ($usuario) {
            $data = array(
                'action' => base_url('front/configController/acceso_action'),
                'id' => set_value('id', $usuario->id),
                'password' => set_value('password'),
                'password_nuevo' => set_value('password_nuevo'),
                'password_repetir' => set_value('password_repetir'),
            );

            $this->twig->display('front/config/acceso', array( 
                'data' => $data
            ));
        }
    }

    public function acceso_action() {

        $this->form_validation->set_rules('password', 'username', 'trim|required');
        $this->form_validation->set_rules('password_nuevo', 'email', 'trim|required');
        $this->form_validation->set_rules('password_repetir', 'email', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->acceso();
        } else {

            $password = $this->input->post('password',TRUE);
            $password = (md5($password) == md5($usuario->password)) ? $this->input->post('password_nuevo',TRUE) : null;
            $password = (md5($password) == md5($this->input->post('password_repetir',TRUE))) ? $this->input->post('password_nuevo',TRUE) : null;

            if ($password != null) {
                $data = array(
                    'password' => md5($password),
                );

                $this->usuario->update($this->input->post('id', TRUE), $data);

                $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            } else {
                $this->session->set_flashdata('message', 'Puede haber ingresado mal sus datos');
            }

            redirect(site_url('configuracion/acceso'));
        }
    }

    public function notificaciones() {

        $this->twig->display('front/config/notificaciones');
    }

    public function suscripcion() {

        $this->twig->display('front/config/suscripcion');
    }

    public function pagos() {

        $this->twig->display('front/config/pagos');
    }

    public function metodo_pago() {

        $this->twig->display('front/config/metodo_pago');
    }

    public function stripe_card() {

        $this->twig->display('front/config/stripe_card');
    }

}