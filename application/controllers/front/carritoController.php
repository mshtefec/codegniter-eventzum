<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CarritoController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->twig->addGlobal('session', $this->session); 
        
        $this->load->model('usuario');
        $this->load->model('evento');
        $this->load->model('producto');
        $this->load->model('sucursal');
        $this->load->model('carrito');
        $this->load->model('carritoEvento');
        $this->load->model('carritoProducto');
        $this->load->model('eventoZona');
        $this->load->model('asiento');

        //$this->output->enable_profiler(TRUE);
    }

    public function index() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $evento = $this->evento->findById($carrito->evento_id);
        $eventoZonas = $this->eventoZona->findAllByIdEvento($evento->id);
        
        $stock = array();
        foreach ($eventoZonas as $zona) {
            $asientos = $this->asiento->findAllDisponiblesByIdEventoZona($zona->id);
            $temp = [
                'lugar' => $zona->Descripcion,
                'precio' => $zona->Precio,
                'stock' => count($asientos),
            ];

            $stock[] = $temp;
        }

        $this->twig->display('front/carrito/index', array( 
            'carrito' => $carrito,
            'evento' => $evento,
            'eventoZonas' => $eventoZonas,
            'stock' => $stock,
        ));
    }

    public function agregar() {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);

        $data = array(
            'usuario_id' => $usuario->id, 
            'evento_id' => $this->input->post('evento_id',TRUE),
            'total' => 0,
        );

        $this->carrito->update($carrito->id, $data);

        $this->session->set_flashdata('message', 'Se agrego evento al carrito');

        redirect(site_url('carrito'));
    }

    public function tickets_action() {
        
        $this->form_validation->set_rules('total_carrito', 'total_carrito', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $email = $this->session->userdata('email');
            $usuario = $this->usuario->findByEmail($email);
            $carrito = $this->carrito->findByIdUser($usuario->id);

            $total = $this->input->post('total_carrito',TRUE);
            
            $data = array(
                'total' => $total,
            );
            
            $this->carrito->update($carrito->id, $data);

            redirect(site_url('carrito/compra'));
        }
    }

    public function carrito_compra() {
        
        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $evento = $this->evento->findById($carrito->evento_id);
        
        $this->twig->display('front/carrito/compra', array( 
            'carrito' => $carrito,
            'usuario' => $usuario,
            'evento' => $evento
        ));
    }

    public function check()
	{
        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $evento = $this->evento->findById($carrito->evento_id);

		//check stripe token si no esta vacio
		if(!empty($_POST['stripeToken']))
		{
			//obtengo el token, informacion de tarjeta y usuario
			$token  = $_POST['stripeToken'];
			$name = $_POST['name'];
			$email = $_POST['email'];
			$card_num = $_POST['card_num'];
			$card_cvc = $_POST['cvc'];
			$card_exp_month = $_POST['exp_month'];
            $card_exp_year = $_POST['exp_year'];
            $total = $_POST['total'];
			
			//incluyo Stripe PHP library
			require_once APPPATH."third_party/stripe/init.php";
			
			//seteo api key de stripe
			$stripe = array(
			  "secret_key"      => "sk_test_UzPtZQhAEh9GMSwXeF4Vqy0r",
			  "publishable_key" => "pk_test_l66rZX9bMJ6JeY62u2nWPoRo"
			);
			
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			
			//creo el cliente en stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $email,
				'source'  => $token
			));
			
			//item informacion
			$itemName = "Stripe Test";
			$itemNumber = "EVE_" . $evento->id;
            //$itemPrice = 2000;
            $itemPrice = intval(strval($total*100)); // convierto valores de double a entero y multiplico por 100 para mantener los decimales, la api solo admite integer
			$currency = "eur";
			$orderID = "SKA92712382139";
			
			//creo un charge (pago) de creditos por la compra
			$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemNumber,
				'metadata' => array(
					'evento_id' => $itemNumber
				)
			));
			
			//serializo el detalle de los pagos
			$chargeJson = $charge->jsonSerialize();
			//chequeo si el pago esta correcto y se realizo bien
			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
			{
				//detalles de la orden 
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");
			
				
				//voy a ingresar una orden en la bd, nuestra bd local
				$dataDB = array(
					'name' => $name,
					'email' => $email, 
					'card_num' => $card_num, 
					'card_cvc' => $card_cvc, 
					'card_exp_month' => $card_exp_month, 
					'card_exp_year' => $card_exp_year, 
					'item_name' => $itemName, 
					'item_number' => $itemNumber, 
					'item_price' => $itemPrice, 
					'item_price_currency' => $currency, 
					'paid_amount' => $amount, 
					'paid_amount_currency' => $currency, 
					'txn_id' => $balance_transaction, 
					'payment_status' => $status,
					'created' => $date,
					'modified' => $date
				);
				if ($this->db->insert('orders', $dataDB)) {
					if($this->db->insert_id() && $status == 'succeeded'){
						$data['insertID'] = $this->db->insert_id();
						$this->load->view('payment_success', $data);
					}else{
						echo "Transaction has been failed";
					}
				}
				else
				{
					echo "not inserted. Transaction has been failed";
				}
			}
			else
			{
				echo "Invalid Token";
				$statusMsg = "";
			}
		}
	}

    public function quitar_carrito($id) {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $row = $this->carritoEvento->findByIdEventoAndCarrito($id, $carrito->id);
        
        if ($row) {
            $this->carritoEvento->delete($row->id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('carrito'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('carrito'));
        }
    }

    

    public function carrito_sucursales() {
        
        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $carritoEventos = $this->carritoEvento->findAllByIdCarrito($carrito->id);

        $sucursales = array();
        foreach ($carritoEventos as $evento) {
            $sucursal_object = $this->sucursal->findSucursalesByIdEvento($evento->evento_id);
            array_push($sucursales, $sucursal_object);
        }

        $this->twig->display('front/carrito/sucursales', array( 
            'carrito' => $carrito,
            'sucursales_array' => $sucursales,
        ));
    }

    public function carrito_productos() {
        
        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $carritoProductos = $this->carritoProducto->findAllByIdCarrito($carrito->id);
        $sucursal_id = $this->input->post('sucursal_id',TRUE);
        $productos = $this->producto->findAllByIdSucursal($sucursal_id);

        $mis_productos = array();
        foreach ($carritoProductos as $producto) {
            array_push($mis_productos, $producto->producto_id);
        }

        $this->twig->display('front/carrito/productos', array( 
            'carrito' => $carrito,
            'productos' => $productos,
            'mis_productos' => $mis_productos,
        ));
    }

    public function agregar_producto() {
        
        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);

        $data = array(
            'carrito_id' => $carrito->id, 
            'producto_id' => $this->input->post('producto_id',TRUE),
        );

        $this->carritoProducto->insert($data);

        $this->session->set_flashdata('message', 'Se agrego producto al carrito');

        redirect(site_url('carrito/compra-productos'));
    }

    public function quitar_producto_carrito($id) {

        $email = $this->session->userdata('email');
        $usuario = $this->usuario->findByEmail($email);
        $carrito = $this->carrito->findByIdUser($usuario->id);
        $row = $this->carritoProducto->findByIdProductoAndCarrito($id, $carrito->id);
        
        if ($row) {
            $this->carritoProducto->delete($row->id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('carrito/compra-productos'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('carrito/compra-productos'));
        }
    }
}