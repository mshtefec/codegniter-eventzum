<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class IndexController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('twig');
        $this->load->library('session');
        
        $this->twig->addGlobal('session', $this->session); 
        
        $this->load->model('evento');
    }

    public function index() {

        $eventos = $this->evento->findAll();

        $this->twig->display('front/index', array( 
            'eventos' => $eventos
        ));
    }
}