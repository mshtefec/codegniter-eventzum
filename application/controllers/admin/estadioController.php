<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class EstadioController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('usuario');
        $this->load->model('estadio');
        $this->load->model('zonaEstadio');
        $this->twig->addGlobal('session', $this->session);
       // $this->output->enable_profiler(TRUE);
        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));
        }
    }
    public function index() {

        $entities = $this->estadio->findAll();
        $this->twig->display('admin/estadios/index', array(
            'entities' => $entities,
        ));
    }
    public function nuevo() {
        $data = array(
            'action' => base_url('admin/estadioController/nuevo_action'),
            'buton' =>  'create' ,
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'latitud' => set_value('latitud'),
            'longitud' => set_value('longitud'),
            'plano_url' => set_value('plano_url'),
            'plano_file' => set_value('plano_file'),
            'informacion' => set_value('informacion'),
        );
        $this->twig->display('admin/estadios/form', array(
            'data' => $data,
            'edit' => false
        ));
    }
    public function nuevo_action() {
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('latitud', 'latitud', 'trim|required');
        $this->form_validation->set_rules('longitud', 'longitud', 'trim|required');
        $this->form_validation->set_rules('informacion', 'informacion', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'latitud' => $this->input->post('latitud',TRUE),
                'longitud' => $this->input->post('longitud',TRUE),
                'plano_url' => $targetFile,
                'plano_file' => $filename,
                'informacion' => $this->input->post('informacion',TRUE),
            );
            $this->estadio->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            $idEstadio=$this->db->insert_id();
            $extension = pathinfo(strtolower($_FILES['plano_url']['name']), PATHINFO_EXTENSION);
            $filename = "plano_" . $idEstadio . '.' . $extension;
            $config = array(
                'upload_path'  => 'uploads/estadios/planos/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('plano_url');
            $this->upload->data();
            $targetFile = $config['upload_path'] . $filename;
          //  $idEstadio=$this->db->insert_id();
          // SE CARGAN LAS ZONAS ESTADIO
            $zonaL=$_POST['Zona'];
            $Fila=$_POST['Fila'];
            $asientoInicial=$_POST['AsientoInicial'];
            $asientoFinal=$_POST['AsientoFinal'];
           $todo=array();
         foreach($zonaL as $k=>$val) {
             $todo[]=array('ZonasEstadioDescripcion'=>$zonaL[$k],'Fila'=>$Fila[$k],'AsientoInicial'=>$asientoInicial[$k],'AsientoFinal'=>$asientoFinal[$k],'Estadio_idEstadio'=>$idEstadio);
             }
           $this->db->insert_batch('zonasestadio',$todo);
            redirect(site_url('admin/estadios'));
        }
    }
    public function editar($id) {

        $estadio = $this->estadio->findById($id);
        $zonas_estadio = $this->zonaEstadio->findByEstadioId($id);
       // var_dump($zonas_estadio);
        if ($estadio) {
            $data = array(
                'action' => base_url('admin/estadioController/editar_action'),
                'id' => set_value('id', $estadio->id),
                'nombre' => set_value('nombre', $estadio->nombre),
                'latitud' => set_value('latitud', $estadio->latitud),
                'longitud' => set_value('longitud', $estadio->longitud),
                'informacion' => set_value('informacion', $estadio->informacion),
                'plano_url' => set_value('plano_url', $estadio->plano_url),
                'plano_file' => set_value('plano_file', $estadio->plano_file),
                
                'zonas_estadio' => $zonas_estadio,
            );

            // echo '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>';
            // var_dump($data);
            // echo(site_url($data['plano_url']));
           $this->twig->display('admin/estadios/form', array(
                'data' => $data,
                'edit' => true,
                'estadio_url' => $estadio->plano_url
            ));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/estadios'));
        }
    }
    public function editar_action() {
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('latitud', 'latitud', 'trim|required');
        $this->form_validation->set_rules('longitud', 'longitud', 'trim|required');
        $this->form_validation->set_rules('informacion', 'informacion', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {

           echo "Plano url ". $this->input->post('plano_url2',TRUE);
           echo  "Plano file". $this->input->post('plano_file2',TRUE);



            if($_FILES['plano_url']['name']!="")
            {//echo ("Use upload".$_FILES['plano_url']['name']);
            
                $extension = pathinfo(strtolower($_FILES['plano_url']['name']), PATHINFO_EXTENSION);
                $filename = "plano_" . $this->input->post('id',TRUE) . '.' . $extension;
                $config = array(
                    'upload_path'  => 'uploads/estadios/planos/',
                    'allowed_types'=> 'gif|jpg|png',
                    'encrypt_name' => FALSE, // Optional, you can add more options as need
                    'overwrite' => TRUE,
                    'max_size' => '7000',
                    'file_name' => $filename
                );
                $this->load->library('upload', $config);
            
                if($this->upload->do_upload('plano_url')) { 
                    $this->upload->data();
                    $targetFile = $config['upload_path'] . $filename;
                $data = array(
                    'nombre' => $this->input->post('nombre',TRUE),
                    'latitud' => $this->input->post('latitud',TRUE),
                    'longitud' => $this->input->post('longitud',TRUE),
                    'plano_url' => $targetFile,
                    'plano_file' => $filename,
                    'informacion' => $this->input->post('informacion',TRUE),
                );
                
                
    
                } else { 
                   echo $this->upload->display_errors(); 
                }
            
            }
             else{
               // echo ("No use upload".$_FILES['plano_url']['name']);
               $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'latitud' => $this->input->post('latitud',TRUE),
                'longitud' => $this->input->post('longitud',TRUE),
                'plano_url' =>  $this->input->post('plano_url2',TRUE),
                'plano_file' => $this->input->post('plano_file2',TRUE),
                'informacion' => $this->input->post('informacion',TRUE),
            );
             }


            
            $idEstadio=$this->input->post('id', TRUE);
            ECHO("DELETE FROM ZONASESTADIO WHERE ESTADIO_IDESTADIO=".$idEstadio);
           $this->db->query("DELETE FROM ZONASESTADIO WHERE ESTADIO_IDESTADIO=".$idEstadio);
           if($_POST['Zona'])
           {
            $zonaL=$_POST['Zona'];
            $Fila=$_POST['Fila'];
            $AsientoInicial=$_POST['AsientoInicial'];
            $asientoFinal=$_POST['AsientoFinal'];
           $todo=array();
           foreach($zonaL as $k=>$val) {
             $todo[]=array('ZonasEstadioDescripcion'=>$zonaL[$k],'Fila'=>$Fila[$k],'AsientoInicial'=>$AsientoInicial[$k],'AsientoFinal'=>$asientoFinal[$k],'Estadio_idEstadio'=>$idEstadio);
             }
           $this->db->insert_batch('zonasestadio',$todo);
            }
            $this->estadio->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
           redirect(site_url('admin/estadios'));
        }
    }
    public function estadio_borrar($id) {
        $row = $this->estadio->findById($id);
        if ($row) {
            $this->estadio->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/estadios'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/estadios'));
        }
    }
}