<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SucursalController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');
        $this->load->model('tienda');
        $this->load->model('sucursal');
        $this->load->model('evento');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {
        
        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $entities = $this->sucursal->findAll();
        } else {
            $entities = $this->sucursal->findAllByIdUser($usuario_id);
        }

        $this->twig->display('admin/tiendas/sucursales/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $tiendas = $this->tienda->findAll();
            $eventos = $this->evento->findAll();
            $usuarios = $this->usuario->findAll();
        } else {
            $tiendas = $this->tienda->findAllByIdUser($usuario_id);
            $eventos = $this->evento->findAllByIdUser($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        $data = array(
            'action' => base_url('admin/sucursalController/nuevo_action'),
            'id' => set_value('id'),
            'tiendas' => $tiendas,
            'eventos' => $eventos,
            'usuarios' => $usuarios,
            'representante' => set_value('representante'),
            'direccion' => set_value('direccion'),
            'ciudad' => set_value('ciudad'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/tiendas/sucursales/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('tienda_id', 'tienda_id', 'trim|required');
        $this->form_validation->set_rules('representante', 'representante', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'ciudad', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {
            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'evento_id' => $this->input->post('evento_id',TRUE),
                'tienda_id' => $this->input->post('tienda_id',TRUE),
                'representante' => $this->input->post('representante',TRUE),
                'direccion' => $this->input->post('direccion',TRUE),
                'ciudad' => $this->input->post('ciudad',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->sucursal->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/tiendas/sucursales'));
        }
    }

    public function editar($id) {
        
        $sucursal = $this->sucursal->findById($id);
        
        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $tiendas = $this->tienda->findAll();
            $eventos = $this->evento->findAll();
            $usuarios = $this->usuario->findAll();
        } else {
            $tiendas = $this->tienda->findAllByIdUser($usuario_id);
            $eventos = $this->evento->findAllByIdUser($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        if ($sucursal) {
            $data = array(
                'action' => base_url('admin/sucursalController/editar_action'),
                'id' => set_value('id', $sucursal->id),
                'tiendas' => $tiendas,
                'eventos' => $eventos,
                'usuarios' => $usuarios,
                'representante' => set_value('representante', $sucursal->representante),
                'direccion' => set_value('direccion', $sucursal->direccion),
                'ciudad' => set_value('ciudad', $sucursal->ciudad),
                'activo' => set_value('activo', $sucursal->activo),
            );

            $this->twig->display('admin/tiendas/sucursales/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/tiendas/sucursales'));
        }
    }

    public function editar_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('tienda_id', 'tienda_id', 'trim|required');
        $this->form_validation->set_rules('evento_id', 'evento_id', 'trim|required');
        $this->form_validation->set_rules('representante', 'representante', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'ciudad', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'tienda_id' => $this->input->post('tienda_id',TRUE),
                'evento_id' => $this->input->post('evento_id',TRUE),
                'representante' => $this->input->post('representante',TRUE),
                'direccion' => $this->input->post('direccion',TRUE),
                'ciudad' => $this->input->post('ciudad',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->sucursal->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/tiendas/sucursales'));
        }
    }

    public function sucursal_borrar($id) {
        $row = $this->sucursal->findById($id);

        if ($row) {
            $this->sucursal->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/tiendas/sucursales'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/tiendas/sucursales'));
        }
    }

}