<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminController extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->load->library('session');

        $this->twig->addGlobal('session', $this->session); 

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));
        } else {
            if ($this->session->userdata('role') == 1) {
                redirect(site_url('login'));
            }
        }
    }

    public function admin() {

        $this->twig->display('admin/example');
    }
}