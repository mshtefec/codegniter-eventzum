<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class UsuarioController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');
        $this->load->model('role');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {
        
        $entities = $this->usuario->findAll();

        $this->twig->display('admin/usuarios/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $roles = $this->role->findAll();

        $data = array(
            'action' => base_url('admin/usuarioController/nuevo_action'),
            'id' => set_value('id'),
            'username' => set_value('username'),
            'email' => set_value('email'),
            'password' => set_value('password'),
            'roles' => $roles,
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/usuarios/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('role_id', 'role_id', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {
            $data = array(
                'username' => $this->input->post('username',TRUE),
                'email' => $this->input->post('email',TRUE),
                'password' => md5($this->input->post('password',TRUE)),
                'role_id' => $this->input->post('role_id',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->usuario->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/usuarios'));
        }
    }

    public function editar($id) {
        
        $usuario = $this->usuario->findById($id);
        $roles = $this->role->findAll();

        if ($usuario) {
            $data = array(
                'action' => base_url('admin/usuarioController/edit_action'),
                'id' => set_value('id', $usuario->id),
                'username' => set_value('username', $usuario->username),
                'email' => set_value('email', $usuario->email),
                'password' => null,
                'roles' => $roles,
                'activo' => set_value('activo', $usuario->activo),
            );

            $this->twig->display('admin/usuarios/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/usuarios/form'));
        }
    }

    public function edit_action() {

        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('role_id', 'role_id', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'username' => $this->input->post('username',TRUE),
                'email' => $this->input->post('email',TRUE),
                'password' => md5($this->input->post('password',TRUE)),
                'role_id' => $this->input->post('role_id',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->usuario->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/usuarios'));
        }
    }

    public function cambiarPass($id) {

        $row = $this->Usuario_model->get_by_id($id);
        
        $data = array(
            'action' => base_url('api/autenticacionController/cambiar_action'),
            'id' => set_value('id', $row->id),
            'password_original' => set_value('inputPasswordOriginal'),
            'password_nuevo' => set_value('inputPasswordNuevo'),
            'password_nuevo_repetir' => set_value('inputPasswordRepetir'),
        );

        $this->twig->display('admin/usuarios/cambiar', array( 
            'data' => $data
        ));
    }

    public function cambiar_action() {

        $this->form_validation->set_rules('inputPasswordOriginal', 'password', 'trim|required');
        $this->form_validation->set_rules('inputPasswordNuevo', 'password', 'trim|required');
        $this->form_validation->set_rules('inputPasswordRepetir', 'password', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->cambiarPass( $this->input->post('id', TRUE) );
        } else {

            $nuevoPass = $this->input->post('inputPasswordNuevo',TRUE);
            
            $data = array(
                'nombreUsuario' => set_value('inputNombreUsuario', $row->nombreUsuario),
                'email' => set_value('inputEmail', $row->email),
                'password' => md5($nuevoPass),
                'role' => set_value('inputRole', $row->role),
            );

            $this->Usuario_model->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado su contraseña correctamente');

            redirect(site_url('admin'));
        }
    }

    public function delete($id)
    {
        $row = $this->usuario->findById($id);

        if ($row) {
            $this->usuario->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

}