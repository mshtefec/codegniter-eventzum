<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PagosStripeController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');
        $this->load->model('pagosStripe');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $entities = $this->pagosStripe->findAll();
        } else {
            $entities = $this->pagosStripe->findAllByIdUser($usuario_id);
        }

        $this->twig->display('admin/pagosStripe/index', array(
            'entities' => $entities,
        ));
    }

}