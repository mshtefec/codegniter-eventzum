<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ProductoController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');

        $this->load->model('sucursal');
        
        $this->load->model('producto');
        $this->load->model('productoCategoria');
        $this->load->model('productoSubCategoria');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {
        
        $entities = $this->producto->findAll();

        $this->twig->display('admin/productos/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $sucursales = $this->sucursal->findAllByTienda();
        $categorias = $this->productoCategoria->findAll();
        $subcategorias = $this->productoSubCategoria->findAll();

        $data = array(
            'action' => base_url('admin/productoController/nuevo_action'),
            'id' => set_value('id'),
            'sucursales' => $sucursales,
            'categorias' => $categorias,
            'subcategorias' => $subcategorias,
            'nombre' => set_value('nombre'),
            'descripcion' => set_value('descripcion'),
            'precio' => set_value('precio'),
            'iva' => set_value('iva'),
            'stock' => set_value('stock'),
            'imagen_url' => set_value('imagen_url'),
            'imagen_file' => set_value('imagen_file'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/productos/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|required');
        $this->form_validation->set_rules('categoria_id', 'categoria_id', 'trim|required');
        $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('precio', 'precio', 'trim|required');
        $this->form_validation->set_rules('iva', 'iva', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {

            $extension = pathinfo(strtolower($_FILES['imagen_url']['name']), PATHINFO_EXTENSION);
            $filename = "producto_" . $this->input->post('nombre',TRUE) . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/productos/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('imagen_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'sucursal_id' => $this->input->post('sucursal_id',TRUE),
                'categoria_id' => $this->input->post('categoria_id',TRUE),
                'subcategoria_id' => $this->input->post('subcategoria_id',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'descripcion' => $this->input->post('descripcion',TRUE),
                'precio' => $this->input->post('precio',TRUE),
                'iva' => $this->input->post('iva',TRUE),
                'stock' => $this->input->post('stock',TRUE),
                'imagen_url' => $targetFile,
                'imagen_file' => $filename,
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->producto->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/productos'));
        }
    }

    public function editar($id) {
        
        $producto = $this->producto->findById($id);
        $sucursales = $this->sucursal->findAllByTienda();
        $categorias = $this->productoCategoria->findAll();
        $subcategorias = $this->productoSubCategoria->findAll();

        if ($producto) {
            $data = array(
                'action' => base_url('admin/productoController/editar_action'),
                'id' => set_value('id', $producto->id),
                'sucursales' => $sucursales,
                'categorias' => $categorias,
                'subcategorias' => $subcategorias,
                'nombre' => set_value('nombre', $producto->nombre),
                'descripcion' => set_value('descripcion', $producto->descripcion),
                'precio' => set_value('precio', $producto->precio),
                'iva' => set_value('iva', $producto->iva),
                'stock' => set_value('stock', $producto->stock),
                'imagen_url' => set_value('imagen_url', $producto->imagen_url),
                'imagen_file' => set_value('imagen_file', $producto->imagen_file),
                'activo' => set_value('activo', $producto->activo),
            );

            $this->twig->display('admin/productos/form', array( 
                'data' => $data,
                'edit' => true,
                'producto_url' => $producto->imagen_url
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos'));
        }
    }

    public function editar_action() {

        $this->form_validation->set_rules('sucursal_id', 'sucursal_id', 'trim|required');
        $this->form_validation->set_rules('categoria_id', 'categoria_id', 'trim|required');
        $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('descripcion', 'descripcion', 'trim|required');
        $this->form_validation->set_rules('precio', 'precio', 'trim|required');
        $this->form_validation->set_rules('iva', 'iva', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {

            $extension = pathinfo(strtolower($_FILES['imagen_url']['name']), PATHINFO_EXTENSION);
            $filename = "producto_" . $this->input->post('nombre',TRUE) . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/productos/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('imagen_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'sucursal_id' => $this->input->post('sucursal_id',TRUE),
                'categoria_id' => $this->input->post('categoria_id',TRUE),
                'subcategoria_id' => $this->input->post('subcategoria_id',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'descripcion' => $this->input->post('descripcion',TRUE),
                'precio' => $this->input->post('precio',TRUE),
                'iva' => $this->input->post('iva',TRUE),
                'stock' => $this->input->post('stock',TRUE),
                'imagen_url' => $targetFile,
                'imagen_file' => $filename,
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->producto->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/productos'));
        }
    }

    public function producto_borrar($id) {
        $row = $this->producto->findById($id);

        if ($row) {
            $this->producto->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/productos'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos'));
        }
    }

    public function categoria_index() {
        
        $entities = $this->productoCategoria->findAll();

        $this->twig->display('admin/productos/categorias/index', array(
            'entities' => $entities,
        ));
    }

    public function categoria_nuevo() {
        $data = array(
            'action' => base_url('admin/productoController/categoria_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/productos/categorias/form', array( 
            'data' => $data
        ));
    }

    public function categoria_nuevo_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->categoria_nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->productoCategoria->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/productos/categorias'));
        }
    }

    public function categoria_editar($id) {
        
        $categoria = $this->productoCategoria->findById($id);

        if ($categoria) {
            $data = array(
                'action' => base_url('admin/productoController/categoria_editar_action'),
                'id' => set_value('id', $categoria->id),
                'nombre' => set_value('nombre', $categoria->nombre),
                'activo' => set_value('activo', $categoria->activo),
            );

            $this->twig->display('admin/productos/categorias/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos/categorias'));
        }
    }

    public function categoria_editar_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->categoria_editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->productoCategoria->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/productos/categorias'));
        }
    }

    public function categoria_borrar($id) {
        $row = $this->productoCategoria->findById($id);

        if ($row) {
            $this->productoCategoria->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/productos/categorias'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos/categorias'));
        }
    }

    public function subcategoria_index() {
        
        $entities = $this->productoSubCategoria->findAll();

        $this->twig->display('admin/productos/subcategorias/index', array(
            'entities' => $entities,
        ));
    }

    public function subcategoria_nuevo() {
        $data = array(
            'action' => base_url('admin/productoController/subcategoria_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/productos/subcategorias/form', array( 
            'data' => $data
        ));
    }

    public function subcategoria_nuevo_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->subcategoria_nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->productoSubCategoria->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/productos/subcategorias'));
        }
    }

    public function subcategoria_editar($id) {
        
        $subcategoria = $this->productoSubCategoria->findById($id);

        if ($subcategoria) {
            $data = array(
                'action' => base_url('admin/productoController/subcategoria_editar_action'),
                'id' => set_value('id', $subcategoria->id),
                'nombre' => set_value('nombre', $subcategoria->nombre),
                'activo' => set_value('activo', $subcategoria->activo),
            );

            $this->twig->display('admin/productos/subcategorias/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos/subcategorias'));
        }
    }

    public function subcategoria_editar_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->subcategoria_editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->productoSubCategoria->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/productos/subcategorias'));
        }
    }

    public function subcategoria_borrar($id) {
        $row = $this->productoSubCategoria->findById($id);

        if ($row) {
            $this->productoSubCategoria->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/productos/subcategorias'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/productos/subcategorias'));
        }
    }

}