<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmpresaController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');
        $this->load->model('empresa');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $entities = $this->empresa->findAll();
        } else {
            $entities = $this->empresa->findAllByIdUser($usuario_id);
        }

        $this->twig->display('admin/empresas/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        $data = array(
            'action' => base_url('admin/empresaController/nuevo_action'),
            'id' => set_value('id'),
            'usuarios' => $usuarios,
            'nif' => set_value('nif'),
            'nombre' => set_value('nombre'),
            'direccion' => set_value('direccion'),
            'telefono' => set_value('telefono'),
            'correo' => set_value('correo'),
            'logo_url' => set_value('logo_url'),
            'logo_file' => set_value('logo_file'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/empresas/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('nif', 'nif', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_rules('correo', 'correo', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {

            $extension = pathinfo(strtolower($_FILES['logo_url']['name']), PATHINFO_EXTENSION);
            $filename = "logo_" . $this->input->post('usuario_id',TRUE) . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/empresas/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('logo_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'nif' => $this->input->post('nif',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'direccion' => $this->input->post('direccion',TRUE),
                'telefono' => $this->input->post('telefono',TRUE),
                'correo' => $this->input->post('correo',TRUE),
                'logo_url' => $targetFile,
                'logo_file' => $filename,
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->empresa->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/empresas'));
        }
    }

    public function editar($id) {
        
        $empresa = $this->empresa->findById($id);
        
        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        if ($empresa) {
            $data = array(
                'action' => base_url('admin/empresaController/editar_action'),
                'id' => set_value('id', $empresa->id),
                'usuarios' => $usuarios,
                'nif' => set_value('nif', $empresa->nif),
                'nombre' => set_value('nombre', $empresa->nombre),
                'direccion' => set_value('direccion', $empresa->direccion),
                'telefono' => set_value('telefono', $empresa->telefono),
                'correo' => set_value('correo', $empresa->correo),
                'logo_url' => set_value('logo_url', $empresa->logo_url),
                'logo_file' => set_value('logo_file', $empresa->logo_file),
                'activo' => $this->input->post('activo', $empresa->activo),
            );

            $this->twig->display('admin/empresas/form', array( 
                'data' => $data,
                'edit' => true,
                'empresa_url' => $empresa->logo_url
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/empresas'));
        }
    }

    public function editar_action() {

        $this->form_validation->set_rules('nif', 'nif', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_rules('correo', 'correo', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {

            $extension = pathinfo(strtolower($_FILES['logo_url']['name']), PATHINFO_EXTENSION);
            $filename = "logo_" . $this->input->post('usuario_id',TRUE) . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/empresas/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('logo_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'nif' => $this->input->post('nif',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'direccion' => $this->input->post('direccion',TRUE),
                'telefono' => $this->input->post('telefono',TRUE),
                'correo' => $this->input->post('correo',TRUE),
                'logo_url' => $targetFile,
                'logo_file' => $filename,
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->empresa->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/empresas'));
        }
    }

    public function empresa_borrar($id) {
        $row = $this->empresa->findById($id);

        if ($row) {
            $this->empresa->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/empresas'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/empresas'));
        }
    }

}