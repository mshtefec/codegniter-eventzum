<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EventoController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');

        $this->load->model('sucursal');
        
        $this->load->model('evento');
        $this->load->model('eventoCategoria');
        $this->load->model('eventoSubCategoria');
        $this->load->model('eventoParticipante');
        $this->load->model('estadio');
        $this->load->model('ZonaEstadio');
       
       

        $this->twig->addGlobal('session', $this->session);

        //$this->output->enable_profiler(TRUE);    

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }


    public function Preciozona($z,$id)
    {
       // var_dump($_POST['ZonaDesc']);
      
       $EventoZona = $this->db->query(" select * from eventozona where idEvento=\"".$id."\"")->result();
     //  var_dump($EventoZona);
        foreach($EventoZona as $k=>$val) {
            if($EventoZona[$k]->Descripcion==$z)
            {$data = array(
                'idEvZona' =>$EventoZona[$k]->id ,
                'Precio' =>$EventoZona[$k]->Precio ,);
                return $data ;
                      }
            
            
            }
      

    }
    public function SelectAsientos()
    {

      //  echo($_POST['zona']."asdfasdfasdfasdfasdfasdfasd".$_POST['id']);
       $Asientos = $this->db->query("select * from asientos where idEvento=\"".$_POST['id']."\" and zona=\"".$_POST['zona']."\"")->result();
  
         
      //var_dump($Asientos);
     print " <table class=\"table table-bordered\" style=\"margin-bottom: 10px\">\n";
       
     $f=1;
        foreach ($Asientos as $fila)
            {
                print " <tr>\n";
                print "	<td width=\"80px\">";
                // echo($f." == ".$fila->Asiento);
                //     if(  $fila==$fila->Asiento)
                 //    { 
                        print "<input type=\"checkbox\" name=\"".$fila->Asiento."\" value=\"true\" />          ";  
                    //  }else{ $f=$fila->Asiento;
                    //         echo "<br>";}

                   
				
            print " </td>   ";
            print "</tr>";
              
            }
          
          print "</table>";
  
         


    }

    public function asientos()
     {
        $ZonasList = $this->db->query("select * from eventozona where idEvento= ".$_POST['dire'])->result();
        $TablaZonas = $this->db->query('select * from ZonasEstadio where Estadio_idEstadio='.$_POST['dire'])->result(); //findByEstadioId($_POST['dire']);
        // $zonas = $this->db->query('select * from zonasestadio where Estadio_idEstadio='.$_POST['dire'])->result();

        $zonas = $this->db->query("select ZonasEstadioDescripcion from zonasestadio where Estadio_idEstadio=".$_POST['dire']." group by ZonasEstadioDescripcion")->result();

        $zest=json_encode($ZonasList);
        $json=json_encode($TablaZonas);
        //var_dump($ZonasList);
        // print "   <label for=\"varchar\">Zonas  </label>\n";
        // print "   <select name=\"zona_id\" id=\"zona_id\" class=\"form-control\"    onchange=\"SelectAsientos()\">\n";
        //      foreach($ZonasList as $k=>$ze  ){ 
        //      print "    <option value=\"".$ze->Descripcion."\">".$ze->Descripcion."</option>\n";
        //                           }
        //      print "    </select>";



        // echo $json;
         print "<textarea style=\"visibility:hidden\"  name=\"ListEventZonas\">".$zest."</textarea> ";
         print "<textarea style=\"visibility:hidden\"  name=\"TablaZonas\">".$json."</textarea> ";
         // var_dump($zonas);
          $start=0;
       
        print "  <div class=\"col-lg-6\">\n";
        print "    <div class=\"form-group\">\n";

        print " <table class=\"table table-bordered\" style=\"margin-bottom: 10px\">\n";
        print "            <tr>\n";
        print "                <th>No</th>\n";
        print "		<th>ZonasEstadioDescripcion</th>\n";        
        print "		<th>Precio</th>\n";
        print "            </tr>";
            foreach ($zonas as $zonasestadio)
            {

            print " <tr>\n";
            print "	<td width=\"80px\">".++$start ."</td> ";
			print "  <td>  <input readonly name=\"ZonaDesc[]\" value=\"".$zonasestadio->ZonasEstadioDescripcion."\"/> </td> ";			
            print " <td>  <input name=\"Precio[]\" /> </td>   ";
            print "</tr>";
              
            }
          
            print "</table>";

            print "    </div>\n";
            print "  </div>\n";

        
     }


    public function index() {
        
        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $entities = $this->evento->findAll();
        } else {
            $entities = $this->evento->findAllByIdUser($usuario_id);
        }

        $this->twig->display('admin/eventos/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $categorias = $this->eventoCategoria->findAll();
        $subcategorias = $this->eventoSubCategoria->findAll();
        $estadios = $this->estadio->findAll();
        


        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }
       //  var_dump($ZonasList);

        $data = array(
            'action' => base_url('admin/eventoController/nuevo_action'),
            'id' => set_value('id'),
            'usuarios' => $usuarios,
            'categorias' => $categorias,
            'subcategorias' => $subcategorias,
            'estadios'=>$estadios,
            'nombre' => set_value('nombre'),
            'direccion' => set_value('direccion'),
            'ciudad' => set_value('ciudad'),
            'codigo_postal' => set_value('codigo_postal'),
            'correo' => set_value('correo'),
            'telefono' => set_value('telefono'),
            'fecha' => set_value('fecha'),
            'hora' => set_value('hora'),
            'imagen_url' => set_value('imagen_url'),
            'imagen_file' => set_value('imagen_file'),
            'activo' => set_value('activo'),
         
        );


        $this->twig->display('admin/eventos/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('estadio_id', 'estadio_id', 'trim|required');
        $this->form_validation->set_rules('categoria_id', 'categoria_id', 'trim|required');
        $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'ciudad', 'trim|required');
        $this->form_validation->set_rules('codigo_postal', 'codigo_postal', 'trim|required');
        $this->form_validation->set_rules('correo', 'correo', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
        $this->form_validation->set_rules('hora', 'hora', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {

            $extension = pathinfo(strtolower($_FILES['imagen_url']['name']), PATHINFO_EXTENSION);
            $filename = "evento_" . $this->input->post('usuario_id',TRUE) . '.' . $extension;

            $config = array(
                'upload_path'  => 'uploads/eventos/',
                'allowed_types'=> 'gif|jpg|png',
                'encrypt_name' => FALSE, // Optional, you can add more options as need
                'overwrite' => TRUE,
                'max_size' => '7000',
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            $this->upload->do_upload('imagen_url');
            $this->upload->data();

            $targetFile = $config['upload_path'] . $filename;

            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),               
                'categoria_id' => $this->input->post('categoria_id',TRUE),
                'subcategoria_id' => $this->input->post('subcategoria_id',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'direccion' => $this->input->post('direccion',TRUE),
                'ciudad' => $this->input->post('ciudad',TRUE),
                'codigo_postal' => $this->input->post('codigo_postal',TRUE),
                'correo' => $this->input->post('correo',TRUE),
                'telefono' => $this->input->post('telefono',TRUE),
                'fecha' => $this->input->post('fecha',TRUE),
                'hora' => $this->input->post('hora',TRUE),
                'estadio' => $this->input->post('estadio_id',TRUE),
                'imagen_url' => $targetFile,
                'imagen_file' => $filename,
                'activo' => $this->input->post('activo',TRUE),
           
            );

            $this->evento->insert($data);

           $id=$this->db->insert_id();

           //Cargamos los precios de las zonas           
           foreach($_POST['ZonaDesc'] as $k=>$val) {
            $z[]=array('Descripcion'=>$_POST['ZonaDesc'][$k],'Precio'=>$_POST['Precio'][$k],'idEvento'=>$id);
            }
            $this->db->insert_batch('EventoZona',$z);

            $LISTA=json_decode($_POST['TablaZonas']);
             
           foreach($LISTA as $clave => $obj) {
                for($i=$obj->AsientoInicial; $i<$obj->AsientoFinal;$i++)
                {   
                    $resultPrecio=$this->Preciozona($obj->ZonasEstadioDescripcion , $id);
                      //var_dump( $resultPrecio); breack;
                    $asientos[] = array(          
                    'zona' => $obj->ZonasEstadioDescripcion,
                    'precio' => $resultPrecio['Precio'],
                    'Fila' => $obj->Fila,
                    'Asiento' => $i,
                    'Disponible' => true,
                    'idEvento' =>$id,
                    'eventozona_id'=>$resultPrecio['idEvZona'] ,
                     );

                   
                }
                
             }
       // echo "<br><br><br><br><br><br><br><br><br>";
        //var_dump($asientos);
      $this->db->insert_batch('asientos',$asientos);




            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/eventos'));
        }
    }

    public function editar($id) {
        
        $evento = $this->evento->findById($id);
        $categorias = $this->eventoCategoria->findAll();
        $subcategorias = $this->eventoSubCategoria->findAll();
        $estadios = $this->estadio->findAll();

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        if ($evento) {
           // var_dump($evento);
            //var_dump($this->session->userdata);
           

            $data = array(
                'action' => base_url('admin/eventoController/editar_action'),
                'id' => set_value('id', $evento->id),
                'usuarios' => $usuarios,
                'usuario'=> set_value('usuario', $usuario_id),
                'empresa' => set_value('empresa', $evento->Empresa),
                'estadios' =>$estadios,
                'categorias' => $categorias,
                'subcategorias' => $subcategorias,
                'nombre' => set_value('nombre', $evento->nombre),
                'direccion' => set_value('direccion', $evento->direccion),
                'ciudad' => set_value('ciudad', $evento->ciudad),
                'codigo_postal' => set_value('codigo_postal', $evento->codigo_postal),
                'correo' => set_value('correo', $evento->correo),
                'telefono' => set_value('telefono', $evento->telefono),
                'fecha' => set_value('fecha', $evento->fecha),
                'hora' => set_value('hora', $evento->hora),
                'estadio'=>  set_value('estadio', $evento->Estadio),
                'imagen_url' =>set_value('imagen_url', $evento->imagen_url),
                'imagen_file' =>set_value('imagen_file', $evento->imagen_file),
                'activo' => set_value('activo', $evento->activo),
              
            );

           // var_dump($data);
          // echo("Estadio de la base".$data['imagen_url']);

           $this->twig->display('admin/eventos/form', array( 
                'data' => $data,
                'edit' => true,
                'evento_url' => $evento->imagen_url
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos'));
        }
    }




    public function editar_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('estadio_id', 'estadio_id', 'trim|required');
        $this->form_validation->set_rules('categoria_id', 'categoria_id', 'trim|required');
        $this->form_validation->set_rules('subcategoria_id', 'subcategoria_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'ciudad', 'trim|required');
        $this->form_validation->set_rules('codigo_postal', 'codigo_postal', 'trim|required');
        $this->form_validation->set_rules('correo', 'correo', 'trim|required');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
        $this->form_validation->set_rules('hora', 'hora', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        $this->db->query("DELETE FROM Asientos   WHERE idEvento=".$this->input->post('id', TRUE));
        $this->db->query("DELETE FROM EventoZona WHERE idEvento=".$this->input->post('id', TRUE));
        
        //Cargamos los precios de las zonas   
        // var_dump($_POST);        
        foreach($_POST['ZonaDesc'] as $k=>$val) {
            $z[]=array('Descripcion'=>$_POST['ZonaDesc'][$k],'Precio'=>$_POST['Precio'][$k],'idEvento'=>$this->input->post('id', TRUE));
        }
        
        $this->db->insert_batch('EventoZona',$z);

        $LISTA=json_decode($_POST['TablaZonas']);
                 

        foreach($LISTA as $clave => $obj) {
        
            for($i=$obj->AsientoInicial; $i<$obj->AsientoFinal;$i++) {
               $resultPrecio=$this->Preciozona($obj->ZonasEstadioDescripcion, $this->input->post('id', TRUE) );
              // var_dump( $resultPrecio); breack;
                $asientos[] = array(          
                    'zona' => $obj->ZonasEstadioDescripcion,
                    'precio' => $resultPrecio['Precio']  ,
                    'Fila' => $obj->Fila,
                    'Asiento' => $i,
                    'Disponible' => true,
                    'idEvento' =>$this->input->post('id', TRUE),
                    'eventozona_id' =>$resultPrecio['idEvZona']  ,//FALTA AGREGAR EL CODIGO DE LA ZONA
                );               
            }            
        }

       // var_dump($asientos);
        
        $this->db->insert_batch('asientos',$asientos);

        if ($this->form_validation->run() == FALSE) {
            $this->editar($this->input->post('id', TRUE));
        } else {

            if($_FILES['imagen_url']['name']!="") {
                $extension = pathinfo(strtolower($_FILES['imagen_url']['name']), PATHINFO_EXTENSION);
                $filename = "evento_" . $this->input->post('usuario_id',TRUE) . '.' . $extension;    
                $config = array(
                    'upload_path'  => 'uploads/eventos/',
                    'allowed_types'=> 'gif|jpg|png',
                    'encrypt_name' => FALSE, // Optional, you can add more options as need
                    'overwrite' => TRUE,
                    'max_size' => '7000',
                    'file_name' => $filename
                );
    
                $this->load->library('upload', $config);    
                $this->upload->do_upload('imagen_url');
                $this->upload->data();    
                $targetFile = $config['upload_path'] . $filename;     
    
                $data = array(
                    'usuario_id' => $this->input->post('usuario_id',TRUE),
                    'categoria_id' => $this->input->post('categoria_id',TRUE),
                    'subcategoria_id' => $this->input->post('subcategoria_id',TRUE),
                    'nombre' => $this->input->post('nombre',TRUE),
                    //'descripcion' => $this->input->post('descripcion',TRUE),
                    'direccion' => $this->input->post('direccion',TRUE),
                    'ciudad' => $this->input->post('ciudad',TRUE),
                    'codigo_postal' => $this->input->post('codigo_postal',TRUE),
                    'correo' => $this->input->post('correo',TRUE),
                    'telefono' => $this->input->post('telefono',TRUE),
                    'fecha' => $this->input->post('fecha',TRUE),
                    'hora' => $this->input->post('hora',TRUE),
                    'empresa' => $this->input->post('empresa',TRUE),
                    //'Informacion' => $this->input->post('Informacion',TRUE),
                    'Estadio' => $this->input->post('estadio_id',TRUE),
                    //'Empresa' => $this->input->post('Empresa',TRUE),
                    'imagen_url' => $targetFile,
                    'imagen_file' =>$filename,
                    'activo' => $this->input->post('activo',TRUE),
                ); 
            } else {
                $data = array(
                    'usuario_id' => $this->input->post('usuario_id',TRUE),
                    'categoria_id' => $this->input->post('categoria_id',TRUE),
                    'subcategoria_id' => $this->input->post('subcategoria_id',TRUE),
                    'nombre' => $this->input->post('nombre',TRUE),
                    //'descripcion' => $this->input->post('descripcion',TRUE),
                    'direccion' => $this->input->post('direccion',TRUE),
                    'ciudad' => $this->input->post('ciudad',TRUE),
                    'codigo_postal' => $this->input->post('codigo_postal',TRUE),
                    'correo' => $this->input->post('correo',TRUE),
                    'telefono' => $this->input->post('telefono',TRUE),
                    'fecha' => $this->input->post('fecha',TRUE),
                    'hora' => $this->input->post('hora',TRUE),
                    'empresa' => $this->input->post('empresa',TRUE),
                    //'Informacion' => $this->input->post('Informacion',TRUE),
                    'Estadio' => $this->input->post('estadio_id',TRUE),
                    //'Empresa' => $this->input->post('Empresa',TRUE),
                    'imagen_url' =>$this->input->post('imagen_url2',TRUE),
                    'imagen_file' =>$this->input->post('imagen_file2',TRUE),
                    'activo' => $this->input->post('activo',TRUE),
                ); 
            }    

            $this->evento->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
           redirect(site_url('admin/eventos'));
        }
    }

    public function evento_borrar($id) {
        $row = $this->evento->findById($id);

        if ($row) {
            $this->db->query("DELETE FROM Asientos   WHERE idEvento=".$id);
            $this->db->query("DELETE FROM EventoZona WHERE idEvento=".$id);
            $this->evento->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/eventos'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos'));
        }
    }

    public function categoria_index() {
        
        $entities = $this->eventoCategoria->findAll();

        $this->twig->display('admin/eventos/categorias/index', array(
            'entities' => $entities,
        ));
    }

    public function categoria_nuevo() {
        $data = array(
            'action' => base_url('admin/eventoController/categoria_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/eventos/categorias/form', array( 
            'data' => $data
        ));
    }

    public function categoria_nuevo_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->categoria_nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoCategoria->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/eventos/categorias'));
        }
    }

    public function categoria_editar($id) {
        
        $categoria = $this->eventoCategoria->findById($id);

        if ($categoria) {
            $data = array(
                'action' => base_url('admin/eventoController/categoria_editar_action'),
                'id' => set_value('id', $categoria->id),
                'nombre' => set_value('nombre', $categoria->nombre),
                'activo' => set_value('activo', $categoria->activo),
            );

            $this->twig->display('admin/eventos/categorias/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/categorias'));
        }
    }

    public function categoria_editar_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->categoria_editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoCategoria->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/eventos/categorias'));
        }
    }

    public function categoria_borrar($id) {
        $row = $this->eventoCategoria->findById($id);

        if ($row) {
            $this->eventoCategoria->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/eventos/categorias'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/categorias'));
        }
    }

    public function subcategoria_index() {
        
        $entities = $this->eventoSubCategoria->findAll();

        $this->twig->display('admin/eventos/subcategorias/index', array(
            'entities' => $entities,
        ));
    }

    public function subcategoria_nuevo() {
        $data = array(
            'action' => base_url('admin/eventoController/subcategoria_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/eventos/subcategorias/form', array( 
            'data' => $data
        ));
    }

    public function subcategoria_nuevo_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->subcategoria_nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoSubCategoria->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/eventos/subcategorias'));
        }
    }

    public function subcategoria_editar($id) {
        
        $subcategoria = $this->eventoSubCategoria->findById($id);

        if ($subcategoria) {
            $data = array(
                'action' => base_url('admin/eventoController/subcategoria_editar_action'),
                'id' => set_value('id', $subcategoria->id),
                'nombre' => set_value('nombre', $subcategoria->nombre),
                'activo' => set_value('activo', $subcategoria->activo),
            );

            $this->twig->display('admin/eventos/subcategorias/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/subcategorias'));
        }
    }

    public function subcategoria_editar_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->subcategoria_editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoSubCategoria->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/eventos/subcategorias'));
        }
    }

    public function subcategoria_borrar($id) {
        $row = $this->eventoSubCategoria->findById($id);

        if ($row) {
            $this->eventoSubCategoria->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/eventos/subcategorias'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/subcategorias'));
        }
    }

    public function participante_index() {
        
        $entities = $this->eventoParticipante->findAll();

        $this->twig->display('admin/eventos/participantes/index', array(
            'entities' => $entities,
        ));
    }

    public function participante_nuevo() {
        $data = array(
            'action' => base_url('admin/eventoController/participante_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/eventos/participantes/form', array( 
            'data' => $data
        ));
    }

    public function participante_nuevo_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->participante_nuevo();
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoParticipante->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/eventos/participantes'));
        }
    }

    public function participante_editar($id) {
        
        $participante = $this->eventoParticipante->findById($id);

        if ($participante) {
            $data = array(
                'action' => base_url('admin/eventoController/participante_editar_action'),
                'id' => set_value('id', $participante->id),
                'nombre' => set_value('nombre', $participante->nombre),
                'activo' => set_value('activo', $participante->activo),
            );

            $this->twig->display('admin/eventos/participantes/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/participantes'));
        }
    }

    public function participante_editar_action() {

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->participante_editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'nombre' => $this->input->post('nombre',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->eventoParticipante->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/eventos/participantes'));
        }
    }

    public function participante_borrar($id) {
        $row = $this->eventoParticipante->findById($id);

        if ($row) {
            $this->eventoParticipante->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/eventos/participantes'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/eventos/participantes'));
        }
    }


   

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "evento.xls";
        $judul = "evento";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Usuario Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Categoria Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Subcategoria Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Nombre");
	xlsWriteLabel($tablehead, $kolomhead++, "Descripcion");
	xlsWriteLabel($tablehead, $kolomhead++, "Direccion");
	xlsWriteLabel($tablehead, $kolomhead++, "Ciudad");
	xlsWriteLabel($tablehead, $kolomhead++, "Codigo Postal");
	xlsWriteLabel($tablehead, $kolomhead++, "Correo");
	xlsWriteLabel($tablehead, $kolomhead++, "Telefono");
	xlsWriteLabel($tablehead, $kolomhead++, "Fecha");
	xlsWriteLabel($tablehead, $kolomhead++, "Hora");
	xlsWriteLabel($tablehead, $kolomhead++, "Informacion");
	xlsWriteLabel($tablehead, $kolomhead++, "Estadio");
	xlsWriteLabel($tablehead, $kolomhead++, "Empresa");
	xlsWriteLabel($tablehead, $kolomhead++, "Imagen Url");
	xlsWriteLabel($tablehead, $kolomhead++, "Imagen File");
	xlsWriteLabel($tablehead, $kolomhead++, "Activo");

	foreach ($this->Evento_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->usuario_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->categoria_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->subcategoria_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nombre);
	    xlsWriteLabel($tablebody, $kolombody++, $data->descripcion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->direccion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ciudad);
	    xlsWriteLabel($tablebody, $kolombody++, $data->codigo_postal);
	    xlsWriteLabel($tablebody, $kolombody++, $data->correo);
	    xlsWriteLabel($tablebody, $kolombody++, $data->telefono);
	    xlsWriteLabel($tablebody, $kolombody++, $data->fecha);
	    xlsWriteLabel($tablebody, $kolombody++, $data->hora);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Informacion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Estadio);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Empresa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->imagen_url);
	    xlsWriteLabel($tablebody, $kolombody++, $data->imagen_file);
	    xlsWriteLabel($tablebody, $kolombody++, $data->activo);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }



}