<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class TiendaController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->load->model('usuario');
        $this->load->model('tienda');

        $this->twig->addGlobal('session', $this->session);

        if ( !$this->session->userdata('isLogin') ) {
            redirect(site_url('login'));   
        }
    }

    public function index() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');
        
        if ($role_id == 6 ) {
            $entities = $this->tienda->findAll();
        } else {
            $entities = $this->tienda->findAllByIdUser($usuario_id);
        }

        $this->twig->display('admin/tiendas/index', array(
            'entities' => $entities,
        ));
    }

    public function nuevo() {

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        $data = array(
            'action' => base_url('admin/tiendaController/nuevo_action'),
            'id' => set_value('id'),
            'usuarios' => $usuarios,
            'nombre' => set_value('nombre'),
            'representante' => set_value('representante'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/tiendas/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

    public function nuevo_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('representante', 'representante', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->nuevo();
        } else {
            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'representante' => $this->input->post('representante',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->tienda->insert($data);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('admin/tiendas'));
        }
    }

    public function editar($id) {
        
        $tienda = $this->tienda->findById($id);

        $role_id = $this->session->userdata('role'); 
        $usuario_id = $this->session->userdata('id');

        if ($role_id == 6 ) {
            $usuarios = $this->usuario->findAll();
        } else {
            $usuario = $this->usuario->findById($usuario_id);
            $usuarios = array($this->usuario->findById($usuario_id));
        }

        if ($tienda) {
            $data = array(
                'action' => base_url('admin/tiendaController/editar_action'),
                'id' => set_value('id', $tienda->id),
                'usuarios' => $usuarios,
                'nombre' => set_value('nombre', $tienda->nombre),
                'representante' => set_value('representante', $tienda->representante),
                'activo' => set_value('activo', $tienda->activo),
            );

            $this->twig->display('admin/tiendas/form', array( 
                'data' => $data,
                'edit' => true
            ));

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/tiendas'));
        }
    }

    public function editar_action() {

        $this->form_validation->set_rules('usuario_id', 'usuario_id', 'trim|required');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('representante', 'representante', 'trim|required');
        $this->form_validation->set_rules('activo', 'activo', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->editar( $this->input->post('id', TRUE) );
        } else {
            $data = array(
                'usuario_id' => $this->input->post('usuario_id',TRUE),
                'nombre' => $this->input->post('nombre',TRUE),
                'representante' => $this->input->post('representante',TRUE),
                'activo' => $this->input->post('activo',TRUE),
            );

            $this->tienda->update($this->input->post('id', TRUE), $data);

            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');

            redirect(site_url('admin/tiendas'));
        }
    }

    public function tienda_borrar($id) {
        $row = $this->tienda->findById($id);

        if ($row) {
            $this->tienda->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('admin/tiendas'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('admin/tiendas'));
        }
    }

    public function sucursal_index() {
        
        $entities = $this->tienda->findAll();

        $this->twig->display('admin/tiendas/sucursales/index', array(
            'entities' => $entities,
        ));
    }

    public function sucursal_nuevo() {

        $data = array(
            'action' => base_url('admin/tiendaController/sucursal_nuevo_action'),
            'id' => set_value('id'),
            'nombre' => set_value('nombre'),
            'representante' => set_value('representante'),
            'direccion' => set_value('direccion'),
            'ciudad' => set_value('ciudad'),
            'activo' => set_value('activo'),
        );

        $this->twig->display('admin/tiendas/form', array( 
            'data' => $data,
            'edit' => false
        ));
    }

}