<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datos_de_pago_empresas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Datos_de_pago_empresas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
         /* verifico que tengo un id de empresa cargado */
        if ($this->session->userdata('idEmpresa')) {
          $idEmp=   $this->session->userdata('idEmpresa');
          }
        /* <<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>><> */

        $q = $idEmp;//urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'datos_de_pago_empresas/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'datos_de_pago_empresas/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'datos_de_pago_empresas/index.html';
            $config['first_url'] = base_url() . 'datos_de_pago_empresas/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Datos_de_pago_empresas_model->total_Cuentas_de_la_empresa($q);
        $datos_de_pago_empresas = $this->Datos_de_pago_empresas_model->get_CuentasDeLaEmpresa($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'datos_de_pago_empresas_data' => $datos_de_pago_empresas,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        $this->load->view('datos_de_pago_empresas/datos_de_pago_empresas_list', $data);
        $this->load->view('global/footer');

    }

    public function read($id) 
    {
        $row = $this->Datos_de_pago_empresas_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'empresa' => $row->empresa,
		'cuenta' => $row->cuenta,
		'banco' => $row->banco,
	    );

        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        $this->load->view('datos_de_pago_empresas/datos_de_pago_empresas_read', $data);
        $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'No se ha encontrado');
            redirect(site_url('datos_de_pago_empresas'));
        }
    }

    public function create()
    {  /* verifico que tengo un id de empresa cargado */
      if ($this->session->userdata('idEmpresa')) {
          $idEmp=   $this->session->userdata('idEmpresa');
          }
        $data = array(
            'button' => 'Create',
            'action' => site_url('datos_de_pago_empresas/create_action'),
	    'id' => set_value('id'),
	    'empresa' => set_value('empresa',$idEmp),
	    'cuenta' => set_value('cuenta'),
	    'banco' => set_value('banco'),
	);

        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        $this->load->view('datos_de_pago_empresas/datos_de_pago_empresas_form', $data);
        $this->load->view('global/footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'empresa' => $this->input->post('empresa',TRUE),
		'cuenta' => $this->input->post('cuenta',TRUE),
		'banco' => $this->input->post('banco',TRUE),
	    );

            $this->Datos_de_pago_empresas_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('datos_de_pago_empresas'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Datos_de_pago_empresas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('datos_de_pago_empresas/update_action'),
		'id' => set_value('id', $row->id),
		'empresa' => set_value('empresa', $row->empresa),
		'cuenta' => set_value('cuenta', $row->cuenta),
		'banco' => set_value('banco', $row->banco),
	    );

        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        $this->load->view('datos_de_pago_empresas/datos_de_pago_empresas_form', $data);
        $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'No se ha encontrado');
            redirect(site_url('datos_de_pago_empresas'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'empresa' => $this->input->post('empresa',TRUE),
		'cuenta' => $this->input->post('cuenta',TRUE),
		'banco' => $this->input->post('banco',TRUE),
	    );

            $this->Datos_de_pago_empresas_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('datos_de_pago_empresas'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Datos_de_pago_empresas_model->get_by_id($id);

        if ($row) {
            $this->Datos_de_pago_empresas_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('datos_de_pago_empresas'));
        } else {
            $this->session->set_flashdata('message', 'No se ha encontrado');
            redirect(site_url('datos_de_pago_empresas'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('empresa', 'empresa', 'trim|required');
	$this->form_validation->set_rules('cuenta', 'cuenta', 'trim|required');
	$this->form_validation->set_rules('banco', 'banco', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "datos_de_pago_empresas.xls";
        $judul = "datos_de_pago_empresas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Empresa");
	xlsWriteLabel($tablehead, $kolomhead++, "Cuenta");
	xlsWriteLabel($tablehead, $kolomhead++, "Banco");

	foreach ($this->Datos_de_pago_empresas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->empresa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->cuenta);
	    xlsWriteLabel($tablebody, $kolombody++, $data->banco);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=datos_de_pago_empresas.doc");

        $data = array(
            'datos_de_pago_empresas_data' => $this->Datos_de_pago_empresas_model->get_all(),
            'start' => 0
        );
        

        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        $this->load->view('datos_de_pago_empresas/datos_de_pago_empresas_doc',$data);
        $this->load->view('global/footer');
    }

}

/* End of file Datos_de_pago_empresas.php */
/* Location: ./application/controllers/Datos_de_pago_empresas.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-26 16:03:14 */
/* http://harviacode.com */