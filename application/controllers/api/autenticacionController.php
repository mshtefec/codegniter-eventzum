<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AutenticacionController extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('twig');
        $this->load->library('session');
        $this->load->library('form_validation');
        
        $this->load->model('usuario');
        $this->load->model('carrito');

        $this->twig->addGlobal('session', $this->session); 
    }

    public function login() {

        $data = array(
            'action' => site_url('api/autenticacionController/validate'),
        );

        $this->twig->display('autenticacion/login', array( 
            'data' => $data
        ));
    }

    public function validate() {

        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $entity = $this->usuario->login($email, $password);
        
        if ( !$entity ) {
            redirect(site_url('login'));
        } else {
            
            $data = array(
                'Id_Usuario' => $entity->id,
                'id' => $entity->id,
                'email' => $entity->email,
                'role' => $entity->role_id,
                'isLogin' => true,
            );
            
            $this->session->set_userdata($data);

            redirect(site_url('configuracion'));
        }
    }

    public function registro() {
        
        $data = array(
            'action' => base_url('api/autenticacionController/create_usuario'),
            'id' => set_value('id'),
            'username' => set_value('username'),
            'email' => set_value('email'),
            'password' => set_value('password'),
        );

        $this->twig->display('autenticacion/registro', array( 
            'data' => $data
        ));
    }

    public function create_usuario() {

        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            $this->registro();
        } else {
            $data = array(
                'username' => $this->input->post('username',TRUE),
                'email' => $this->input->post('email',TRUE),
                'password' => md5($this->input->post('password',TRUE)),
            );

            $this->usuario->insert($data);
            $usuario_id = $this->db->insert_id();

            $data_carrito = array(
                'id' => set_value('id'),
                'usuario_id' => $usuario_id,
                'total' => 0,
            );

            $this->carrito->insert($data_carrito);

            $this->session->set_flashdata('message', 'Se ha creado correctamente');

            redirect(site_url('login'));
        }
    }

    public function recupero() {

        $data = array(
            'action' => site_url('api/autenticacionController/recupero_action'),
        );

        $this->twig->display('autenticacion/recupero', array( 
            'data' => $data
        ));
    }

    public function recupero_action() {

        $this->form_validation->set_rules('email', 'email', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->recupero();
        } else {

            $this->load->library('email');
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
                
            //El servidor de correo que utilizaremos
            $config["smtp_host"] = 'smtp.gmail.com';
                
            //Nuestro usuario
            $config["smtp_user"] = 'placidomaidana@gmail.com';
                
            //Nuestra contraseña
            $config["smtp_pass"] = '3692hkhm';
                
            //El puerto que utilizará el servidor smtp
            $config["smtp_port"] = '587';
                
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8';
        
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
                
            //El email debe ser valido 
            $config['validate'] = true;

            //Establecemos esta configuración
            $this->email->initialize($config);

            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('placidomaidana@gmail.com', 'Plácido Maidana');
            $email_destinatario = $this->input->post('email');

            /*
            * Ponemos el o los destinatarios para los que va el email
            * en este caso al ser un formulario de contacto te lo enviarás a ti
            * mismo
            */
            $this->email->to($email_destinatario, 'Estimado Usuario:.....');

            //Definimos el asunto del mensaje
            $this->email->subject($this->input->post("Restablecer contraseña"));

            //Definimos el mensaje a enviar
            $this->email->message(
                "Email: " . $email_destinatario .
                " Mensaje: " . $this->input->post("mensaje")
            );

            //Enviamos el email y si se produce bien o mal que avise con una flasdata
            if($this->email->send()){
                $this->session->set_flashdata('message', 'Se ha enviado un mensaje a la casilla de correo');
            } else {
                $this->session->set_flashdata('message', 'No se a enviado el email');
            }

            $data = array(
                'email' => $email_destinatario,
            );

            $this->twig->display('autenticacion/correo', array( 
                'data' => $data
            ));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(site_url(''));
    }
}