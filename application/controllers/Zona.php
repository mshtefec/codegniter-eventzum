<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zona extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Zona_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'zona/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'zona/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'zona/index.html';
            $config['first_url'] = base_url() . 'zona/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Zona_model->total_rows($q);
        $zona = $this->Zona_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'zona_data' => $zona,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('zona/zona_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Zona_model->get_by_id($id);
        if ($row) {
            $data = array(
		'idZonasFila' => $row->idZonasFila,
		'ZonasEstadioDescripcion' => $row->ZonasEstadioDescripcion,
		'Fila' => $row->Fila,
		'AsientoInicial' => $row->AsientoInicial,
		'AsientoFinal' => $row->AsientoFinal,
		'Precio' => $row->Precio,
		'eventos_id' => $row->eventos_id,
	    );
            $this->load->view('zona/zona_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zona'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('zona/create_action'),
	    'idZonasFila' => set_value('idZonasFila'),
	    'ZonasEstadioDescripcion' => set_value('ZonasEstadioDescripcion'),
	    'Fila' => set_value('Fila'),
	    'AsientoInicial' => set_value('AsientoInicial'),
	    'AsientoFinal' => set_value('AsientoFinal'),
	    'Precio' => set_value('Precio'),
	    'eventos_id' => set_value('eventos_id'),
	);
        $this->load->view('zona/zona_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ZonasEstadioDescripcion' => $this->input->post('ZonasEstadioDescripcion',TRUE),
		'Fila' => $this->input->post('Fila',TRUE),
		'AsientoInicial' => $this->input->post('AsientoInicial',TRUE),
		'AsientoFinal' => $this->input->post('AsientoFinal',TRUE),
		'Precio' => $this->input->post('Precio',TRUE),
		'eventos_id' => $this->input->post('eventos_id',TRUE),
	    );

            $this->Zona_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('zona'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Zona_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('zona/update_action'),
		'idZonasFila' => set_value('idZonasFila', $row->idZonasFila),
		'ZonasEstadioDescripcion' => set_value('ZonasEstadioDescripcion', $row->ZonasEstadioDescripcion),
		'Fila' => set_value('Fila', $row->Fila),
		'AsientoInicial' => set_value('AsientoInicial', $row->AsientoInicial),
		'AsientoFinal' => set_value('AsientoFinal', $row->AsientoFinal),
		'Precio' => set_value('Precio', $row->Precio),
		'eventos_id' => set_value('eventos_id', $row->eventos_id),
	    );
            $this->load->view('zona/zona_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zona'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('idZonasFila', TRUE));
        } else {
            $data = array(
		'ZonasEstadioDescripcion' => $this->input->post('ZonasEstadioDescripcion',TRUE),
		'Fila' => $this->input->post('Fila',TRUE),
		'AsientoInicial' => $this->input->post('AsientoInicial',TRUE),
		'AsientoFinal' => $this->input->post('AsientoFinal',TRUE),
		'Precio' => $this->input->post('Precio',TRUE),
		'eventos_id' => $this->input->post('eventos_id',TRUE),
	    );

            $this->Zona_model->update($this->input->post('idZonasFila', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('zona'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Zona_model->get_by_id($id);

        if ($row) {
            $this->Zona_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('zona'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('zona'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ZonasEstadioDescripcion', 'zonasestadiodescripcion', 'trim|required');
	$this->form_validation->set_rules('Fila', 'fila', 'trim|required');
	$this->form_validation->set_rules('AsientoInicial', 'asientoinicial', 'trim|required');
	$this->form_validation->set_rules('AsientoFinal', 'asientofinal', 'trim|required');
	$this->form_validation->set_rules('Precio', 'precio', 'trim|required|numeric');
	$this->form_validation->set_rules('eventos_id', 'eventos id', 'trim|required');

	$this->form_validation->set_rules('idZonasFila', 'idZonasFila', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "zona.xls";
        $judul = "zona";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "ZonasEstadioDescripcion");
	xlsWriteLabel($tablehead, $kolomhead++, "Fila");
	xlsWriteLabel($tablehead, $kolomhead++, "AsientoInicial");
	xlsWriteLabel($tablehead, $kolomhead++, "AsientoFinal");
	xlsWriteLabel($tablehead, $kolomhead++, "Precio");
	xlsWriteLabel($tablehead, $kolomhead++, "Eventos Id");

	foreach ($this->Zona_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ZonasEstadioDescripcion);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Fila);
	    xlsWriteNumber($tablebody, $kolombody++, $data->AsientoInicial);
	    xlsWriteNumber($tablebody, $kolombody++, $data->AsientoFinal);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Precio);
	    xlsWriteNumber($tablebody, $kolombody++, $data->eventos_id);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=zona.doc");

        $data = array(
            'zona_data' => $this->Zona_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('zona/zona_doc',$data);
    }

}

/* End of file Zona.php */
/* Location: ./application/controllers/Zona.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-04-18 18:31:34 */
/* http://harviacode.com */