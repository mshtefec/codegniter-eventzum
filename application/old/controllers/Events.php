<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Eventos_model');
        $this->load->model('eventos_categorias_model');
        $this->load->model('eventos_subcategorias_model');
        $this->load->model('empresas_model');
        $this->load->library('form_validation');
    }


    public function index()
    {   
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'events/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events/index.html';
            $config['first_url'] = base_url() . 'events/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_model->total_rows($q);
        $events = $this->Events_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_data' => $events,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
                    );





     //$this->load->view('events/events_list', $data);
      $this->load->view('global/header');
      $this->load->view('global/menulateral');
      $this->load->view('events/events_list', $data);
      $this->load->view('global/footer');
     
     
    }

    public function tablas()
    {
      $this->load->view('global/header');
      $this->load->view('global/menulateral');
      $this->load->view('global/tables');
      $this->load->view('global/footer');

    }

    public function read($id)
    {
        $row = $this->Events_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'category' => $row->category,
		'name' => $row->name,
		'address' => $row->address,
		'city' => $row->city,
		'zipcode' => $row->zipcode,
		'email' => $row->email,
		'phone' => $row->phone,
		'password' => $row->password,
		'start_date' => date_format(new DateTime($row->start_date), 'd/m/y'),
		'end_date' => date_format(new DateTime($row->end_date), 'd/m/y'),
		'active' => $row->active,
		'date' => date_format(new DateTime($row->date), 'd/m/y'),
		'company' => $row->company,
	    );
            $this->load->view('events/events_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events/create_action'),
	    'id' => set_value('id'),
	    'category' => set_value('category'),
	    'name' => set_value('name'),
	    'address' => set_value('address'),
	    'city' => set_value('city'),
	    'zipcode' => set_value('zipcode'),
	    'email' => set_value('email'),
	    'phone' => set_value('phone'),
	    'password' => set_value('password'),
	    'start_date' => set_value('start_date'),
	    'end_date' => set_value('end_date'),
	    'active' => set_value('active'),
	    'date' => set_value('date'),
	    'company' => set_value('company'),
        	);

      $this->load->view('global/header');
      $this->load->view('global/menulateral');
      $this->load->view('events/events_form', $data);
      $this->load->view('global/footer');
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'category' => $this->input->post('category',TRUE),
		'name' => $this->input->post('name',TRUE),
		'address' => $this->input->post('address',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'email' => $this->input->post('email',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'active' => $this->input->post('active',TRUE),
		'date' => $this->input->post('date',TRUE),
		'company' => $this->input->post('company',TRUE),
	    );

            $this->Events_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events'));
        }
    }

    public function update($id)
    {
        $row = $this->Events_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events/update_action'),
		'id' => set_value('id', $row->id),
		'category' => set_value('category', $row->category),
		'name' => set_value('name', $row->name),
		'address' => set_value('address', $row->address),
		'city' => set_value('city', $row->city),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'email' => set_value('email', $row->email),
		'phone' => set_value('phone', $row->phone),
	   	'start_date' => set_value('start_date',date_format(new DateTime($row->start_date), 'd/m/y')),
		'end_date' => set_value('end_date', $row->end_date),
		'active' => set_value('active', $row->active),
		'date' => set_value('date', $row->date),
		'company' => set_value('company', $row->company),
        'categoriasT' => $this->Categories_model->get_all(),
        'companiesT'=>$this->Companies_model->get_all(),
	    );
        $this->load->view('global/header');
        $this->load->view('global/menulateral');
        //$this->load->view('events/events_form', $data);
        $this->load->view('events/formulario',$data);
        $this->load->view('global/footer');

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events'));
        }
    }
    
    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'category' => $this->input->post('category',TRUE),
		'name' => $this->input->post('name',TRUE),
		'address' => $this->input->post('address',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'email' => $this->input->post('email',TRUE),
		'phone' => $this->input->post('phone',TRUE),
	 	'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'active' => $this->input->post('active',TRUE),
		'date' => $this->input->post('date',TRUE),
		'company' => $this->input->post('company',TRUE),
	    );

            $this->Events_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events'));
        }
    }
    
    public function delete($id)
    {
        $row = $this->Events_model->get_by_id($id);

        if ($row) {
            $this->Events_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events'));
        }
    }

     public function seleccionar()
    {         echo('nombre es: <br> '. $this->input->post('name').'<br>');
               echo var_dump($this->input->post());
         if($this->input->is_ajax_request()){
              echo('nombre es: <br> '. $this->input->post('name').'<br><br>');
            $_SESSION['form']=$this->input->post();

            echo('Imprimiendo la variable de sesion:<br>');
            var_dump($_SESSION['form']);



        }else{
            show_404();

        }

    }


    public function _rules() 
    {
	$this->form_validation->set_rules('category', 'category', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('address', 'address', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
   	$this->form_validation->set_rules('start_date', 'start date', 'trim|required');
	$this->form_validation->set_rules('end_date', 'end date', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('company', 'company', 'trim|required');
 	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events.php */
/* Location: ./application/controllers/Events.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:05 */
/* http://harviacode.com */