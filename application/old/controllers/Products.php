<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Products_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'products/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'products/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'products/index.html';
            $config['first_url'] = base_url() . 'products/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Products_model->total_rows($q);
        $products = $this->Products_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'products_data' => $products,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('products/products_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Products_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'shop' => $row->shop,
		'category' => $row->category,
		'name' => $row->name,
		'description' => $row->description,
		'price' => $row->price,
		'tax' => $row->tax,
		'stock' => $row->stock,
		'celiac' => $row->celiac,
		'vegetarian' => $row->vegetarian,
		'active' => $row->active,
	    );
            $this->load->view('products/products_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('products/create_action'),
	    'id' => set_value('id'),
	    'shop' => set_value('shop'),
	    'category' => set_value('category'),
	    'name' => set_value('name'),
	    'description' => set_value('description'),
	    'price' => set_value('price'),
	    'tax' => set_value('tax'),
	    'stock' => set_value('stock'),
	    'celiac' => set_value('celiac'),
	    'vegetarian' => set_value('vegetarian'),
	    'active' => set_value('active'),
	);
        $this->load->view('products/products_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'category' => $this->input->post('category',TRUE),
		'name' => $this->input->post('name',TRUE),
		'description' => $this->input->post('description',TRUE),
		'price' => $this->input->post('price',TRUE),
		'tax' => $this->input->post('tax',TRUE),
		'stock' => $this->input->post('stock',TRUE),
		'celiac' => $this->input->post('celiac',TRUE),
		'vegetarian' => $this->input->post('vegetarian',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Products_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('products'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Products_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('products/update_action'),
		'id' => set_value('id', $row->id),
		'shop' => set_value('shop', $row->shop),
		'category' => set_value('category', $row->category),
		'name' => set_value('name', $row->name),
		'description' => set_value('description', $row->description),
		'price' => set_value('price', $row->price),
		'tax' => set_value('tax', $row->tax),
		'stock' => set_value('stock', $row->stock),
		'celiac' => set_value('celiac', $row->celiac),
		'vegetarian' => set_value('vegetarian', $row->vegetarian),
		'active' => set_value('active', $row->active),
	    );
            $this->load->view('products/products_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'category' => $this->input->post('category',TRUE),
		'name' => $this->input->post('name',TRUE),
		'description' => $this->input->post('description',TRUE),
		'price' => $this->input->post('price',TRUE),
		'tax' => $this->input->post('tax',TRUE),
		'stock' => $this->input->post('stock',TRUE),
		'celiac' => $this->input->post('celiac',TRUE),
		'vegetarian' => $this->input->post('vegetarian',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Products_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('products'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Products_model->get_by_id($id);

        if ($row) {
            $this->Products_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('products'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('shop', 'shop', 'trim|required');
	$this->form_validation->set_rules('category', 'category', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');
	$this->form_validation->set_rules('tax', 'tax', 'trim|required');
	$this->form_validation->set_rules('stock', 'stock', 'trim|required');
	$this->form_validation->set_rules('celiac', 'celiac', 'trim|required');
	$this->form_validation->set_rules('vegetarian', 'vegetarian', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Products.php */
/* Location: ./application/controllers/Products.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */