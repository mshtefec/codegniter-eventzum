<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_artists extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_artists_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_artists/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_artists/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_artists/index.html';
            $config['first_url'] = base_url() . 'events_artists/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_artists_model->total_rows($q);
        $events_artists = $this->Events_artists_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_artists_data' => $events_artists,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_artists/events_artists_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_artists_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'spotify' => $row->spotify,
		'event' => $row->event,
	    );
            $this->load->view('events_artists/events_artists_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_artists'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_artists/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'spotify' => set_value('spotify'),
	    'event' => set_value('event'),
	);
        $this->load->view('events_artists/events_artists_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'spotify' => $this->input->post('spotify',TRUE),
		'event' => $this->input->post('event',TRUE),
	    );

            $this->Events_artists_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_artists'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_artists_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_artists/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'spotify' => set_value('spotify', $row->spotify),
		'event' => set_value('event', $row->event),
	    );
            $this->load->view('events_artists/events_artists_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_artists'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'spotify' => $this->input->post('spotify',TRUE),
		'event' => $this->input->post('event',TRUE),
	    );

            $this->Events_artists_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_artists'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_artists_model->get_by_id($id);

        if ($row) {
            $this->Events_artists_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_artists'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_artists'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('spotify', 'spotify', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_artists.php */
/* Location: ./application/controllers/Events_artists.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */