<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_confirmation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_confirmation_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_confirmation/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_confirmation/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_confirmation/index.html';
            $config['first_url'] = base_url() . 'users_confirmation/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_confirmation_model->total_rows($q);
        $users_confirmation = $this->Users_confirmation_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_confirmation_data' => $users_confirmation,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_confirmation/users_confirmation_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_confirmation_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'token' => $row->token,
	    );
            $this->load->view('users_confirmation/users_confirmation_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_confirmation'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_confirmation/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'token' => set_value('token'),
	);
        $this->load->view('users_confirmation/users_confirmation_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'token' => $this->input->post('token',TRUE),
	    );

            $this->Users_confirmation_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_confirmation'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_confirmation_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_confirmation/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'token' => set_value('token', $row->token),
	    );
            $this->load->view('users_confirmation/users_confirmation_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_confirmation'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'token' => $this->input->post('token',TRUE),
	    );

            $this->Users_confirmation_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_confirmation'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_confirmation_model->get_by_id($id);

        if ($row) {
            $this->Users_confirmation_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_confirmation'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_confirmation'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('token', 'token', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users_confirmation.php */
/* Location: ./application/controllers/Users_confirmation.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */