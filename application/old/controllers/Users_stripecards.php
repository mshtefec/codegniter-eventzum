<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_stripecards extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_stripecards_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_stripecards/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_stripecards/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_stripecards/index.html';
            $config['first_url'] = base_url() . 'users_stripecards/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_stripecards_model->total_rows($q);
        $users_stripecards = $this->Users_stripecards_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_stripecards_data' => $users_stripecards,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_stripecards/users_stripecards_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_stripecards_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'card' => $row->card,
		'last4' => $row->last4,
		'brand' => $row->brand,
		'exp_month' => $row->exp_month,
		'exp_year' => $row->exp_year,
	    );
            $this->load->view('users_stripecards/users_stripecards_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_stripecards'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_stripecards/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'card' => set_value('card'),
	    'last4' => set_value('last4'),
	    'brand' => set_value('brand'),
	    'exp_month' => set_value('exp_month'),
	    'exp_year' => set_value('exp_year'),
	);
        $this->load->view('users_stripecards/users_stripecards_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'card' => $this->input->post('card',TRUE),
		'last4' => $this->input->post('last4',TRUE),
		'brand' => $this->input->post('brand',TRUE),
		'exp_month' => $this->input->post('exp_month',TRUE),
		'exp_year' => $this->input->post('exp_year',TRUE),
	    );

            $this->Users_stripecards_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_stripecards'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_stripecards_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_stripecards/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'card' => set_value('card', $row->card),
		'last4' => set_value('last4', $row->last4),
		'brand' => set_value('brand', $row->brand),
		'exp_month' => set_value('exp_month', $row->exp_month),
		'exp_year' => set_value('exp_year', $row->exp_year),
	    );
            $this->load->view('users_stripecards/users_stripecards_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_stripecards'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'card' => $this->input->post('card',TRUE),
		'last4' => $this->input->post('last4',TRUE),
		'brand' => $this->input->post('brand',TRUE),
		'exp_month' => $this->input->post('exp_month',TRUE),
		'exp_year' => $this->input->post('exp_year',TRUE),
	    );

            $this->Users_stripecards_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_stripecards'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_stripecards_model->get_by_id($id);

        if ($row) {
            $this->Users_stripecards_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_stripecards'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_stripecards'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('card', 'card', 'trim|required');
	$this->form_validation->set_rules('last4', 'last4', 'trim|required');
	$this->form_validation->set_rules('brand', 'brand', 'trim|required');
	$this->form_validation->set_rules('exp_month', 'exp month', 'trim|required');
	$this->form_validation->set_rules('exp_year', 'exp year', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users_stripecards.php */
/* Location: ./application/controllers/Users_stripecards.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */