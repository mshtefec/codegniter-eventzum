<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shops_subcategories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Shops_subcategories_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'shops_subcategories/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'shops_subcategories/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'shops_subcategories/index.html';
            $config['first_url'] = base_url() . 'shops_subcategories/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Shops_subcategories_model->total_rows($q);
        $shops_subcategories = $this->Shops_subcategories_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'shops_subcategories_data' => $shops_subcategories,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('shops_subcategories/shops_subcategories_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Shops_subcategories_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'shop' => $row->shop,
		'subcategory' => $row->subcategory,
	    );
            $this->load->view('shops_subcategories/shops_subcategories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_subcategories'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('shops_subcategories/create_action'),
	    'id' => set_value('id'),
	    'shop' => set_value('shop'),
	    'subcategory' => set_value('subcategory'),
	);
        $this->load->view('shops_subcategories/shops_subcategories_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'subcategory' => $this->input->post('subcategory',TRUE),
	    );

            $this->Shops_subcategories_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('shops_subcategories'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Shops_subcategories_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('shops_subcategories/update_action'),
		'id' => set_value('id', $row->id),
		'shop' => set_value('shop', $row->shop),
		'subcategory' => set_value('subcategory', $row->subcategory),
	    );
            $this->load->view('shops_subcategories/shops_subcategories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_subcategories'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'subcategory' => $this->input->post('subcategory',TRUE),
	    );

            $this->Shops_subcategories_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('shops_subcategories'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Shops_subcategories_model->get_by_id($id);

        if ($row) {
            $this->Shops_subcategories_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('shops_subcategories'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_subcategories'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('shop', 'shop', 'trim|required');
	$this->form_validation->set_rules('subcategory', 'subcategory', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Shops_subcategories.php */
/* Location: ./application/controllers/Shops_subcategories.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */