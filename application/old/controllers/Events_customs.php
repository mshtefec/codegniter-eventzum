<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_customs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_customs_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_customs/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_customs/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_customs/index.html';
            $config['first_url'] = base_url() . 'events_customs/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_customs_model->total_rows($q);
        $events_customs = $this->Events_customs_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_customs_data' => $events_customs,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_customs/events_customs_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_customs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'background_color' => $row->background_color,
		'logo_width' => $row->logo_width,
		'button_color' => $row->button_color,
		'highlight_color' => $row->highlight_color,
	    );
            $this->load->view('events_customs/events_customs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_customs'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_customs/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'background_color' => set_value('background_color'),
	    'logo_width' => set_value('logo_width'),
	    'button_color' => set_value('button_color'),
	    'highlight_color' => set_value('highlight_color'),
	);
        $this->load->view('events_customs/events_customs_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'background_color' => $this->input->post('background_color',TRUE),
		'logo_width' => $this->input->post('logo_width',TRUE),
		'button_color' => $this->input->post('button_color',TRUE),
		'highlight_color' => $this->input->post('highlight_color',TRUE),
	    );

            $this->Events_customs_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_customs'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_customs_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_customs/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'background_color' => set_value('background_color', $row->background_color),
		'logo_width' => set_value('logo_width', $row->logo_width),
		'button_color' => set_value('button_color', $row->button_color),
		'highlight_color' => set_value('highlight_color', $row->highlight_color),
	    );
            $this->load->view('events_customs/events_customs_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_customs'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'background_color' => $this->input->post('background_color',TRUE),
		'logo_width' => $this->input->post('logo_width',TRUE),
		'button_color' => $this->input->post('button_color',TRUE),
		'highlight_color' => $this->input->post('highlight_color',TRUE),
	    );

            $this->Events_customs_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_customs'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_customs_model->get_by_id($id);

        if ($row) {
            $this->Events_customs_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_customs'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_customs'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('background_color', 'background color', 'trim|required');
	$this->form_validation->set_rules('logo_width', 'logo width', 'trim|required');
	$this->form_validation->set_rules('button_color', 'button color', 'trim|required');
	$this->form_validation->set_rules('highlight_color', 'highlight color', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_customs.php */
/* Location: ./application/controllers/Events_customs.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */