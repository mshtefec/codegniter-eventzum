<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallets_history extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wallets_history_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'wallets_history/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'wallets_history/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'wallets_history/index.html';
            $config['first_url'] = base_url() . 'wallets_history/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Wallets_history_model->total_rows($q);
        $wallets_history = $this->Wallets_history_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'wallets_history_data' => $wallets_history,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('wallets_history/wallets_history_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Wallets_history_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'wallet' => $row->wallet,
		'value' => $row->value,
		'total' => $row->total,
		'type' => $row->type,
		'user' => $row->user,
		'date' => $row->date,
	    );
            $this->load->view('wallets_history/wallets_history_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('wallets_history/create_action'),
	    'id' => set_value('id'),
	    'wallet' => set_value('wallet'),
	    'value' => set_value('value'),
	    'total' => set_value('total'),
	    'type' => set_value('type'),
	    'user' => set_value('user'),
	    'date' => set_value('date'),
	);
        $this->load->view('wallets_history/wallets_history_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'wallet' => $this->input->post('wallet',TRUE),
		'value' => $this->input->post('value',TRUE),
		'total' => $this->input->post('total',TRUE),
		'type' => $this->input->post('type',TRUE),
		'user' => $this->input->post('user',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Wallets_history_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wallets_history'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Wallets_history_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wallets_history/update_action'),
		'id' => set_value('id', $row->id),
		'wallet' => set_value('wallet', $row->wallet),
		'value' => set_value('value', $row->value),
		'total' => set_value('total', $row->total),
		'type' => set_value('type', $row->type),
		'user' => set_value('user', $row->user),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('wallets_history/wallets_history_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'wallet' => $this->input->post('wallet',TRUE),
		'value' => $this->input->post('value',TRUE),
		'total' => $this->input->post('total',TRUE),
		'type' => $this->input->post('type',TRUE),
		'user' => $this->input->post('user',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Wallets_history_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wallets_history'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Wallets_history_model->get_by_id($id);

        if ($row) {
            $this->Wallets_history_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wallets_history'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('wallet', 'wallet', 'trim|required');
	$this->form_validation->set_rules('value', 'value', 'trim|required|numeric');
	$this->form_validation->set_rules('total', 'total', 'trim|required|numeric');
	$this->form_validation->set_rules('type', 'type', 'trim|required');
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Wallets_history.php */
/* Location: ./application/controllers/Wallets_history.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */