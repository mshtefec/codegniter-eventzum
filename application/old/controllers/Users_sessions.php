<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_sessions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_sessions_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_sessions/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_sessions/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_sessions/index.html';
            $config['first_url'] = base_url() . 'users_sessions/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_sessions_model->total_rows($q);
        $users_sessions = $this->Users_sessions_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_sessions_data' => $users_sessions,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_sessions/users_sessions_list', $data);
    }

   

    public function read($id)
    {
        $row = $this->Users_sessions_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'ssid' => $row->ssid,
		'user' => $row->user,
		'date' => $row->date,
	    );
            $this->load->view('users_sessions/users_sessions_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_sessions'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_sessions/create_action'),
	    'id' => set_value('id'),
	    'ssid' => set_value('ssid'),
	    'user' => set_value('user'),
	    'date' => set_value('date'),
	);
        $this->load->view('users_sessions/users_sessions_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'user' => $this->input->post('user',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Users_sessions_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_sessions'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_sessions_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_sessions/update_action'),
		'id' => set_value('id', $row->id),
		'ssid' => set_value('ssid', $row->ssid),
		'user' => set_value('user', $row->user),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('users_sessions/users_sessions_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_sessions'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'user' => $this->input->post('user',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Users_sessions_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_sessions'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_sessions_model->get_by_id($id);

        if ($row) {
            $this->Users_sessions_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_sessions'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_sessions'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ssid', 'ssid', 'trim|required');
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users_sessions.php */
/* Location: ./application/controllers/Users_sessions.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */