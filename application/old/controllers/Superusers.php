<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Superusers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Superusers_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'superusers/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'superusers/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'superusers/index.html';
            $config['first_url'] = base_url() . 'superusers/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Superusers_model->total_rows($q);
        $superusers = $this->Superusers_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'superusers_data' => $superusers,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('superusers/superusers_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Superusers_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'email' => $row->email,
		'password' => $row->password,
		'date' => $row->date,
		'active' => $row->active,
	    );
            $this->load->view('superusers/superusers_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('superusers/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'date' => set_value('date'),
	    'active' => set_value('active'),
	);
        $this->load->view('superusers/superusers_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Superusers_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('superusers'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Superusers_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('superusers/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'date' => set_value('date', $row->date),
		'active' => set_value('active', $row->active),
	    );
            $this->load->view('superusers/superusers_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Superusers_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('superusers'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Superusers_model->get_by_id($id);

        if ($row) {
            $this->Superusers_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('superusers'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Superusers.php */
/* Location: ./application/controllers/Superusers.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */