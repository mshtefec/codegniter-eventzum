<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shops extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Shops_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'shops/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'shops/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'shops/index.html';
            $config['first_url'] = base_url() . 'shops/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Shops_model->total_rows($q);
        $shops = $this->Shops_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'shops_data' => $shops,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('shops/shops_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Shops_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'business' => $row->business,
		'nif' => $row->nif,
		'representative' => $row->representative,
		'email' => $row->email,
		'phone' => $row->phone,
		'password' => $row->password,
		'date' => $row->date,
		'event' => $row->event,
		'address' => $row->address,
		'zipcode' => $row->zipcode,
		'city' => $row->city,
	    );
            $this->load->view('shops/shops_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('shops/create_action'),
	    'id' => set_value('id'),
	    'business' => set_value('business'),
	    'nif' => set_value('nif'),
	    'representative' => set_value('representative'),
	    'email' => set_value('email'),
	    'phone' => set_value('phone'),
	    'password' => set_value('password'),
	    'date' => set_value('date'),
	    'event' => set_value('event'),
	    'address' => set_value('address'),
	    'zipcode' => set_value('zipcode'),
	    'city' => set_value('city'),
	);
        $this->load->view('shops/shops_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'business' => $this->input->post('business',TRUE),
		'nif' => $this->input->post('nif',TRUE),
		'representative' => $this->input->post('representative',TRUE),
		'email' => $this->input->post('email',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'event' => $this->input->post('event',TRUE),
		'address' => $this->input->post('address',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'city' => $this->input->post('city',TRUE),
	    );

            $this->Shops_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('shops'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Shops_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('shops/update_action'),
		'id' => set_value('id', $row->id),
		'business' => set_value('business', $row->business),
		'nif' => set_value('nif', $row->nif),
		'representative' => set_value('representative', $row->representative),
		'email' => set_value('email', $row->email),
		'phone' => set_value('phone', $row->phone),
		'password' => set_value('password', $row->password),
		'date' => set_value('date', $row->date),
		'event' => set_value('event', $row->event),
		'address' => set_value('address', $row->address),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'city' => set_value('city', $row->city),
	    );
            $this->load->view('shops/shops_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'business' => $this->input->post('business',TRUE),
		'nif' => $this->input->post('nif',TRUE),
		'representative' => $this->input->post('representative',TRUE),
		'email' => $this->input->post('email',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'event' => $this->input->post('event',TRUE),
		'address' => $this->input->post('address',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'city' => $this->input->post('city',TRUE),
	    );

            $this->Shops_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('shops'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Shops_model->get_by_id($id);

        if ($row) {
            $this->Shops_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('shops'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('business', 'business', 'trim|required');
	$this->form_validation->set_rules('nif', 'nif', 'trim|required');
	$this->form_validation->set_rules('representative', 'representative', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('address', 'address', 'trim|required');
	$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Shops.php */
/* Location: ./application/controllers/Shops.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */