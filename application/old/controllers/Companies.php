<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Companies extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Companies_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'companies/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'companies/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'companies/index.html';
            $config['first_url'] = base_url() . 'companies/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Companies_model->total_rows($q);
        $companies = $this->Companies_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'companies_data' => $companies,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('companies/companies_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Companies_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nif' => $row->nif,
		'name' => $row->name,
		'address' => $row->address,
		'phone' => $row->phone,
		'email' => $row->email,
		'password' => $row->password,
		'date' => $row->date,
		'active' => $row->active,
	    );
            $this->load->view('companies/companies_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('companies/create_action'),
	    'id' => set_value('id'),
	    'nif' => set_value('nif'),
	    'name' => set_value('name'),
	    'address' => set_value('address'),
	    'phone' => set_value('phone'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'date' => set_value('date'),
	    'active' => set_value('active'),
	);
        $this->load->view('companies/companies_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'name' => $this->input->post('name',TRUE),
		'address' => $this->input->post('address',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Companies_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('companies'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Companies_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('companies/update_action'),
		'id' => set_value('id', $row->id),
		'nif' => set_value('nif', $row->nif),
		'name' => set_value('name', $row->name),
		'address' => set_value('address', $row->address),
		'phone' => set_value('phone', $row->phone),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'date' => set_value('date', $row->date),
		'active' => set_value('active', $row->active),
	    );
            $this->load->view('companies/companies_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'name' => $this->input->post('name',TRUE),
		'address' => $this->input->post('address',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Companies_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('companies'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Companies_model->get_by_id($id);

        if ($row) {
            $this->Companies_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('companies'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nif', 'nif', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('address', 'address', 'trim|required');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "companies.xls";
        $judul = "companies";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nif");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Address");
	xlsWriteLabel($tablehead, $kolomhead++, "Phone");
	xlsWriteLabel($tablehead, $kolomhead++, "Email");
	xlsWriteLabel($tablehead, $kolomhead++, "Password");
	xlsWriteLabel($tablehead, $kolomhead++, "Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Active");

	foreach ($this->Companies_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nif);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->address);
	    xlsWriteLabel($tablebody, $kolombody++, $data->phone);
	    xlsWriteLabel($tablebody, $kolombody++, $data->email);
	    xlsWriteLabel($tablebody, $kolombody++, $data->password);
	    xlsWriteNumber($tablebody, $kolombody++, $data->date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->active);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Companies.php */
/* Location: ./application/controllers/Companies.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-13 20:18:30 */
/* http://harviacode.com */