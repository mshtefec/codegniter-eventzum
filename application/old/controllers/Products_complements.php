<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_complements extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Products_complements_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'products_complements/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'products_complements/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'products_complements/index.html';
            $config['first_url'] = base_url() . 'products_complements/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Products_complements_model->total_rows($q);
        $products_complements = $this->Products_complements_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'products_complements_data' => $products_complements,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('products_complements/products_complements_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Products_complements_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'cgroup' => $row->cgroup,
		'name' => $row->name,
		'price' => $row->price,
	    );
            $this->load->view('products_complements/products_complements_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_complements'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('products_complements/create_action'),
	    'id' => set_value('id'),
	    'cgroup' => set_value('cgroup'),
	    'name' => set_value('name'),
	    'price' => set_value('price'),
	);
        $this->load->view('products_complements/products_complements_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'cgroup' => $this->input->post('cgroup',TRUE),
		'name' => $this->input->post('name',TRUE),
		'price' => $this->input->post('price',TRUE),
	    );

            $this->Products_complements_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('products_complements'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Products_complements_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('products_complements/update_action'),
		'id' => set_value('id', $row->id),
		'cgroup' => set_value('cgroup', $row->cgroup),
		'name' => set_value('name', $row->name),
		'price' => set_value('price', $row->price),
	    );
            $this->load->view('products_complements/products_complements_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_complements'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'cgroup' => $this->input->post('cgroup',TRUE),
		'name' => $this->input->post('name',TRUE),
		'price' => $this->input->post('price',TRUE),
	    );

            $this->Products_complements_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('products_complements'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Products_complements_model->get_by_id($id);

        if ($row) {
            $this->Products_complements_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('products_complements'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_complements'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('cgroup', 'cgroup', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Products_complements.php */
/* Location: ./application/controllers/Products_complements.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */