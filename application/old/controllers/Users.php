<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');
     
    }

    public function index()
    {
        //$img->load->view('Dropzone/form1');
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users/index.html';
            $config['first_url'] = base_url() . 'users/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_model->total_rows($q);
        $users = $this->Users_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_data' => $users,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

            $this->load->view('global/header');
                        $this->load->view('global/menulateral',$data);
                      $this->load->view('users/users_list', $data);
                        $this->load->view('global/footer');



    }


    public function form1($id)  //muestro la ficha con los datos actualizados del usuario
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Modificar',
                'action' => site_url('users/ModifDatos_action'),
		'id' => set_value('id', $row->id),
        'avatar'=> set_value('avatar',base_url().'uploads/'.$row->avatar),
       	'name' => set_value('name', $row->name),
		'lastname' => set_value('lastname', $row->lastname),
		'phone' => set_value('phone', $row->phone),
		'address' => set_value('address', $row->address),
		'city' => set_value('city', $row->city),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'email' => set_value('email', $row->email),
		'date' => set_value('date', $row->date),
		'active' => set_value('active', $row->active),
		'stripe_customer' => set_value('stripe_customer', $row->stripe_customer),
        'tipo' => $this->input->post('tipo',TRUE),
	    );

            $this->load->view('global/header');
            $this->load->view('global/menulateral',$data);
            $this->load->view('users/ModificarDatos', $data);
            $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

    public function read($id)
    {
        $row = $this->Users_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'lastname' => $row->lastname,
		'phone' => $row->phone,
		'address' => $row->address,
		'city' => $row->city,
		'zipcode' => $row->zipcode,
		'email' => $row->email,
		'password' => $row->password,
		'date' => $row->date,
		'active' => $row->active,
		'stripe_customer' => $row->stripe_customer,
	    );
            $this->load->view('users/users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('users/create_action'),
	    'id' => set_value('id'),
        'avatar'=> set_value('avatar', base_url().'img/usuario03.jpg'),
	    'name' => set_value('name'),
	    'lastname' => set_value('lastname'),
	    'phone' => set_value('phone'),
	    'address' => set_value('address'),
	    'city' => set_value('city'),
	    'zipcode' => set_value('zipcode'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'date' => set_value('date'),
	    'active' => set_value('active'),
	    'stripe_customer' => set_value('stripe_customer'),
        'tipo' => set_value('tipo'),
	);


        $this->load->view('global/header');
        $this->load->view('global/menulateral',$data);
        $this->load->view('users/users_form', $data);
        $this->load->view('global/footer');
    }
    
    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'lastname' => $this->input->post('lastname',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'address' => $this->input->post('address',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => md5($this->input->post('password',TRUE)),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
		'stripe_customer' => $this->input->post('stripe_customer',TRUE),
        'tipo' => $this->input->post('tipo',TRUE),
	    );

            $this->Users_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('users'));
        }
    }


     public function ModifDatos($id)
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Modificar',
                'action' => site_url('users/ModifDatos_action'),
		'id' => set_value('id', $row->id),
        'avatar'=> set_value('avatar',base_url().'uploads/'.$row->avatar),
       	'name' => set_value('name', $row->name),
		'lastname' => set_value('lastname', $row->lastname),
		'phone' => set_value('phone', $row->phone),
		'address' => set_value('address', $row->address),
		'city' => set_value('city', $row->city),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'email' => set_value('email', $row->email),
		'date' => set_value('date', $row->date),
		'active' => set_value('active', $row->active),
		'stripe_customer' => set_value('stripe_customer', $row->stripe_customer),
        'tipo' => set_value('tipo', $row->tipo),
	    );

            $this->load->view('global/header');
            $this->load->view('global/menulateral',$data);
            $this->load->view('users/ModificarDatos', $data);
            $this->load->view('global/footer');
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users/form1()'));
        }
    }


    public function ModifDatos_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'lastname' => $this->input->post('lastname',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'address' => $this->input->post('address',TRUE),
		'city' => $this->input->post('city',TRUE),
        'avatar' => $this->input->post('id', TRUE).'.'.pathinfo(strtolower($_FILES['file']['name']), PATHINFO_EXTENSION),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'email' => $this->input->post('email',TRUE),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
		'stripe_customer' => $this->input->post('stripe_customer',TRUE),
        'tipo' => $this->input->post('tipo',TRUE),
	    );

            $this->Users_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');



    // if (!empty($_FILES)) {
            echo "<script>alert('Trae un archivo, �Gracias!.');</script>";

        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        $targetPath = './uploads/';

        $targetFile = $targetPath .  $this->input->post('id', TRUE).'.'.pathinfo(strtolower($_FILES['file']['name']), PATHINFO_EXTENSION) ;
        move_uploaded_file($tempFile, $targetFile);
        redirect(site_url('users/ModifDatos/'.$this->input->post('id', TRUE)));

        // if you want to save in db,where here
        // with out model just for example
        // $this->load->database(); // load database
        // $this->db->insert('file_table',array('file_name' => $fileName));
      //  }

        }
    }

    public function uploadImagenes()
    {   echo "<script>alert('Trae un archivo, �Gracias!.');</script>";
     if (!empty($_FILES)) {


        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        $targetPath = base_url().'img/';
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        redirect(site_url('users/form-1'));
        // if you want to save in db,where here
        // with out model just for example
        // $this->load->database(); // load database
        // $this->db->insert('file_table',array('file_name' => $fileName));
        }

    }
    public function update($id)
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'lastname' => set_value('lastname', $row->lastname),
		'phone' => set_value('phone', $row->phone),
		'address' => set_value('address', $row->address),
		'city' => set_value('city', $row->city),
		'zipcode' => set_value('zipcode', $row->zipcode),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'date' => set_value('date', $row->date),
		'active' => set_value('active', $row->active),
		'stripe_customer' => set_value('stripe_customer', $row->stripe_customer),
        'tipo' => $this->input->post('tipo',TRUE),
	    );
            $this->load->view('users/users_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'lastname' => $this->input->post('lastname',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'address' => $this->input->post('address',TRUE),
		'city' => $this->input->post('city',TRUE),
		'zipcode' => $this->input->post('zipcode',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => md5($this->input->post('password',TRUE)),
		'date' => $this->input->post('date',TRUE),
		'active' => $this->input->post('active',TRUE),
		'stripe_customer' => $this->input->post('stripe_customer',TRUE),
        'tipo' => $this->input->post('tipo',TRUE),
	    );

            $this->Users_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('users'));
        }
    }

    public function delete($id)
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $this->Users_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

    //Par de metodos para la modificacion de la contrase�a de usuario
     public function ModifPasword($id)
    {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Modificar',
                'action' => site_url('users/ModifPasword_action'),
                'id' => set_value('id', $row->id),
                'password' => set_value('password', $row->password),
                          );


            $this->load->view('global/header');
            $this->load->view('global/menulateral',$data);
            $this->load->view('users/ModificarPasword', $data);
            $this->load->view('global/footer');

        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('users'));
        }
    }

    public function CambiaPasword()
    {



         $this->load->view('global/header');
                        $this->load->view('global/menulateral',$data);
                         $this->load->view('global/errorLogin');
                        $this->load->view('global/footer');

       /*
        if ($this->input->post('password') == $this->input->post('password2')) {

              $data = array('password' => md5($this->input->post('password',TRUE)));
              $this->Users_model->update($this->input->post('id', TRUE), $data);
              $this->session->set_flashdata('message', 'Se ha actualizado los datos');

        } else {// echo "Las claves no coinciden"  ;

         $this->session->set_flashdata('message', 'Las contrase&ntilde;as no coiciden');
        }

        redirect(site_url('PantInicio/fichaLogueo' );*/

    }

    //finalizan los metdos para la modificacion de la contrase�a del usuario




    public function _rules()
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('lastname', 'lastname', 'trim|required');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('address', 'address', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	//$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');
	$this->form_validation->set_rules('stripe_customer', 'stripe customer', 'trim|required');
    $this->form_validation->set_rules('tipo', 'stripe customer', 'trim|required');
	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */