<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_faq extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_faq_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_faq/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_faq/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_faq/index.html';
            $config['first_url'] = base_url() . 'events_faq/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_faq_model->total_rows($q);
        $events_faq = $this->Events_faq_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_faq_data' => $events_faq,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_faq/events_faq_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_faq_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'question' => $row->question,
		'answer' => $row->answer,
	    );
            $this->load->view('events_faq/events_faq_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_faq'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_faq/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'question' => set_value('question'),
	    'answer' => set_value('answer'),
	);
        $this->load->view('events_faq/events_faq_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'question' => $this->input->post('question',TRUE),
		'answer' => $this->input->post('answer',TRUE),
	    );

            $this->Events_faq_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_faq'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_faq_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_faq/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'question' => set_value('question', $row->question),
		'answer' => set_value('answer', $row->answer),
	    );
            $this->load->view('events_faq/events_faq_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_faq'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'question' => $this->input->post('question',TRUE),
		'answer' => $this->input->post('answer',TRUE),
	    );

            $this->Events_faq_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_faq'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_faq_model->get_by_id($id);

        if ($row) {
            $this->Events_faq_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_faq'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_faq'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('question', 'question', 'trim|required');
	$this->form_validation->set_rules('answer', 'answer', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_faq.php */
/* Location: ./application/controllers/Events_faq.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */