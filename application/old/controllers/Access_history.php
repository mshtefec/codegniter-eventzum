<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Access_history extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Access_history_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'access_history/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'access_history/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'access_history/index.html';
            $config['first_url'] = base_url() . 'access_history/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Access_history_model->total_rows($q);
        $access_history = $this->Access_history_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'access_history_data' => $access_history,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('access_history/access_history_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Access_history_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'uticket' => $row->uticket,
		'event' => $row->event,
		'seller' => $row->seller,
		'response' => $row->response,
		'date' => $row->date,
	    );
            $this->load->view('access_history/access_history_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('access_history'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('access_history/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'uticket' => set_value('uticket'),
	    'event' => set_value('event'),
	    'seller' => set_value('seller'),
	    'response' => set_value('response'),
	    'date' => set_value('date'),
	);
        $this->load->view('access_history/access_history_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'uticket' => $this->input->post('uticket',TRUE),
		'event' => $this->input->post('event',TRUE),
		'seller' => $this->input->post('seller',TRUE),
		'response' => $this->input->post('response',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Access_history_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('access_history'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Access_history_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('access_history/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'uticket' => set_value('uticket', $row->uticket),
		'event' => set_value('event', $row->event),
		'seller' => set_value('seller', $row->seller),
		'response' => set_value('response', $row->response),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('access_history/access_history_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('access_history'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'uticket' => $this->input->post('uticket',TRUE),
		'event' => $this->input->post('event',TRUE),
		'seller' => $this->input->post('seller',TRUE),
		'response' => $this->input->post('response',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Access_history_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('access_history'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Access_history_model->get_by_id($id);

        if ($row) {
            $this->Access_history_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('access_history'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('access_history'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('uticket', 'uticket', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('seller', 'seller', 'trim|required');
	$this->form_validation->set_rules('response', 'response', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Access_history.php */
/* Location: ./application/controllers/Access_history.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:05 */
/* http://harviacode.com */