<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Companies_stripedata extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Companies_stripedata_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'companies_stripedata/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'companies_stripedata/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'companies_stripedata/index.html';
            $config['first_url'] = base_url() . 'companies_stripedata/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Companies_stripedata_model->total_rows($q);
        $companies_stripedata = $this->Companies_stripedata_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'companies_stripedata_data' => $companies_stripedata,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('companies_stripedata/companies_stripedata_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Companies_stripedata_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'company' => $row->company,
		'account' => $row->account,
		'external_account' => $row->external_account,
		'business_logo' => $row->business_logo,
	    );
            $this->load->view('companies_stripedata/companies_stripedata_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies_stripedata'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('companies_stripedata/create_action'),
	    'id' => set_value('id'),
	    'company' => set_value('company'),
	    'account' => set_value('account'),
	    'external_account' => set_value('external_account'),
	    'business_logo' => set_value('business_logo'),
	);
        $this->load->view('companies_stripedata/companies_stripedata_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'company' => $this->input->post('company',TRUE),
		'account' => $this->input->post('account',TRUE),
		'external_account' => $this->input->post('external_account',TRUE),
		'business_logo' => $this->input->post('business_logo',TRUE),
	    );

            $this->Companies_stripedata_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('companies_stripedata'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Companies_stripedata_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('companies_stripedata/update_action'),
		'id' => set_value('id', $row->id),
		'company' => set_value('company', $row->company),
		'account' => set_value('account', $row->account),
		'external_account' => set_value('external_account', $row->external_account),
		'business_logo' => set_value('business_logo', $row->business_logo),
	    );
            $this->load->view('companies_stripedata/companies_stripedata_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies_stripedata'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'company' => $this->input->post('company',TRUE),
		'account' => $this->input->post('account',TRUE),
		'external_account' => $this->input->post('external_account',TRUE),
		'business_logo' => $this->input->post('business_logo',TRUE),
	    );

            $this->Companies_stripedata_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('companies_stripedata'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Companies_stripedata_model->get_by_id($id);

        if ($row) {
            $this->Companies_stripedata_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('companies_stripedata'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('companies_stripedata'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('company', 'company', 'trim|required');
	$this->form_validation->set_rules('account', 'account', 'trim|required');
	$this->form_validation->set_rules('external_account', 'external account', 'trim|required');
	$this->form_validation->set_rules('business_logo', 'business logo', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Companies_stripedata.php */
/* Location: ./application/controllers/Companies_stripedata.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:05 */
/* http://harviacode.com */