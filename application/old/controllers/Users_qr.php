<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_qr extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_qr_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_qr/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_qr/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_qr/index.html';
            $config['first_url'] = base_url() . 'users_qr/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_qr_model->total_rows($q);
        $users_qr = $this->Users_qr_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_qr_data' => $users_qr,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_qr/users_qr_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_qr_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'code' => $row->code,
		'last' => $row->last,
	    );
            $this->load->view('users_qr/users_qr_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_qr'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_qr/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'code' => set_value('code'),
	    'last' => set_value('last'),
	);
        $this->load->view('users_qr/users_qr_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'code' => $this->input->post('code',TRUE),
		'last' => $this->input->post('last',TRUE),
	    );

            $this->Users_qr_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_qr'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_qr_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_qr/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'code' => set_value('code', $row->code),
		'last' => set_value('last', $row->last),
	    );
            $this->load->view('users_qr/users_qr_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_qr'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'code' => $this->input->post('code',TRUE),
		'last' => $this->input->post('last',TRUE),
	    );

            $this->Users_qr_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_qr'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_qr_model->get_by_id($id);

        if ($row) {
            $this->Users_qr_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_qr'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_qr'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('code', 'code', 'trim|required');
	$this->form_validation->set_rules('last', 'last', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users_qr.php */
/* Location: ./application/controllers/Users_qr.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */