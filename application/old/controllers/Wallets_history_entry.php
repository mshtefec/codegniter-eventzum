<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallets_history_entry extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wallets_history_entry_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'wallets_history_entry/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'wallets_history_entry/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'wallets_history_entry/index.html';
            $config['first_url'] = base_url() . 'wallets_history_entry/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Wallets_history_entry_model->total_rows($q);
        $wallets_history_entry = $this->Wallets_history_entry_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'wallets_history_entry_data' => $wallets_history_entry,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('wallets_history_entry/wallets_history_entry_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Wallets_history_entry_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'wallet_history' => $row->wallet_history,
		'card' => $row->card,
		'charge' => $row->charge,
	    );
            $this->load->view('wallets_history_entry/wallets_history_entry_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_entry'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('wallets_history_entry/create_action'),
	    'id' => set_value('id'),
	    'wallet_history' => set_value('wallet_history'),
	    'card' => set_value('card'),
	    'charge' => set_value('charge'),
	);
        $this->load->view('wallets_history_entry/wallets_history_entry_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'wallet_history' => $this->input->post('wallet_history',TRUE),
		'card' => $this->input->post('card',TRUE),
		'charge' => $this->input->post('charge',TRUE),
	    );

            $this->Wallets_history_entry_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wallets_history_entry'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Wallets_history_entry_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wallets_history_entry/update_action'),
		'id' => set_value('id', $row->id),
		'wallet_history' => set_value('wallet_history', $row->wallet_history),
		'card' => set_value('card', $row->card),
		'charge' => set_value('charge', $row->charge),
	    );
            $this->load->view('wallets_history_entry/wallets_history_entry_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_entry'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'wallet_history' => $this->input->post('wallet_history',TRUE),
		'card' => $this->input->post('card',TRUE),
		'charge' => $this->input->post('charge',TRUE),
	    );

            $this->Wallets_history_entry_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wallets_history_entry'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Wallets_history_entry_model->get_by_id($id);

        if ($row) {
            $this->Wallets_history_entry_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wallets_history_entry'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_entry'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('wallet_history', 'wallet history', 'trim|required');
	$this->form_validation->set_rules('card', 'card', 'trim|required');
	$this->form_validation->set_rules('charge', 'charge', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Wallets_history_entry.php */
/* Location: ./application/controllers/Wallets_history_entry.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */