<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_categories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Products_categories_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'products_categories/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'products_categories/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'products_categories/index.html';
            $config['first_url'] = base_url() . 'products_categories/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Products_categories_model->total_rows($q);
        $products_categories = $this->Products_categories_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'products_categories_data' => $products_categories,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('products_categories/products_categories_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Products_categories_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'shop' => $row->shop,
		'name' => $row->name,
	    );
            $this->load->view('products_categories/products_categories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_categories'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('products_categories/create_action'),
	    'id' => set_value('id'),
	    'shop' => set_value('shop'),
	    'name' => set_value('name'),
	);
        $this->load->view('products_categories/products_categories_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'name' => $this->input->post('name',TRUE),
	    );

            $this->Products_categories_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('products_categories'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Products_categories_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('products_categories/update_action'),
		'id' => set_value('id', $row->id),
		'shop' => set_value('shop', $row->shop),
		'name' => set_value('name', $row->name),
	    );
            $this->load->view('products_categories/products_categories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_categories'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'name' => $this->input->post('name',TRUE),
	    );

            $this->Products_categories_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('products_categories'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Products_categories_model->get_by_id($id);

        if ($row) {
            $this->Products_categories_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('products_categories'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_categories'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('shop', 'shop', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Products_categories.php */
/* Location: ./application/controllers/Products_categories.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */