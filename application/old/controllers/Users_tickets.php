<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_tickets extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_tickets_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users_tickets/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users_tickets/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users_tickets/index.html';
            $config['first_url'] = base_url() . 'users_tickets/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users_tickets_model->total_rows($q);
        $users_tickets = $this->Users_tickets_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users_tickets_data' => $users_tickets,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('users_tickets/users_tickets_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Users_tickets_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user' => $row->user,
		'ticket' => $row->ticket,
		'event' => $row->event,
		'code' => $row->code,
		'used' => $row->used,
	    );
            $this->load->view('users_tickets/users_tickets_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_tickets'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users_tickets/create_action'),
	    'id' => set_value('id'),
	    'user' => set_value('user'),
	    'ticket' => set_value('ticket'),
	    'event' => set_value('event'),
	    'code' => set_value('code'),
	    'used' => set_value('used'),
	);
        $this->load->view('users_tickets/users_tickets_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'ticket' => $this->input->post('ticket',TRUE),
		'event' => $this->input->post('event',TRUE),
		'code' => $this->input->post('code',TRUE),
		'used' => $this->input->post('used',TRUE),
	    );

            $this->Users_tickets_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users_tickets'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Users_tickets_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users_tickets/update_action'),
		'id' => set_value('id', $row->id),
		'user' => set_value('user', $row->user),
		'ticket' => set_value('ticket', $row->ticket),
		'event' => set_value('event', $row->event),
		'code' => set_value('code', $row->code),
		'used' => set_value('used', $row->used),
	    );
            $this->load->view('users_tickets/users_tickets_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_tickets'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user' => $this->input->post('user',TRUE),
		'ticket' => $this->input->post('ticket',TRUE),
		'event' => $this->input->post('event',TRUE),
		'code' => $this->input->post('code',TRUE),
		'used' => $this->input->post('used',TRUE),
	    );

            $this->Users_tickets_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users_tickets'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Users_tickets_model->get_by_id($id);

        if ($row) {
            $this->Users_tickets_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users_tickets'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users_tickets'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('ticket', 'ticket', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('code', 'code', 'trim|required');
	$this->form_validation->set_rules('used', 'used', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Users_tickets.php */
/* Location: ./application/controllers/Users_tickets.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */