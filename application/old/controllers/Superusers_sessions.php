<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Superusers_sessions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Superusers_sessions_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'superusers_sessions/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'superusers_sessions/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'superusers_sessions/index.html';
            $config['first_url'] = base_url() . 'superusers_sessions/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Superusers_sessions_model->total_rows($q);
        $superusers_sessions = $this->Superusers_sessions_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'superusers_sessions_data' => $superusers_sessions,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('superusers_sessions/superusers_sessions_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Superusers_sessions_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'ssid' => $row->ssid,
		'superuser' => $row->superuser,
		'date' => $row->date,
	    );
            $this->load->view('superusers_sessions/superusers_sessions_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers_sessions'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('superusers_sessions/create_action'),
	    'id' => set_value('id'),
	    'ssid' => set_value('ssid'),
	    'superuser' => set_value('superuser'),
	    'date' => set_value('date'),
	);
        $this->load->view('superusers_sessions/superusers_sessions_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'superuser' => $this->input->post('superuser',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Superusers_sessions_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('superusers_sessions'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Superusers_sessions_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('superusers_sessions/update_action'),
		'id' => set_value('id', $row->id),
		'ssid' => set_value('ssid', $row->ssid),
		'superuser' => set_value('superuser', $row->superuser),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('superusers_sessions/superusers_sessions_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers_sessions'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'superuser' => $this->input->post('superuser',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Superusers_sessions_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('superusers_sessions'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Superusers_sessions_model->get_by_id($id);

        if ($row) {
            $this->Superusers_sessions_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('superusers_sessions'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superusers_sessions'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ssid', 'ssid', 'trim|required');
	$this->form_validation->set_rules('superuser', 'superuser', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Superusers_sessions.php */
/* Location: ./application/controllers/Superusers_sessions.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */