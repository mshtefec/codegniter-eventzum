<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_schedule extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_schedule_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_schedule/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_schedule/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_schedule/index.html';
            $config['first_url'] = base_url() . 'events_schedule/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_schedule_model->total_rows($q);
        $events_schedule = $this->Events_schedule_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_schedule_data' => $events_schedule,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_schedule/events_schedule_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_schedule_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'title' => $row->title,
		'start' => $row->start,
		'end' => $row->end,
		'artist' => $row->artist,
		'location' => $row->location,
	    );
            $this->load->view('events_schedule/events_schedule_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_schedule'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_schedule/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'title' => set_value('title'),
	    'start' => set_value('start'),
	    'end' => set_value('end'),
	    'artist' => set_value('artist'),
	    'location' => set_value('location'),
	);
        $this->load->view('events_schedule/events_schedule_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'title' => $this->input->post('title',TRUE),
		'start' => $this->input->post('start',TRUE),
		'end' => $this->input->post('end',TRUE),
		'artist' => $this->input->post('artist',TRUE),
		'location' => $this->input->post('location',TRUE),
	    );

            $this->Events_schedule_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_schedule'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_schedule_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_schedule/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'title' => set_value('title', $row->title),
		'start' => set_value('start', $row->start),
		'end' => set_value('end', $row->end),
		'artist' => set_value('artist', $row->artist),
		'location' => set_value('location', $row->location),
	    );
            $this->load->view('events_schedule/events_schedule_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_schedule'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'title' => $this->input->post('title',TRUE),
		'start' => $this->input->post('start',TRUE),
		'end' => $this->input->post('end',TRUE),
		'artist' => $this->input->post('artist',TRUE),
		'location' => $this->input->post('location',TRUE),
	    );

            $this->Events_schedule_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_schedule'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_schedule_model->get_by_id($id);

        if ($row) {
            $this->Events_schedule_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_schedule'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_schedule'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('start', 'start', 'trim|required');
	$this->form_validation->set_rules('end', 'end', 'trim|required');
	$this->form_validation->set_rules('artist', 'artist', 'trim|required');
	$this->form_validation->set_rules('location', 'location', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_schedule.php */
/* Location: ./application/controllers/Events_schedule.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */