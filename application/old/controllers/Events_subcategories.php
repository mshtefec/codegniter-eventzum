<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_subcategories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_subcategories_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_subcategories/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_subcategories/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_subcategories/index.html';
            $config['first_url'] = base_url() . 'events_subcategories/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_subcategories_model->total_rows($q);
        $events_subcategories = $this->Events_subcategories_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_subcategories_data' => $events_subcategories,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_subcategories/events_subcategories_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_subcategories_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'subcategory' => $row->subcategory,
	    );
            $this->load->view('events_subcategories/events_subcategories_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_subcategories'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_subcategories/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'subcategory' => set_value('subcategory'),
	);
        $this->load->view('events_subcategories/events_subcategories_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'subcategory' => $this->input->post('subcategory',TRUE),
	    );

            $this->Events_subcategories_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_subcategories'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_subcategories_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_subcategories/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'subcategory' => set_value('subcategory', $row->subcategory),
	    );
            $this->load->view('events_subcategories/events_subcategories_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_subcategories'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'subcategory' => $this->input->post('subcategory',TRUE),
	    );

            $this->Events_subcategories_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_subcategories'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_subcategories_model->get_by_id($id);

        if ($row) {
            $this->Events_subcategories_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_subcategories'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_subcategories'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('subcategory', 'subcategory', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_subcategories.php */
/* Location: ./application/controllers/Events_subcategories.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */