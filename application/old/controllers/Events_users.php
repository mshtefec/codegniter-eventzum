<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_users_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_users/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_users/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_users/index.html';
            $config['first_url'] = base_url() . 'events_users/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_users_model->total_rows($q);
        $events_users = $this->Events_users_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_users_data' => $events_users,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_users/events_users_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_users_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'user' => $row->user,
		'uticket' => $row->uticket,
		'inside' => $row->inside,
	    );
            $this->load->view('events_users/events_users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_users'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_users/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'user' => set_value('user'),
	    'uticket' => set_value('uticket'),
	    'inside' => set_value('inside'),
	);
        $this->load->view('events_users/events_users_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'user' => $this->input->post('user',TRUE),
		'uticket' => $this->input->post('uticket',TRUE),
		'inside' => $this->input->post('inside',TRUE),
	    );

            $this->Events_users_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_users'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_users/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'user' => set_value('user', $row->user),
		'uticket' => set_value('uticket', $row->uticket),
		'inside' => set_value('inside', $row->inside),
	    );
            $this->load->view('events_users/events_users_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_users'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'user' => $this->input->post('user',TRUE),
		'uticket' => $this->input->post('uticket',TRUE),
		'inside' => $this->input->post('inside',TRUE),
	    );

            $this->Events_users_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_users'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_users_model->get_by_id($id);

        if ($row) {
            $this->Events_users_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_users'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_users'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('uticket', 'uticket', 'trim|required');
	$this->form_validation->set_rules('inside', 'inside', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_users.php */
/* Location: ./application/controllers/Events_users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */