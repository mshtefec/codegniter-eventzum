<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_cgroups extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Products_cgroups_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'products_cgroups/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'products_cgroups/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'products_cgroups/index.html';
            $config['first_url'] = base_url() . 'products_cgroups/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Products_cgroups_model->total_rows($q);
        $products_cgroups = $this->Products_cgroups_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'products_cgroups_data' => $products_cgroups,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('products_cgroups/products_cgroups_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Products_cgroups_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'product' => $row->product,
		'name' => $row->name,
		'min' => $row->min,
		'max' => $row->max,
	    );
            $this->load->view('products_cgroups/products_cgroups_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_cgroups'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('products_cgroups/create_action'),
	    'id' => set_value('id'),
	    'product' => set_value('product'),
	    'name' => set_value('name'),
	    'min' => set_value('min'),
	    'max' => set_value('max'),
	);
        $this->load->view('products_cgroups/products_cgroups_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'product' => $this->input->post('product',TRUE),
		'name' => $this->input->post('name',TRUE),
		'min' => $this->input->post('min',TRUE),
		'max' => $this->input->post('max',TRUE),
	    );

            $this->Products_cgroups_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('products_cgroups'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Products_cgroups_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('products_cgroups/update_action'),
		'id' => set_value('id', $row->id),
		'product' => set_value('product', $row->product),
		'name' => set_value('name', $row->name),
		'min' => set_value('min', $row->min),
		'max' => set_value('max', $row->max),
	    );
            $this->load->view('products_cgroups/products_cgroups_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_cgroups'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'product' => $this->input->post('product',TRUE),
		'name' => $this->input->post('name',TRUE),
		'min' => $this->input->post('min',TRUE),
		'max' => $this->input->post('max',TRUE),
	    );

            $this->Products_cgroups_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('products_cgroups'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Products_cgroups_model->get_by_id($id);

        if ($row) {
            $this->Products_cgroups_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('products_cgroups'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('products_cgroups'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('product', 'product', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('min', 'min', 'trim|required');
	$this->form_validation->set_rules('max', 'max', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Products_cgroups.php */
/* Location: ./application/controllers/Products_cgroups.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */