<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Items_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'items/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'items/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'items/index.html';
            $config['first_url'] = base_url() . 'items/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Items_model->total_rows($q);
        $items = $this->Items_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'items_data' => $items,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('items/items_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Items_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'company' => $row->company,
		'name' => $row->name,
		'description' => $row->description,
		'price' => $row->price,
		'quantity' => $row->quantity,
		'sold' => $row->sold,
		'start_date' => $row->start_date,
		'end_date' => $row->end_date,
		'vip' => $row->vip,
	    );
            $this->load->view('items/items_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('items'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('items/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'company' => set_value('company'),
	    'name' => set_value('name'),
	    'description' => set_value('description'),
	    'price' => set_value('price'),
	    'quantity' => set_value('quantity'),
	    'sold' => set_value('sold'),
	    'start_date' => set_value('start_date'),
	    'end_date' => set_value('end_date'),
	    'vip' => set_value('vip'),
	);
        $this->load->view('items/items_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'company' => $this->input->post('company',TRUE),
		'name' => $this->input->post('name',TRUE),
		'description' => $this->input->post('description',TRUE),
		'price' => $this->input->post('price',TRUE),
		'quantity' => $this->input->post('quantity',TRUE),
		'sold' => $this->input->post('sold',TRUE),
		'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'vip' => $this->input->post('vip',TRUE),
	    );

            $this->Items_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('items'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Items_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('items/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'company' => set_value('company', $row->company),
		'name' => set_value('name', $row->name),
		'description' => set_value('description', $row->description),
		'price' => set_value('price', $row->price),
		'quantity' => set_value('quantity', $row->quantity),
		'sold' => set_value('sold', $row->sold),
		'start_date' => set_value('start_date', $row->start_date),
		'end_date' => set_value('end_date', $row->end_date),
		'vip' => set_value('vip', $row->vip),
	    );
            $this->load->view('items/items_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('items'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'company' => $this->input->post('company',TRUE),
		'name' => $this->input->post('name',TRUE),
		'description' => $this->input->post('description',TRUE),
		'price' => $this->input->post('price',TRUE),
		'quantity' => $this->input->post('quantity',TRUE),
		'sold' => $this->input->post('sold',TRUE),
		'start_date' => $this->input->post('start_date',TRUE),
		'end_date' => $this->input->post('end_date',TRUE),
		'vip' => $this->input->post('vip',TRUE),
	    );

            $this->Items_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('items'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Items_model->get_by_id($id);

        if ($row) {
            $this->Items_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('items'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('items'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('company', 'company', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');
	$this->form_validation->set_rules('quantity', 'quantity', 'trim|required');
	$this->form_validation->set_rules('sold', 'sold', 'trim|required');
	$this->form_validation->set_rules('start_date', 'start date', 'trim|required');
	$this->form_validation->set_rules('end_date', 'end date', 'trim|required');
	$this->form_validation->set_rules('vip', 'vip', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Items.php */
/* Location: ./application/controllers/Items.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */