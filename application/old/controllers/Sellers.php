<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sellers extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Sellers_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'sellers/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'sellers/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'sellers/index.html';
            $config['first_url'] = base_url() . 'sellers/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Sellers_model->total_rows($q);
        $sellers = $this->Sellers_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'sellers_data' => $sellers,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('sellers/sellers_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Sellers_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nif' => $row->nif,
		'name' => $row->name,
		'lastname' => $row->lastname,
		'phone' => $row->phone,
		'email' => $row->email,
		'password' => $row->password,
		'active' => $row->active,
		'date' => $row->date,
		'event' => $row->event,
		'company' => $row->company,
	    );
            $this->load->view('sellers/sellers_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sellers'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('sellers/create_action'),
	    'id' => set_value('id'),
	    'nif' => set_value('nif'),
	    'name' => set_value('name'),
	    'lastname' => set_value('lastname'),
	    'phone' => set_value('phone'),
	    'email' => set_value('email'),
	    'password' => set_value('password'),
	    'active' => set_value('active'),
	    'date' => set_value('date'),
	    'event' => set_value('event'),
	    'company' => set_value('company'),
	);
        $this->load->view('sellers/sellers_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'name' => $this->input->post('name',TRUE),
		'lastname' => $this->input->post('lastname',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'active' => $this->input->post('active',TRUE),
		'date' => $this->input->post('date',TRUE),
		'event' => $this->input->post('event',TRUE),
		'company' => $this->input->post('company',TRUE),
	    );

            $this->Sellers_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('sellers'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Sellers_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('sellers/update_action'),
		'id' => set_value('id', $row->id),
		'nif' => set_value('nif', $row->nif),
		'name' => set_value('name', $row->name),
		'lastname' => set_value('lastname', $row->lastname),
		'phone' => set_value('phone', $row->phone),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'active' => set_value('active', $row->active),
		'date' => set_value('date', $row->date),
		'event' => set_value('event', $row->event),
		'company' => set_value('company', $row->company),
	    );
            $this->load->view('sellers/sellers_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sellers'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nif' => $this->input->post('nif',TRUE),
		'name' => $this->input->post('name',TRUE),
		'lastname' => $this->input->post('lastname',TRUE),
		'phone' => $this->input->post('phone',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'active' => $this->input->post('active',TRUE),
		'date' => $this->input->post('date',TRUE),
		'event' => $this->input->post('event',TRUE),
		'company' => $this->input->post('company',TRUE),
	    );

            $this->Sellers_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('sellers'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Sellers_model->get_by_id($id);

        if ($row) {
            $this->Sellers_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('sellers'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sellers'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nif', 'nif', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('lastname', 'lastname', 'trim|required');
	$this->form_validation->set_rules('phone', 'phone', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('company', 'company', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Sellers.php */
/* Location: ./application/controllers/Sellers.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */