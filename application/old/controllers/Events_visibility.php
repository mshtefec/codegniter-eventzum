<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_visibility extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_visibility_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_visibility/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_visibility/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_visibility/index.html';
            $config['first_url'] = base_url() . 'events_visibility/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_visibility_model->total_rows($q);
        $events_visibility = $this->Events_visibility_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_visibility_data' => $events_visibility,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_visibility/events_visibility_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_visibility_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'event' => $row->event,
		'artists' => $row->artists,
		'schedule' => $row->schedule,
		'images' => $row->images,
		'map' => $row->map,
		'faq' => $row->faq,
	    );
            $this->load->view('events_visibility/events_visibility_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_visibility'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_visibility/create_action'),
	    'id' => set_value('id'),
	    'event' => set_value('event'),
	    'artists' => set_value('artists'),
	    'schedule' => set_value('schedule'),
	    'images' => set_value('images'),
	    'map' => set_value('map'),
	    'faq' => set_value('faq'),
	);
        $this->load->view('events_visibility/events_visibility_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'artists' => $this->input->post('artists',TRUE),
		'schedule' => $this->input->post('schedule',TRUE),
		'images' => $this->input->post('images',TRUE),
		'map' => $this->input->post('map',TRUE),
		'faq' => $this->input->post('faq',TRUE),
	    );

            $this->Events_visibility_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_visibility'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_visibility_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_visibility/update_action'),
		'id' => set_value('id', $row->id),
		'event' => set_value('event', $row->event),
		'artists' => set_value('artists', $row->artists),
		'schedule' => set_value('schedule', $row->schedule),
		'images' => set_value('images', $row->images),
		'map' => set_value('map', $row->map),
		'faq' => set_value('faq', $row->faq),
	    );
            $this->load->view('events_visibility/events_visibility_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_visibility'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'event' => $this->input->post('event',TRUE),
		'artists' => $this->input->post('artists',TRUE),
		'schedule' => $this->input->post('schedule',TRUE),
		'images' => $this->input->post('images',TRUE),
		'map' => $this->input->post('map',TRUE),
		'faq' => $this->input->post('faq',TRUE),
	    );

            $this->Events_visibility_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_visibility'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_visibility_model->get_by_id($id);

        if ($row) {
            $this->Events_visibility_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_visibility'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_visibility'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('artists', 'artists', 'trim|required');
	$this->form_validation->set_rules('schedule', 'schedule', 'trim|required');
	$this->form_validation->set_rules('images', 'images', 'trim|required');
	$this->form_validation->set_rules('map', 'map', 'trim|required');
	$this->form_validation->set_rules('faq', 'faq', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_visibility.php */
/* Location: ./application/controllers/Events_visibility.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */