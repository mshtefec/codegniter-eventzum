<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallets extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wallets_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'wallets/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'wallets/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'wallets/index.html';
            $config['first_url'] = base_url() . 'wallets/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Wallets_model->total_rows($q);
        $wallets = $this->Wallets_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'wallets_data' => $wallets,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('wallets/wallets_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Wallets_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'value' => $row->value,
		'user' => $row->user,
		'event' => $row->event,
		'date' => $row->date,
	    );
            $this->load->view('wallets/wallets_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('wallets/create_action'),
	    'id' => set_value('id'),
	    'value' => set_value('value'),
	    'user' => set_value('user'),
	    'event' => set_value('event'),
	    'date' => set_value('date'),
	);
        $this->load->view('wallets/wallets_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'value' => $this->input->post('value',TRUE),
		'user' => $this->input->post('user',TRUE),
		'event' => $this->input->post('event',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Wallets_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wallets'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Wallets_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wallets/update_action'),
		'id' => set_value('id', $row->id),
		'value' => set_value('value', $row->value),
		'user' => set_value('user', $row->user),
		'event' => set_value('event', $row->event),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('wallets/wallets_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'value' => $this->input->post('value',TRUE),
		'user' => $this->input->post('user',TRUE),
		'event' => $this->input->post('event',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Wallets_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wallets'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Wallets_model->get_by_id($id);

        if ($row) {
            $this->Wallets_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wallets'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('value', 'value', 'trim|required|numeric');
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Wallets.php */
/* Location: ./application/controllers/Wallets.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */