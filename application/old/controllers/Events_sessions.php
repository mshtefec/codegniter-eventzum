<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Events_sessions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Events_sessions_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'events_sessions/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'events_sessions/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'events_sessions/index.html';
            $config['first_url'] = base_url() . 'events_sessions/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Events_sessions_model->total_rows($q);
        $events_sessions = $this->Events_sessions_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events_sessions_data' => $events_sessions,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('events_sessions/events_sessions_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Events_sessions_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'ssid' => $row->ssid,
		'event' => $row->event,
		'date' => $row->date,
	    );
            $this->load->view('events_sessions/events_sessions_read', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_sessions'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Crear',
            'action' => site_url('events_sessions/create_action'),
	    'id' => set_value('id'),
	    'ssid' => set_value('ssid'),
	    'event' => set_value('event'),
	    'date' => set_value('date'),
	);
        $this->load->view('events_sessions/events_sessions_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'event' => $this->input->post('event',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Events_sessions_model->insert($data);
            $this->session->set_flashdata('message', 'Se ha creado correctamente');
            redirect(site_url('events_sessions'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Events_sessions_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Actualizar',
                'action' => site_url('events_sessions/update_action'),
		'id' => set_value('id', $row->id),
		'ssid' => set_value('ssid', $row->ssid),
		'event' => set_value('event', $row->event),
		'date' => set_value('date', $row->date),
	    );
            $this->load->view('events_sessions/events_sessions_form', $data);
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_sessions'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'ssid' => $this->input->post('ssid',TRUE),
		'event' => $this->input->post('event',TRUE),
		'date' => $this->input->post('date',TRUE),
	    );

            $this->Events_sessions_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Se ha actualizado correctamente');
            redirect(site_url('events_sessions'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Events_sessions_model->get_by_id($id);

        if ($row) {
            $this->Events_sessions_model->delete($id);
            $this->session->set_flashdata('message', 'Se ha borrado correctamente');
            redirect(site_url('events_sessions'));
        } else {
            $this->session->set_flashdata('message', 'No se encontr&oacute;');
            redirect(site_url('events_sessions'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ssid', 'ssid', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Events_sessions.php */
/* Location: ./application/controllers/Events_sessions.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:06 */
/* http://harviacode.com */