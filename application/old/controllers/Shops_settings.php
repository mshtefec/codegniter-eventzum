<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shops_settings extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Shops_settings_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'shops_settings/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'shops_settings/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'shops_settings/index.html';
            $config['first_url'] = base_url() . 'shops_settings/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Shops_settings_model->total_rows($q);
        $shops_settings = $this->Shops_settings_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'shops_settings_data' => $shops_settings,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('shops_settings/shops_settings_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Shops_settings_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'shop' => $row->shop,
		'event' => $row->event,
		'title' => $row->title,
		'description' => $row->description,
		'category' => $row->category,
		'active' => $row->active,
	    );
            $this->load->view('shops_settings/shops_settings_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_settings'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('shops_settings/create_action'),
	    'id' => set_value('id'),
	    'shop' => set_value('shop'),
	    'event' => set_value('event'),
	    'title' => set_value('title'),
	    'description' => set_value('description'),
	    'category' => set_value('category'),
	    'active' => set_value('active'),
	);
        $this->load->view('shops_settings/shops_settings_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'event' => $this->input->post('event',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'category' => $this->input->post('category',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Shops_settings_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('shops_settings'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Shops_settings_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('shops_settings/update_action'),
		'id' => set_value('id', $row->id),
		'shop' => set_value('shop', $row->shop),
		'event' => set_value('event', $row->event),
		'title' => set_value('title', $row->title),
		'description' => set_value('description', $row->description),
		'category' => set_value('category', $row->category),
		'active' => set_value('active', $row->active),
	    );
            $this->load->view('shops_settings/shops_settings_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_settings'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'shop' => $this->input->post('shop',TRUE),
		'event' => $this->input->post('event',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description' => $this->input->post('description',TRUE),
		'category' => $this->input->post('category',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Shops_settings_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('shops_settings'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Shops_settings_model->get_by_id($id);

        if ($row) {
            $this->Shops_settings_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('shops_settings'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('shops_settings'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('shop', 'shop', 'trim|required');
	$this->form_validation->set_rules('event', 'event', 'trim|required');
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');
	$this->form_validation->set_rules('category', 'category', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Shops_settings.php */
/* Location: ./application/controllers/Shops_settings.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:07 */
/* http://harviacode.com */