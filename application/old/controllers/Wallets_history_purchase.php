<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallets_history_purchase extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Wallets_history_purchase_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'wallets_history_purchase/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'wallets_history_purchase/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'wallets_history_purchase/index.html';
            $config['first_url'] = base_url() . 'wallets_history_purchase/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Wallets_history_purchase_model->total_rows($q);
        $wallets_history_purchase = $this->Wallets_history_purchase_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'wallets_history_purchase_data' => $wallets_history_purchase,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('wallets_history_purchase/wallets_history_purchase_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Wallets_history_purchase_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'wallet_history' => $row->wallet_history,
		'ticket' => $row->ticket,
		'price' => $row->price,
		'quantity' => $row->quantity,
	    );
            $this->load->view('wallets_history_purchase/wallets_history_purchase_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_purchase'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('wallets_history_purchase/create_action'),
	    'id' => set_value('id'),
	    'wallet_history' => set_value('wallet_history'),
	    'ticket' => set_value('ticket'),
	    'price' => set_value('price'),
	    'quantity' => set_value('quantity'),
	);
        $this->load->view('wallets_history_purchase/wallets_history_purchase_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'wallet_history' => $this->input->post('wallet_history',TRUE),
		'ticket' => $this->input->post('ticket',TRUE),
		'price' => $this->input->post('price',TRUE),
		'quantity' => $this->input->post('quantity',TRUE),
	    );

            $this->Wallets_history_purchase_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wallets_history_purchase'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Wallets_history_purchase_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wallets_history_purchase/update_action'),
		'id' => set_value('id', $row->id),
		'wallet_history' => set_value('wallet_history', $row->wallet_history),
		'ticket' => set_value('ticket', $row->ticket),
		'price' => set_value('price', $row->price),
		'quantity' => set_value('quantity', $row->quantity),
	    );
            $this->load->view('wallets_history_purchase/wallets_history_purchase_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_purchase'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'wallet_history' => $this->input->post('wallet_history',TRUE),
		'ticket' => $this->input->post('ticket',TRUE),
		'price' => $this->input->post('price',TRUE),
		'quantity' => $this->input->post('quantity',TRUE),
	    );

            $this->Wallets_history_purchase_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wallets_history_purchase'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Wallets_history_purchase_model->get_by_id($id);

        if ($row) {
            $this->Wallets_history_purchase_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wallets_history_purchase'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wallets_history_purchase'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('wallet_history', 'wallet history', 'trim|required');
	$this->form_validation->set_rules('ticket', 'ticket', 'trim|required');
	$this->form_validation->set_rules('price', 'price', 'trim|required|numeric');
	$this->form_validation->set_rules('quantity', 'quantity', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Wallets_history_purchase.php */
/* Location: ./application/controllers/Wallets_history_purchase.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-05 22:10:08 */
/* http://harviacode.com */