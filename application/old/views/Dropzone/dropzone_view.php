<!DOCTYPE html>
<html>
<head>
  <title>Codeigniter - Multiple Image upload using dropzone.js</title>
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>


  <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css">


 <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
-->

 <title>Codeigniter - Multiple Image upload using dropzone.js</title>
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>

 <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/dropzone-master/dist/min/dropzone.min.css' ?>">
 <script src="<?= base_url().'assets/dropzone-master/dist/min/dropzone.min.js' ?>"></script>


  <style>
    /*.content{
      width: 50%;
      padding: 5px;
      margin: 0 auto;
    }
    .content span{
      width: 250px;
    }
    .dz-message{
      text-align: center;
      font-size: 28px;
     background-color: #94CC60;
    }*/

    #dropzone-here {
    text-align: center;
    padding-top: 60px;
    width: 100%;
    position: absolute;
    font-size: 18px;
    font-weight: bold;
    top: 50px;
    background-color: #F0F0F0;
}

    </style>
 <script type="text/javascript">

$('.dropzone')[0].dropzone.files.forEach(function(file) {
    file.previewElement.remove();
});

$('.dropzone').removeClass('dz-started');






   </script>

</head>
<body>


<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2>Codeigniter - Multiple Image upload using dropzone.js</h2>

      <div class='content'>
      <!-- Dropzone -->
      <form action="<?= base_url('Dropzone/imageUploadPost') ?>" class="dropzone" id="dropzone-here">
     <!--  <input name="userfile" type="file" />       -->
     <!--<input type="submit" value="Send File" /> -->
     <!--  <div class="dropzone-here">Drop files here to upload.</div>  -->
     </form>
    </div>

    </div>
  </div>
</div>


<script>
// Get the template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, {
    url: "<?= base_url('Dropzone/imageUploadPost') ?>",
    paramName: "file",
    acceptedFiles: 'image/*',
    maxFilesize: 2,
    maxFiles: 3,
    thumbnailWidth: 160,
    thumbnailHeight: 160,
    thumbnailMethod: 'contain',
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: true,
    previewsContainer: "#previews",
    clickable: ".fileinput-button"
});

myDropzone.on("addedfile", function(file) {
    $('.dropzone-here').hide();
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1";
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
    //document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};

$('#previews').sortable({
    items:'.file-row',
    cursor: 'move',
    opacity: 0.5,
    containment: "parent",
    distance: 20,
    tolerance: 'pointer',
    update: function(e, ui){
        //actions when sorting
    }
});



  //ESTE SEGMENTO DE CODIGO PRETENDE CARGAR LAS IMAGENES DESDE EL ARCHIVO DE IMAGENES
    window.addEventListener('load', function()

             //var thisDropzone = this;

            $.getJSON('<?= base_url('Dropzone/GetImagenUpload') ?>', function(data) { // get the json response

            $.each(data, function(key,value){ //loop through it

                var mockFile = { name: value.name, size: value.size }; // here we get the file name and size as response

                myDropzone.options.addedfile.call(myDropzone, mockFile);

                myDropzone.options.thumbnail.call(myDropzone, mockFile, "./uploads/"+value.name);//uploadsfolder is the folder where you have all those uploaded files

            });

        });



           } , false);


</script>





</body>
</html>



