<!DOCTYPE HTML>

<html>

<head>
  <title>Untitled</title>

   <title>Codeigniter - Multiple Image upload using dropzone.js</title>
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>

 <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/dropzone-master/dist/min/dropzone.min.css' ?>">
 <script src="<?= base_url().'assets/dropzone-master/dist/min/dropzone.min.js' ?>"></script>


 <style type="text/css">
 #previews {
  padding: 15px;
  padding-top: 0px;
  padding-bottom: 0px;
  margin-top: 15px;
  min-height: 220px;
  background-color: #fbfbfb;
}

.dropzone-here {
    text-align: center;
    padding-top: 60px;
    width: 100%;
    position: absolute;
    font-size: 18px;
    font-weight: bold;
    top: 50px;
}

#previews .file-row .delete {
    display: none;
}

#previews .file-row.dz-success .start,
#previews .file-row.dz-success .cancel {
    display: none;
}

#previews .file-row.dz-success .delete {
    display: block;
}

.dz-image-preview {
    border: 1px solid #d6d4d4;
    padding-top: 15px;
    padding-bottom: 15px;
    margin-bottom: 15px;
}

.preview {
    position: relative;
    background: #fff;
    border: 1px solid #dadada;
    text-align: center;
    display: table-cell;
    vertical-align: middle;
}

.preview img {
    cursor: pointer;
}

.progress {
    border: 1px solid #ccc;
    position: relative;
    display: block;
    height: 22px;
    padding: 0;
    min-width: 200px;
    margin:4px 0;
    background: #DEDEDE;
    background: -webkit-gradient(linear, left top, left bottom, from(#ccc), to(#e9e9e9));
    background: -moz-linear-gradient(top, #ccc, #e9e9e9);
    filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#cccccc', endColorstr='#e9e9e9');
    -moz-box-shadow:0 1px 0 #fff;
    -webkit-box-shadow:0 1px 0 #fff;
    box-shadow:0 1px 0 #fff;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

.progress-bar {
    color: #ffffff;
    display: block;
    height: 20px;
    margin: 0;
    padding: 0;
    text-align:center;
    -moz-box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
    -webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
    box-shadow:inset 0 1px 0 rgba(255,255,255,0.5);
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    border: 1px solid #0078a5;
    background-color: #5C9ADE;
    background: -moz-linear-gradient(top, #00adee 10%, #0078a5 90%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0.1, #00adee), color-stop(0.9, #0078a5));
}
 </style>

</head>

<body>


 <form action="<?= base_url('Dropzone/imageUploadPost') ?>" method="post" enctype="multipart/form-data">
    <div class="fallback">
        <input name="file" type="file" multiple />
        <input type="hidden" name="$nombre" value="">
    </div>
    <div id="actions" class="row">
        <div class="col-lg-7">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
            </span>
            <button type="submit" class="btn btn-primary start" style="display: none;">
                <i class="glyphicon glyphicon-upload"></i>
                <span>Start upload</span>
            </button>
            <button type="reset" class="btn btn-warning cancel" style="display: none;">
                <i class="glyphicon glyphicon-ban-circle"></i>
                <span>Cancel upload</span>
            </button>
        </div>

        <div class="col-lg-5">
            <!-- The global file processing state -->
            <span class="fileupload-process">
                <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </span>
        </div>
    </div>

    <div class="table table-striped files" id="previews">
        <div id="template" class="file-row row">
            <!-- This is used as the file preview template -->
            <div class="col-xs-12 col-lg-3">
                <span class="preview" style="width:160px;height:160px;">
                    <img data-dz-thumbnail />
                </span>
                <br/>
                <button class="btn btn-primary start" style="display:none;">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Empezar</span>
                </button>
                <button data-dz-remove class="btn btn-warning cancel">
                    <i class="icon-ban-circle fa fa-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
                <button data-dz-remove class="btn btn-danger delete">
                    <i class="icon-trash fa fa-trash"></i>
                    <span>Eliminar</span>
                </button>
            </div>
            <div class="col-xs-12 col-lg-9">
                <p class="name" data-dz-name></p>
                <p class="size" data-dz-size></p>
                <div>
                    <strong class="error text-danger" data-dz-errormessage></strong>
                </div>
                <div>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                      <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="dropzone-here">Drop files here to upload.</div>
</form>


 <script>
// Get the template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, {
    url: "<?= base_url('Dropzone/imageUploadPost') ?>",
    paramName: "file",
    acceptedFiles: 'image/*',
    maxFilesize: 2,
    maxFiles: 1,
    thumbnailWidth: 160,
    thumbnailHeight: 160,
    thumbnailMethod: 'contain',
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: true,
    previewsContainer: "#previews",
    clickable: ".fileinput-button"
});

myDropzone.on("addedfile", function(file) {
    $('.dropzone-here').hide();
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1";
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
    //document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};

$('#previews').sortable({
    items:'.file-row',
    cursor: 'move',
    opacity: 0.5,
    containment: "parent",
    distance: 20,
    tolerance: 'pointer',
    update: function(e, ui){
        //actions when sorting
    }
});
</script>












</body>

</html>