<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_customs List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('events_customs/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('events_customs/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('events_customs'); ?>" class="btn btn-default">Reiniciar</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">B&uacute;squeda</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
            <th>No</th>
		    <th>Evento</th>
		    <th>Color de fondo</th>
		    <th>Ancho del logo Width</th>
		    <th>Color del bot&oacute;n</th>
		    <th>Color de resaltado</th>
		    <th>Activar</th>
            </tr><?php
            foreach ($events_customs_data as $events_customs)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $events_customs->event ?></td>
			<td><?php echo $events_customs->background_color ?></td>
			<td><?php echo $events_customs->logo_width ?></td>
			<td><?php echo $events_customs->button_color ?></td>
			<td><?php echo $events_customs->highlight_color ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('events_customs/read/'.$events_customs->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('events_customs/update/'.$events_customs->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('events_customs/delete/'.$events_customs->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total de archivos : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </body>
</html>