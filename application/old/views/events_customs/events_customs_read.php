<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_customs Read</h2>
        <table class="table">
	    <tr><td>Evento</td><td><?php echo $event; ?></td></tr>
	    <tr><td>Color del fondo</td><td><?php echo $background_color; ?></td></tr>
	    <tr><td>Ancho del logo</td><td><?php echo $logo_width; ?></td></tr>
	    <tr><td>Color del bot&oacute;n</td><td><?php echo $button_color; ?></td></tr>
	    <tr><td>Color de resaltado</td><td><?php echo $highlight_color; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('events_customs') ?>" class="btn btn-default">Cancelar</a></td></tr>
	</table>
        </body>
</html>