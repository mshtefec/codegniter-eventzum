<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_customs <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Event <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Background Color <?php echo form_error('background_color') ?></label>
            <input type="text" class="form-control" name="background_color" id="background_color" placeholder="Background Color" value="<?php echo $background_color; ?>" />
        </div>
	    <div class="form-group">
            <label for="smallint">Logo Width <?php echo form_error('logo_width') ?></label>
            <input type="text" class="form-control" name="logo_width" id="logo_width" placeholder="Logo Width" value="<?php echo $logo_width; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Button Color <?php echo form_error('button_color') ?></label>
            <input type="text" class="form-control" name="button_color" id="button_color" placeholder="Button Color" value="<?php echo $button_color; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Highlight Color <?php echo form_error('highlight_color') ?></label>
            <input type="text" class="form-control" name="highlight_color" id="highlight_color" placeholder="Highlight Color" value="<?php echo $highlight_color; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events_customs') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>