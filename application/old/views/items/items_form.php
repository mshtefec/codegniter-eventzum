<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Items <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Event <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Company <?php echo form_error('company') ?></label>
            <input type="text" class="form-control" name="company" id="company" placeholder="Company" value="<?php echo $company; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="description">Description <?php echo form_error('description') ?></label>
            <textarea class="form-control" rows="3" name="description" id="description" placeholder="Description"><?php echo $description; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="decimal">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Quantity <?php echo form_error('quantity') ?></label>
            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="<?php echo $quantity; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Sold <?php echo form_error('sold') ?></label>
            <input type="text" class="form-control" name="sold" id="sold" placeholder="Sold" value="<?php echo $sold; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Start Date <?php echo form_error('start_date') ?></label>
            <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Start Date" value="<?php echo $start_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">End Date <?php echo form_error('end_date') ?></label>
            <input type="text" class="form-control" name="end_date" id="end_date" placeholder="End Date" value="<?php echo $end_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Vip <?php echo form_error('vip') ?></label>
            <input type="text" class="form-control" name="vip" id="vip" placeholder="Vip" value="<?php echo $vip; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('items') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>