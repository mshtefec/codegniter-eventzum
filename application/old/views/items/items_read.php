<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Items Read</h2>
        <table class="table">
	    <tr><td>Event</td><td><?php echo $event; ?></td></tr>
	    <tr><td>Company</td><td><?php echo $company; ?></td></tr>
	    <tr><td>Name</td><td><?php echo $name; ?></td></tr>
	    <tr><td>Description</td><td><?php echo $description; ?></td></tr>
	    <tr><td>Price</td><td><?php echo $price; ?></td></tr>
	    <tr><td>Quantity</td><td><?php echo $quantity; ?></td></tr>
	    <tr><td>Sold</td><td><?php echo $sold; ?></td></tr>
	    <tr><td>Start Date</td><td><?php echo $start_date; ?></td></tr>
	    <tr><td>End Date</td><td><?php echo $end_date; ?></td></tr>
	    <tr><td>Vip</td><td><?php echo $vip; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('items') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>