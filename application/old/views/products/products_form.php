<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Products <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Shop <?php echo form_error('shop') ?></label>
            <input type="text" class="form-control" name="shop" id="shop" placeholder="Shop" value="<?php echo $shop; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category <?php echo form_error('category') ?></label>
            <input type="text" class="form-control" name="category" id="category" placeholder="Category" value="<?php echo $category; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Name <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Description <?php echo form_error('description') ?></label>
            <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Tax <?php echo form_error('tax') ?></label>
            <input type="text" class="form-control" name="tax" id="tax" placeholder="Tax" value="<?php echo $tax; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Stock <?php echo form_error('stock') ?></label>
            <input type="text" class="form-control" name="stock" id="stock" placeholder="Stock" value="<?php echo $stock; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Celiac <?php echo form_error('celiac') ?></label>
            <input type="text" class="form-control" name="celiac" id="celiac" placeholder="Celiac" value="<?php echo $celiac; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Vegetarian <?php echo form_error('vegetarian') ?></label>
            <input type="text" class="form-control" name="vegetarian" id="vegetarian" placeholder="Vegetarian" value="<?php echo $vegetarian; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Active <?php echo form_error('active') ?></label>
            <input type="text" class="form-control" name="active" id="active" placeholder="Active" value="<?php echo $active; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('products') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>