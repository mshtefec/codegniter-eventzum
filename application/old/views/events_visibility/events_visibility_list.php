<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_visibility List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('events_visibility/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('events_visibility/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('events_visibility'); ?>" class="btn btn-default">Reiniciar</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">B&uacute;squeda</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Evento</th>
		<th>Art&iacute;stas</th>
		<th>Programa</th>
		<th>Im&aacute;genes</th>
		<th>Mapa</th>
		<th>Preguntas m&aacute;s frecuentes</th>
		<th>Activar</th>
            </tr><?php
            foreach ($events_visibility_data as $events_visibility)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $events_visibility->event ?></td>
			<td><?php echo $events_visibility->artists ?></td>
			<td><?php echo $events_visibility->schedule ?></td>
			<td><?php echo $events_visibility->images ?></td>
			<td><?php echo $events_visibility->map ?></td>
			<td><?php echo $events_visibility->faq ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('events_visibility/read/'.$events_visibility->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('events_visibility/update/'.$events_visibility->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('events_visibility/delete/'.$events_visibility->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total de archivos : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </body>
</html>