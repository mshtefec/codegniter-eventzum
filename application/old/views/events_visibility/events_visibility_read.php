<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_visibility Read</h2>
        <table class="table">
	    <tr><td>Evento</td><td><?php echo $event; ?></td></tr>
	    <tr><td>Art&iacute;stas</td><td><?php echo $artists; ?></td></tr>
	    <tr><td>Programa</td><td><?php echo $schedule; ?></td></tr>
	    <tr><td>Im&aacute;genes</td><td><?php echo $images; ?></td></tr>
	    <tr><td>Mapa</td><td><?php echo $map; ?></td></tr>
	    <tr><td>Preguntas m&aacute;s frecuentes</td><td><?php echo $faq; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('events_visibility') ?>" class="btn btn-default">Cancelar</a></td></tr>
	</table>
        </body>
</html>