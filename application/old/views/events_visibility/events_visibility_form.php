<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_visibility <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Evento <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Artistas <?php echo form_error('artists') ?></label>
            <input type="text" class="form-control" name="artists" id="artists" placeholder="Artists" value="<?php echo $artists; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Programa <?php echo form_error('schedule') ?></label>
            <input type="text" class="form-control" name="schedule" id="schedule" placeholder="Schedule" value="<?php echo $schedule; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Imagenes <?php echo form_error('images') ?></label>
            <input type="text" class="form-control" name="images" id="images" placeholder="Images" value="<?php echo $images; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Mapa <?php echo form_error('map') ?></label>
            <input type="text" class="form-control" name="map" id="map" placeholder="Map" value="<?php echo $map; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Preguntas m&aacute;s frecuentes <?php echo form_error('faq') ?></label>
            <input type="text" class="form-control" name="faq" id="faq" placeholder="Faq" value="<?php echo $faq; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events_visibility') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>