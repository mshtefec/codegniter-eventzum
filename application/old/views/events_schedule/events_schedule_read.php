<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_schedule Read</h2>
        <table class="table">
	    <tr><td>Evento</td><td><?php echo $event; ?></td></tr>
	    <tr><td>T&iacute;tulo</td><td><?php echo $title; ?></td></tr>
	    <tr><td>Fecha de inicio</td><td><?php echo $start; ?></td></tr>
	    <tr><td>Fecha de cierre</td><td><?php echo $end; ?></td></tr>
	    <tr><td>Artista</td><td><?php echo $artist; ?></td></tr>
	    <tr><td>Ubicaci&oacute;n</td><td><?php echo $location; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('events_schedule') ?>" class="btn btn-default">Cancelar</a></td></tr>
	</table>
        </body>
</html>