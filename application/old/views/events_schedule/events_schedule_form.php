<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_schedule <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Evento <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">T&iacute;tulo <?php echo form_error('title') ?></label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Fecha de inicio <?php echo form_error('start') ?></label>
            <input type="text" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Fecha de cierre <?php echo form_error('end') ?></label>
            <input type="text" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Artista <?php echo form_error('artist') ?></label>
            <input type="text" class="form-control" name="artist" id="artist" placeholder="Artist" value="<?php echo $artist; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ubicaci&oacute;n <?php echo form_error('location') ?></label>
            <input type="text" class="form-control" name="location" id="location" placeholder="Location" value="<?php echo $location; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events_schedule') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>