<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Shops Read</h2>
        <table class="table">
	    <tr><td>Business</td><td><?php echo $business; ?></td></tr>
	    <tr><td>Nif</td><td><?php echo $nif; ?></td></tr>
	    <tr><td>Representative</td><td><?php echo $representative; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Phone</td><td><?php echo $phone; ?></td></tr>
	    <tr><td>Password</td><td><?php echo $password; ?></td></tr>
	    <tr><td>Date</td><td><?php echo $date; ?></td></tr>
	    <tr><td>Event</td><td><?php echo $event; ?></td></tr>
	    <tr><td>Address</td><td><?php echo $address; ?></td></tr>
	    <tr><td>Zipcode</td><td><?php echo $zipcode; ?></td></tr>
	    <tr><td>City</td><td><?php echo $city; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('shops') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>