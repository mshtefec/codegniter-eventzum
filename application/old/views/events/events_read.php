<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Eventos Le&icute;dos</h2>
        <table class="table">
	    <tr><td>Categor&iacute;a</td><td><?php echo $category; ?></td></tr>
	    <tr><td>Nombre</td><td><?php echo $name; ?></td></tr>
	    <tr><td>Direcci&ocute;n</td><td><?php echo $address; ?></td></tr>
	    <tr><td>Ciudad</td><td><?php echo $city; ?></td></tr>
	    <tr><td>C&oacute;digo Postal</td><td><?php echo $zipcode; ?></td></tr>
	    <tr><td>Correo</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Tel&eacute;fono</td><td><?php echo $phone; ?></td></tr>
	    <tr><td>Contrase&ntilde;a</td><td><?php echo $password; ?></td></tr>
	    <tr><td>Fecha de Inicio</td><td><?php echo $start_date->format('d.m.Y'); ?></td></tr>
	    <tr><td>Fecha de Cierre</td><td><?php echo $end_date->format('d.m.Y'); ?></td></tr>
	    <tr><td>Activar</td><td><?php echo $active; ?></td></tr>
	    <tr><td>Fecha</td><td><?php echo $date->format('d.m.Y'); ?></td></tr>
	    <tr><td>Empresa</td><td><?php echo $company; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('events') ?>" class="btn btn-default">Cancelar</a></td></tr>
	</table>
        </body>
</html>