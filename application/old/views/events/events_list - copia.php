 <div id="content-wrapper">

        <div class="container-fluid">

        <h2 style="margin-top:0px"></h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('events/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('events/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('events'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>


        <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Events List</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                           <th>No</th>
                            <th>Category</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                          <!--  <th>Zipcode</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Password</th>-->
                            <th>Start Date</th>
                            <th>End Date</th>
                            <!--<th>Active</th>
                            <th>Date</th>
                            <th>Company</th>-->
                            <th>Action</th>
                    </tr>
                  </thead>
                 <tbody>
       <?php
            foreach ($events_data as $events)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $events->category ?></td>
			<td><?php echo $events->name ?></td>
			<td><?php echo $events->address ?></td>
			<td><?php echo $events->city ?></td>
		   <!--	<td><?php echo $events->zipcode ?></td>
			<td><?php echo $events->email ?></td>
			<td><?php echo $events->phone ?></td>
			<td><?php echo $events->password ?></td>-->
			<td><?php echo date_format(new DateTime($events->start_date), 'd/m/y') ?></td>
			<td><?php echo date_format(new DateTime( $events->end_date), 'd/m/y') ?></td>
		    <!--	<td><?php echo $events->active ?></td>
			<td><?php echo    date_format( $events->date, 'd/m/y');  ?></td>
			<td><?php echo $events->company ?></td>-->
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('events/read/'.$events->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('events/update/'.$events->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('events/delete/'.$events->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
            </tbody>
        </table>

        <div class="table-responsive>
            <div class="card mb-3>
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	        </div>
            <div class="card mb-3">
                <?php echo $pagination ?>
            </div>
        </div>
<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
 </div>

<!--<div class="container-fluid">-->
