<div id="content-wrapper">

        <div class="container-fluid">
          <!-- Cabecera del contenido -->

    <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<MARCO DE TARJETA>>>>>>>>>>>>>>>>>>>>>< -->
    <div id="eventos" class="tab-pane fade in active">
    <div class="card card-form mx-auto mt-5" style="width:75%;  bottom: 3rem;">
        <div class="card-header">Events <?php echo $button ?></div>
        <div class="card-body">
        <h2 style="margin-top:0px"></h2>

      <!-- script con funcion en ajax para serializar el formulario y procesarlo en un php externo -->
      <script type="text/javascript">
       $(document).ready(function() {

            $('#bt').click(function(){

                var dataString = $('#formulario').serialize();
                var url=$('#url').val();
                var categorias=$('#categorias').val();
                alert('Datos serializados: '+url);

                $.ajax({

                    type: "POST",
                    url: url,
                    data: dataString,
                    beforedSend:function(){
                      $("#salida").html('procesando');
                     },
                    success: function(response) {
                   		 $("#salida").html(response);

               			 }


                });
                 window.location.href = categorias;

                //  window.location.href = url;

            });
          });
        </script>   <!-- fin del script ajax -->


        <h2 style="margin-top:0px"></h2>
        <div class="row align-items-start" >
    <div class="col-sm"> <!-- Formulario -->

       <form id="formulario"  name="formulario"   action="<?php echo $action; ?>" method="post">

      <div class="form-group ">
            <label for="int">Categor&iacute;a <?php echo form_error('category') ?></label>

              <select name="category" id="category" class="form-control">
              <?php
               foreach($categoriasT as $fila)
               {
               ?>
               <option value="<?=$fila->id ?>"><?=$fila->name ?></option>
               <?php
               }
               ?>
               </select>


        </div>
         <div class="form-group">
            <label for="int">Empresa <?php echo form_error('company') ?></label>
          <!--  <input type="number" class="form-control" name="company" id="company" placeholder="Company" value="<?php echo $company; ?>" />      -->
              <select name="companies" id="companies" class="form-control">
              <?php
               foreach($companiesT as $fila)
               {
               ?>
               <option value="<?=$fila->id ?>"><?=$fila->name ?></option>
               <?php
               }
               ?>
               </select>

        </div>

      <div class="form-group">
            <label for="varchar">Nombre <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
      </div>
      <div class="form-group">
            <label for="varchar">Direcci&oacute;n <?php echo form_error('address') ?></label>
            <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $address; ?>" />
      </div>
      <div class="form-group">
            <label for="varchar">Ciudad <?php echo form_error('city') ?></label>
            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $city; ?>" />
      </div>
      <div class="form-group">
            <label for="varchar">C&ocute;digo Postal<?php echo form_error('zipcode') ?></label>
            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zipcode" value="<?php echo $zipcode; ?>" />
      </div>
      <div class="form-group">
            <label for="varchar">Correo <?php echo form_error('email') ?></label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
      </div>
      <div class="form-group">
            <label for="varchar">Tel&eacute;fono <?php echo form_error('phone') ?></label>
            <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
      </div>

      <div class="form-group">
            <label for="int">Fecha de Inicio <?php echo form_error('start_date') ?></label>
            <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Start Date" value="<?php echo $start_date; ?>" />
      </div>
      <div class="form-group">
            <label for="int">Fecha de Cierre <?php echo form_error('end_date') ?></label>
            <input type="date" class="form-control" name="end_date" id="end_date" placeholder="End Date" value="<?php echo $end_date; ?>" />
      </div>
     <!-- <div class="form-group">
            <label for="tinyint">Active <?php echo form_error('active') ?></label>
            <input type="text" class="form-control" name="active" id="active" placeholder="Active" value="<?php echo $active; ?>" />
        </div>-->
      <div class="form-group">
            <label for="int">Fecha <?php echo form_error('date') ?></label>
            <input type="date" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
      </div>

      <input type="hidden" name="id" value="<?php echo $id; ?>" />
      <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
      <a href="<?php echo site_url('events') ?>" class="btn btn-default">Cancelar</a>
  </form>


    </div>

    <!-- botonera -->
    <div class="col-sm button-wrapper ">
            <div class="col-sm">
            <button class="btn btn-primary btn-sm" type="submit" >Artistas</button>
            </div>
            <div class="col-sm">
               <button class="btn btn-primary btn-sm" type="submit">Agenda</button>
            </div>
            <div class="col-sm">
            <button class="btn btn-primary btn-sm" type="submit">Personalizar</button>
            </div>
            <div class="col-sm">
              <button class="btn btn-primary btn-sm" type="submit">Informaci&oacute;n</button>
            </div>
            <div class="col-sm">
            <button class="btn btn-primary btn-sm" type="submit">Visibilidad</button>
            </div>
        </div>    <!-- termina la botonera -->


         </div>
         </div>
         </div> <!-- cerramos el div con estylo cart-form-->








         <!-- Pie del contendio -->
        </div>
        <!-- /.container-fluid -->