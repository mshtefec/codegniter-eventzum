<div id="content-wrapper" >

        <div class="container-fluid ">


      <!-- <div id="reductor">   -->



        <div class="card card-form mx-auto mt-5 " style="width:75%;  bottom: 3rem;">
        <div class="card-header">Usuarios <?php echo $button ?></div>
        <div class="card-body ">
        <h2 style="margin-top:0px"></h2>


    <br>
    <br>

    <br>
        
        <form action="<?php echo $action; ?>" method="post">

          <div class="form-group" style="-webkit-box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);
-moz-box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);
box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);">
            <!--<label for="varchar">Avatar</label>     -->

        <!--    <img src="<?php //echo $Imagen; ?>" width="100" height="100" alt="" class="rounded-circle shadow  bg-white"" > -->

            <input name="file" type="file" />
    <!--        <input name="file" type="file"  name="avatar" id="avatar" placeholder="avatar"  /> -->

  </div>


        <div id="capa" class="form-group"></div>

	    <div class="form-group">
            <label for="varchar">Nombre <?php echo form_error('name') ?></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Apellidos <?php echo form_error('lastname') ?></label>
            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Lastname" value="<?php echo $lastname; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Tel&eacute;fono <?php echo form_error('phone') ?></label>
            <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Direcci&oacute;n <?php echo form_error('address') ?></label>
            <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $address; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ciudad <?php echo form_error('city') ?></label>
            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $city; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">C&oacute;digo Postal <?php echo form_error('zipcode') ?></label>
            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zipcode" value="<?php echo $zipcode; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Correo <?php echo form_error('email') ?></label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Contrase&ntilde;a <?php echo form_error('password') ?></label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Cumplea&ntilde;os <?php echo form_error('date') ?></label>
            <input type="date" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Activar <?php echo form_error('active') ?></label>
            <input type="text" class="form-control" name="active" id="active" placeholder="Active" value="<?php echo $active; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Cliente de Stripe<?php echo form_error('stripe_customer') ?></label>
            <input type="text" class="form-control" name="stripe_customer" id="stripe_customer" placeholder="Stripe Customer" value="<?php echo $stripe_customer; ?>" />
        </div>
         <div class="form-group">
            <label for="varchar">Tipo <?php echo form_error('tipo') ?></label>
           <!-- <input type="text" class="form-control" name="tipo"
            id="tipo" placeholder="tipo"
            value="<?php // if ($tipo=="Superusuario"){echo "selected='selected'"}; ?>" />
             -->

            <select name="tipo" size="1" class="form-control" name="tipo" id="tipo" placeholder="tipo" >
            <option value="Administrador" <?php if ($tipo=="Administrador"){echo "selected='selected'";} ?>  >Administrador</option>
            <option value="Promotor" <?php if ($tipo=="Promotor"){echo "selected='selected'";} ?> >Promotor</option>
            <option value="Vendedor" <?php if ($tipo=="Vendedor"){echo "selected='selected'";} ?> >Vendedor</option>
            <option value="Usuario" <?php if ($tipo=="Usuario"){echo "selected='selected'";} ?> >Usuario</option>
            </select>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
	    <a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancelar</a>
	</form>



       <!--</div> -->


             </div> <!-- parte inferior de las capas -->
             </div>

            </div> <!-- cerramos el div con estylo cart-form-->


        </div>
        <!-- /.container-fluid -->

    <!-- Cargamos el formulario para cargar imagenes -->
        <script>
        $(document).ready(function() {
             $("#capa").load("<?= site_url('Dropzone')?>", function(response, status, xhr) {
              if (status == "error") {
                var msg = "Error!, algo ha sucedido: ";
                $("#capa").html(msg + xhr.status + " " + xhr.statusText);
              }
            });

      });
    </script>
