   <div id="content-wrapper">

        <div class="container-fluid">

        <h2 style="margin-top:0px">Lista de usuarios :</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('users/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('users/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('users'); ?>" class="btn btn-default">Reiniciar</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">B&uacute;squeda</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
        <tr>
        <th>No</th>
		<th>Nombre</th>
		<th>Apellidos</th>
		<th>Tel&eacute;fono</th>
		<th>Direcci&oacute;n</th>
		<th>Ciudad</th>
		<th>C&oacute;digo Postal</th>
		<th>Correo</th>
		<th>Contrase&ntilde;a</th>
		<th>Fecha</th>
		<th>Activar</th>
		<th>Cliente de Stripe</th>
		<th>Activar</th>
        </tr><?php
            foreach ($users_data as $users)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $users->name ?></td>
			<td><?php echo $users->lastname ?></td>
			<td><?php echo $users->phone ?></td>
			<td><?php echo $users->address ?></td>
			<td><?php echo $users->city ?></td>
			<td><?php echo $users->zipcode ?></td>
			<td><?php echo $users->email ?></td>
			<td><?php echo $users->password ?></td>
			<td><?php echo $users->date ?></td>
			<td><?php echo $users->active ?></td>
			<td><?php echo $users->stripe_customer ?></td>
			<td style="text-align:center" width="200px">
				<?php
				echo anchor(site_url('users/read/'.$users->id),'Read');
				echo ' | ';
				echo anchor(site_url('users/update/'.$users->id),'Update');
				echo ' | ';
				echo anchor(site_url('users/delete/'.$users->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total de archivos : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>

     </div>

   </div>
                <!-- /.container-fluid -->