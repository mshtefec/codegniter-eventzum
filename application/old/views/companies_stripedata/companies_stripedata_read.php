<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Companies_stripedata Read</h2>
        <table class="table">
	    <tr><td>Company</td><td><?php echo $company; ?></td></tr>
	    <tr><td>Account</td><td><?php echo $account; ?></td></tr>
	    <tr><td>External Account</td><td><?php echo $external_account; ?></td></tr>
	    <tr><td>Business Logo</td><td><?php echo $business_logo; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('companies_stripedata') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>