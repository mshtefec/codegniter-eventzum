<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Companies_stripedata <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Company <?php echo form_error('company') ?></label>
            <input type="text" class="form-control" name="company" id="company" placeholder="Company" value="<?php echo $company; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Account <?php echo form_error('account') ?></label>
            <input type="text" class="form-control" name="account" id="account" placeholder="Account" value="<?php echo $account; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">External Account <?php echo form_error('external_account') ?></label>
            <input type="text" class="form-control" name="external_account" id="external_account" placeholder="External Account" value="<?php echo $external_account; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Business Logo <?php echo form_error('business_logo') ?></label>
            <input type="text" class="form-control" name="business_logo" id="business_logo" placeholder="Business Logo" value="<?php echo $business_logo; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('companies_stripedata') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>