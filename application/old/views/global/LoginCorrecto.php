<div id="content-wrapper">

        <div class="container-fluid">


           <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                  </div>
                  <div class="mr-5">Se ha iniciado la sesi&oacute;n de <?php echo $Tipo ?> correctamente</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                  <span class="float-left">Ver detalles :</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>

        </div>
        <!-- /.container-fluid -->