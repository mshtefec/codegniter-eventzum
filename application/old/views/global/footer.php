   <!-- Sticky Footer -->
    <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
            <span>Eventzum </span>
            &copy;<?php echo date("Y"); ?>
            <span> Copyright.</span>
            </div>
          </div>
    </footer>
    </div>
    <!-- /#wrapper -->
    </div>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cerrar la sesi&oacute;n?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Salir" si desea cerrar su sesi&oacute;n.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="login.html">Salir</a>
          </div>
        </div>
      </div>
    </div>


   <!-- Core plugin JavaScript-->
   <!-- <script src="<?= base_url();?>assets/jquery-easing/jquery.easing.min.js"></script>      -->

    <!-- Page level plugin JavaScript-->
    <script src="<?= base_url();?>assets/chart.js/Chart.min.js"></script>
    <script src="<?= base_url();?>assets/datatables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>assets/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url();?>assets/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="<?= base_url();?>assets/js/demo/datatables-demo.js"></script>
    <script src="<?= base_url();?>assets/js/demo/chart-area-demo.js"></script>

    <!-- Librerias para arrastrar archivos -->

    <script src="<?= base_url();?>assets/dropzone-master/dist/min/dropzone.min.js "></script>
    <!-- <script src="<?= base_url();?>assets/DropzoneVideo1/dropzone.js"></script>
    <script src="<?= base_url();?>assets/DropzoneVideo1/dropzone-config.js"></script>
    -->


  </body>

</html>
