<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Users_stripecards <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Usuario <?php echo form_error('user') ?></label>
            <input type="text" class="form-control" name="user" id="user" placeholder="User" value="<?php echo $user; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Tarjeta <?php echo form_error('card') ?></label>
            <input type="text" class="form-control" name="card" id="card" placeholder="Card" value="<?php echo $card; ?>" />
        </div>
	    <div class="form-group">
            <label for="smallint">Ultimos 4 d&iacute;gitos <?php echo form_error('last4') ?></label>
            <input type="text" class="form-control" name="last4" id="last4" placeholder="Last4" value="<?php echo $last4; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Marca <?php echo form_error('brand') ?></label>
            <input type="text" class="form-control" name="brand" id="brand" placeholder="Brand" value="<?php echo $brand; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Mes de caducidad <?php echo form_error('exp_month') ?></label>
            <input type="text" class="form-control" name="exp_month" id="exp_month" placeholder="Exp Month" value="<?php echo $exp_month; ?>" />
        </div>
	    <div class="form-group">
            <label for="smallint">A&ntilde;o de caducidad <?php echo form_error('exp_year') ?></label>
            <input type="text" class="form-control" name="exp_year" id="exp_year" placeholder="Exp Year" value="<?php echo $exp_year; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('users_stripecards') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>