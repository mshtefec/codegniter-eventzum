<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Users_tickets List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('users_tickets/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('users_tickets/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('users_tickets'); ?>" class="btn btn-default">Reiniciar</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">B&uacute;squeda</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
        <tr>
        <th>No</th>
		<th>Usuario</th>
		<th>Entrada</th>
		<th>Evento</th>
		<th>C&oacute;digo</th>
		<th>Utilizado</th>
		<th>Activar</th>
            </tr><?php
            foreach ($users_tickets_data as $users_tickets)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $users_tickets->user ?></td>
			<td><?php echo $users_tickets->ticket ?></td>
			<td><?php echo $users_tickets->event ?></td>
			<td><?php echo $users_tickets->code ?></td>
			<td><?php echo $users_tickets->used ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('users_tickets/read/'.$users_tickets->id),'Read'); 
				echo ' | '; 
				echo anchor(site_url('users_tickets/update/'.$users_tickets->id),'Update'); 
				echo ' | '; 
				echo anchor(site_url('users_tickets/delete/'.$users_tickets->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total de archivos : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </body>
</html>