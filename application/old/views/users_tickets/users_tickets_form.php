<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Users_tickets <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Usuario <?php echo form_error('user') ?></label>
            <input type="text" class="form-control" name="user" id="user" placeholder="User" value="<?php echo $user; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Entrada <?php echo form_error('ticket') ?></label>
            <input type="text" class="form-control" name="ticket" id="ticket" placeholder="Ticket" value="<?php echo $ticket; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Evento <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="bigint">C&oacute;digo <?php echo form_error('code') ?></label>
            <input type="text" class="form-control" name="code" id="code" placeholder="Code" value="<?php echo $code; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Utilizado <?php echo form_error('used') ?></label>
            <input type="text" class="form-control" name="used" id="used" placeholder="Used" value="<?php echo $used; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('users_tickets') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>