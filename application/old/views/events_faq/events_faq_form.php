<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_faq <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Evento <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Pregunta <?php echo form_error('question') ?></label>
            <input type="text" class="form-control" name="question" id="question" placeholder="Question" value="<?php echo $question; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Respuesta <?php echo form_error('answer') ?></label>
            <input type="text" class="form-control" name="answer" id="answer" placeholder="Answer" value="<?php echo $answer; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events_faq') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>