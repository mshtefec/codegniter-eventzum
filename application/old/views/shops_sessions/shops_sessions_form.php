<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Shops_sessions <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Ssid <?php echo form_error('ssid') ?></label>
            <input type="text" class="form-control" name="ssid" id="ssid" placeholder="Ssid" value="<?php echo $ssid; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Shop <?php echo form_error('shop') ?></label>
            <input type="text" class="form-control" name="shop" id="shop" placeholder="Shop" value="<?php echo $shop; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('shops_sessions') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>