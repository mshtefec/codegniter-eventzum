<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Events_users <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Evento <?php echo form_error('event') ?></label>
            <input type="text" class="form-control" name="event" id="event" placeholder="Event" value="<?php echo $event; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Usuario <?php echo form_error('user') ?></label>
            <input type="text" class="form-control" name="user" id="user" placeholder="User" value="<?php echo $user; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Entrada <?php echo form_error('uticket') ?></label>
            <input type="text" class="form-control" name="uticket" id="uticket" placeholder="Uticket" value="<?php echo $uticket; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Dentro <?php echo form_error('inside') ?></label>
            <input type="text" class="form-control" name="inside" id="inside" placeholder="Inside" value="<?php echo $inside; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('events_users') ?>" class="btn btn-default">Cancelar</a>
	</form>
    </body>
</html>