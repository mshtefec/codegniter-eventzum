<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Wallets_history_purchase <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Wallet History <?php echo form_error('wallet_history') ?></label>
            <input type="text" class="form-control" name="wallet_history" id="wallet_history" placeholder="Wallet History" value="<?php echo $wallet_history; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Ticket <?php echo form_error('ticket') ?></label>
            <input type="text" class="form-control" name="ticket" id="ticket" placeholder="Ticket" value="<?php echo $ticket; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Price <?php echo form_error('price') ?></label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $price; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Quantity <?php echo form_error('quantity') ?></label>
            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="<?php echo $quantity; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('wallets_history_purchase') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>