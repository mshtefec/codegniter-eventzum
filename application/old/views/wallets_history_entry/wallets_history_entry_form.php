<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Wallets_history_entry <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Wallet History <?php echo form_error('wallet_history') ?></label>
            <input type="text" class="form-control" name="wallet_history" id="wallet_history" placeholder="Wallet History" value="<?php echo $wallet_history; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Card <?php echo form_error('card') ?></label>
            <input type="text" class="form-control" name="card" id="card" placeholder="Card" value="<?php echo $card; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Charge <?php echo form_error('charge') ?></label>
            <input type="text" class="form-control" name="charge" id="charge" placeholder="Charge" value="<?php echo $charge; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('wallets_history_entry') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>