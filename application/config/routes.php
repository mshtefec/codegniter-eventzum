<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//$route['default_controller'] ="PantInicio";

$route['default_controller'] = 'indexController';

$route['configuracion'] = 'front/configController/index';
$route['configuracion/avatar'] = 'front/configController/avatar';
$route['configuracion/cuenta'] = 'front/configController/cuenta';
$route['configuracion/acceso'] = 'front/configController/acceso';
$route['configuracion/notificaciones'] = 'front/configController/notificaciones';
$route['configuracion/suscripcion'] = 'front/configController/suscripcion';
$route['configuracion/pagos'] = 'front/configController/pagos';
$route['configuracion/metodo_pago'] = 'front/configController/metodo_pago';
$route['configuracion/stripe'] = 'front/configController/stripe_card';

$route['carrito'] = 'front/carritoController/index';
$route['carrito/quitar-evento/(:any)'] = 'front/carritoController/quitar_carrito/$1';
$route['carrito/compra'] = 'front/carritoController/carrito_compra';
$route['carrito/sucursales'] = 'front/carritoController/carrito_sucursales';
$route['carrito/compra-productos'] = 'front/carritoController/carrito_productos';
$route['carrito/quitar-producto/(:any)'] = 'front/carritoController/quitar_producto_carrito/$1';

$route['campo-virtual'] = 'front/campoVirtualController/index';

$route['login'] = 'api/autenticacionController/login';
$route['logout'] = 'api/autenticacionController/logout';
$route['registro'] = 'api/autenticacionController/registro';
$route['recupero'] = 'api/autenticacionController/recupero';

$route['admin'] = 'admin/adminController/admin';

  $route['admin/usuarios'] = 'admin/usuarioController';
  $route['admin/usuarios/nuevo'] = 'admin/usuarioController/nuevo';
  $route['admin/usuarios/editar/(:any)'] = 'admin/usuarioController/editar/$1';
  $route['admin/usuarios/borrar/(:any)'] = 'admin/usuarioController/usuario_borrar/$1';
  $route['admin/usuarios/cambiar/(:any)'] = 'admin/usuarioController/cambiarPass/$1';

  $route['admin/empresas'] = 'admin/empresaController';
  $route['admin/empresas/nuevo'] = 'admin/empresaController/nuevo';
  $route['admin/empresas/editar/(:any)'] = 'admin/empresaController/editar/$1';
  $route['admin/empresas/borrar/(:any)'] = 'admin/empresaController/empresa_borrar/$1';

  $route['admin/eventos'] = 'admin/eventoController';
  $route['admin/eventos/nuevo'] = 'admin/eventoController/nuevo';
  $route['admin/eventos/editar/(:any)'] = 'admin/eventoController/editar/$1';
  $route['admin/eventos/borrar/(:any)'] = 'admin/eventoController/evento_borrar/$1';
  $route['admin/eventos/asientos'] = 'admin/eventoController/asientos/$1';
  $route['admin/eventos/SelectAsientos'] = 'admin/eventoController/SelectAsientos';


  $route['admin/eventos/categorias'] = 'admin/eventoController/categoria_index';
  $route['admin/eventos/categorias/nuevo'] = 'admin/eventoController/categoria_nuevo';
  $route['admin/eventos/categorias/editar/(:any)'] = 'admin/eventoController/categoria_editar/$1';
  $route['admin/eventos/categorias/borrar/(:any)'] = 'admin/eventoController/categoria_borrar/$1';

  $route['admin/eventos/subcategorias'] = 'admin/eventoController/subcategoria_index';
  $route['admin/eventos/subcategorias/nuevo'] = 'admin/eventoController/subcategoria_nuevo';
  $route['admin/eventos/subcategorias/editar/(:any)'] = 'admin/eventoController/subcategoria_editar/$1';
  $route['admin/eventos/subcategorias/borrar/(:any)'] = 'admin/eventoController/subcategoria_borrar/$1';

  $route['admin/eventos/participantes'] = 'admin/eventoController/participante_index';
  $route['admin/eventos/participantes/nuevo'] = 'admin/eventoController/participante_nuevo';
  $route['admin/eventos/participantes/editar/(:any)'] = 'admin/eventoController/participante_editar/$1';
  $route['admin/eventos/participantes/borrar/(:any)'] = 'admin/eventoController/participante_borrar/$1';

  $route['admin/tiendas'] = 'admin/tiendaController';
  $route['admin/tiendas/nuevo'] = 'admin/tiendaController/nuevo';
  $route['admin/tiendas/editar/(:any)'] = 'admin/tiendaController/editar/$1';
  $route['admin/tiendas/borrar/(:any)'] = 'admin/tiendaController/tienda_borrar/$1';

  $route['admin/tiendas/sucursales'] = 'admin/sucursalController';
  $route['admin/tiendas/sucursales/nuevo'] = 'admin/sucursalController/nuevo';
  $route['admin/tiendas/sucursales/editar/(:any)'] = 'admin/sucursalController/editar/$1';
  $route['admin/tiendas/sucursales/borrar/(:any)'] = 'admin/sucursalController/sucursal_borrar/$1';

  $route['admin/productos'] = 'admin/productoController';
  $route['admin/productos/nuevo'] = 'admin/productoController/nuevo';
  $route['admin/productos/editar/(:any)'] = 'admin/productoController/editar/$1';
  $route['admin/productos/borrar/(:any)'] = 'admin/productoController/producto_borrar/$1';

  $route['admin/productos/categorias'] = 'admin/productoController/categoria_index';
  $route['admin/productos/categorias/nuevo'] = 'admin/productoController/categoria_nuevo';
  $route['admin/productos/categorias/editar/(:any)'] = 'admin/productoController/categoria_editar/$1';
  $route['admin/productos/categorias/borrar/(:any)'] = 'admin/productoController/categoria_borrar/$1';

  $route['admin/productos/subcategorias'] = 'admin/productoController/subcategoria_index';
  $route['admin/productos/subcategorias/nuevo'] = 'admin/productoController/subcategoria_nuevo';
  $route['admin/productos/subcategorias/editar/(:any)'] = 'admin/productoController/subcategoria_editar/$1';
  $route['admin/productos/subcategorias/borrar/(:any)'] = 'admin/productoController/subcategoria_borrar/$1';

  $route['admin/estadios'] = 'admin/estadioController';
  $route['admin/estadios/nuevo'] = 'admin/estadioController/nuevo';
  $route['admin/estadios/editar/(:any)'] = 'admin/estadioController/editar/$1';
  $route['admin/estadios/borrar/(:any)'] = 'admin/estadioController/estadio_borrar/$1';

  $route['admin/pagosStripe'] = 'admin/pagosStripeController';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['image-upload'] = 'Dropzone';
$route['image-upload/post']['post'] = 'Dropzone/imageUploadPost';


//$route['global'] ='application/views/global/';

      /*
    $route['Video'] = "Videoconferencias";
    $route['VideoCreate']['post'] = "Videoconferencias/store";
    $route['VideoEdit/(:any)'] = "Videocoferencia/edit/$1";
    $route['VideoUpdate/(:any)']['put'] = "Videoconferencia/update/$1";
    $route['VideoDelete/(:any)']['delete'] = "Videoconferencia/delete/$1";*/