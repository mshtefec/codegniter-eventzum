<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Evento extends CI_Model
{

    public $table = 'evento';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function findAllByIdUser($id) {
        $this->db->where('usuario_id', $id);

        return $this->db->get($this->table)->result();
    }

    function findAll() {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    function findById($id) {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('nombre', $q);
        $this->db->or_like('direccion', $q);
        $this->db->or_like('ciudad', $q);
        $this->db->or_like('codigo_postal', $q);
        $this->db->or_like('correo', $q);
        $this->db->or_like('telefono', $q);
        $this->db->or_like('fecha', $q);
        $this->db->or_like('hora', $q);
        $this->db->or_like('activo', $q);
        $this->db->or_like('Informacion', $q);
        $this->db->or_like('Imagen', $q);
        $this->db->or_like('Estadio', $q);
        $this->db->or_like('Categoria', $q);
        $this->db->or_like('Subcategoria', $q);
        $this->db->or_like('Empresa', $q);
        $this->db->from($this->table);
        
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('nombre', $q);
        $this->db->or_like('direccion', $q);
        $this->db->or_like('ciudad', $q);
        $this->db->or_like('codigo_postal', $q);
        $this->db->or_like('correo', $q);
        $this->db->or_like('telefono', $q);
        $this->db->or_like('fecha', $q);
        $this->db->or_like('hora', $q);
        $this->db->or_like('activo', $q);
        $this->db->or_like('Informacion', $q);
        $this->db->or_like('Imagen', $q);
        $this->db->or_like('Estadio', $q);
        $this->db->or_like('Categoria', $q);
        $this->db->or_like('Subcategoria', $q);
        $this->db->or_like('Empresa', $q);
        $this->db->limit($limit, $start);
        
        return $this->db->get($this->table)->result();
    }

    // get data with limit and search
    function MisEventos($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('Promotor', $q);
	    $this->db->limit($limit, $start);
        
        return $this->db->get($this->table)->result();
    }
}