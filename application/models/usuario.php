<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuario extends CI_Model
{

    public $table = 'usuario';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function findAll() {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    function findById($id) {
        $this->db->where('id', $id);
        
        return $this->db->get($this->table)->row();
    }

    function findByEmail($email) {
        $this->db->where('email', $email);
        
        return $this->db->get($this->table)->row();
    }

    function login($email, $password) {
        $this->db->where('email', $email);
        $this->db->where('password', $password);

        return $this->db->get($this->table)->row();
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}