<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CarritoProducto extends CI_Model
{

    public $table = 'carrito_producto';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function findAll() {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    function findAllByIdCarrito($id) {
        $this->db->order_by($this->id, $this->order);
        $this->db->where('carrito_id', $id);

        return $this->db->get($this->table)->result();
    }

    function findByIdProductoAndCarrito($producto_id, $carrito_id) {
        
        $this->db->where('carrito_id', $carrito_id);
        $this->db->where('producto_id', $producto_id);        

        return $this->db->get($this->table)->row();
    }

    function findById($id) {
        $this->db->where($this->id, $id);
        
        return $this->db->get($this->table)->row();
    }

    function findByIdUser($id) {
        $this->db->where('usuario_id', $id);
        
        return $this->db->get($this->table)->row();
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}