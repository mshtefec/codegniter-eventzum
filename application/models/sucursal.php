<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sucursal extends CI_Model
{

    public $table = 'sucursal';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function findAll() {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    function findAllByIdUser($id) {
        $this->db->where('usuario_id', $id);

        return $this->db->get($this->table)->result();
    }

    function findAllByTienda() {
        $this->db->select("sucursal.id AS id, CONCAT('Tienda: ', tienda.nombre, ' - ', sucursal.ciudad, ' ', sucursal.direccion) AS tienda");
        $this->db->from('sucursal');
        $this->db->join('tienda', 'tienda.id = sucursal.tienda_id', 'left');

        $query = $this->db->get();

        return $query->result();
    }

    function findSucursalesByIdEvento($id) {
        $this->db->where('evento_id', $id);
        
        return $this->db->get($this->table)->result();
    }

    function findById($id) {
        $this->db->where($this->id, $id);
        
        return $this->db->get($this->table)->row();
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}