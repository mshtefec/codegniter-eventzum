<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Zonasestadio <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">ZonasEstadioDescripcion <?php echo form_error('ZonasEstadioDescripcion') ?></label>
            <input type="text" class="form-control" name="ZonasEstadioDescripcion" id="ZonasEstadioDescripcion" placeholder="ZonasEstadioDescripcion" value="<?php echo $ZonasEstadioDescripcion; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Capacidad <?php echo form_error('Capacidad') ?></label>
            <input type="text" class="form-control" name="Capacidad" id="Capacidad" placeholder="Capacidad" value="<?php echo $Capacidad; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">AsientoInicial <?php echo form_error('AsientoInicial') ?></label>
            <input type="text" class="form-control" name="AsientoInicial" id="AsientoInicial" placeholder="AsientoInicial" value="<?php echo $AsientoInicial; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">AsientoFinal <?php echo form_error('AsientoFinal') ?></label>
            <input type="text" class="form-control" name="AsientoFinal" id="AsientoFinal" placeholder="AsientoFinal" value="<?php echo $AsientoFinal; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Precio <?php echo form_error('Precio') ?></label>
            <input type="text" class="form-control" name="Precio" id="Precio" placeholder="Precio" value="<?php echo $Precio; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Estadio IdEstadio <?php echo form_error('Estadio_idEstadio') ?></label>
            <input type="text" class="form-control" name="Estadio_idEstadio" id="Estadio_idEstadio" placeholder="Estadio IdEstadio" value="<?php echo $Estadio_idEstadio; ?>" />
        </div>
	    <input type="hidden" name="idZonasEstadio" value="<?php echo $idZonasEstadio; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('zonasestadio') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>