<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Zona List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>ZonasEstadioDescripcion</th>
		<th>Fila</th>
		<th>AsientoInicial</th>
		<th>AsientoFinal</th>
		<th>Precio</th>
		<th>Eventos Id</th>
		
            </tr><?php
            foreach ($zona_data as $zona)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $zona->ZonasEstadioDescripcion ?></td>
		      <td><?php echo $zona->Fila ?></td>
		      <td><?php echo $zona->AsientoInicial ?></td>
		      <td><?php echo $zona->AsientoFinal ?></td>
		      <td><?php echo $zona->Precio ?></td>
		      <td><?php echo $zona->eventos_id ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>