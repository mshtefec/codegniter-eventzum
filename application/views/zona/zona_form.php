<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Zona <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">ZonasEstadioDescripcion <?php echo form_error('ZonasEstadioDescripcion') ?></label>
            <input type="text" class="form-control" name="ZonasEstadioDescripcion" id="ZonasEstadioDescripcion" placeholder="ZonasEstadioDescripcion" value="<?php echo $ZonasEstadioDescripcion; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Fila <?php echo form_error('Fila') ?></label>
            <input type="text" class="form-control" name="Fila" id="Fila" placeholder="Fila" value="<?php echo $Fila; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">AsientoInicial <?php echo form_error('AsientoInicial') ?></label>
            <input type="text" class="form-control" name="AsientoInicial" id="AsientoInicial" placeholder="AsientoInicial" value="<?php echo $AsientoInicial; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">AsientoFinal <?php echo form_error('AsientoFinal') ?></label>
            <input type="text" class="form-control" name="AsientoFinal" id="AsientoFinal" placeholder="AsientoFinal" value="<?php echo $AsientoFinal; ?>" />
        </div>
	    <div class="form-group">
            <label for="decimal">Precio <?php echo form_error('Precio') ?></label>
            <input type="text" class="form-control" name="Precio" id="Precio" placeholder="Precio" value="<?php echo $Precio; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Eventos Id <?php echo form_error('eventos_id') ?></label>
            <input type="text" class="form-control" name="eventos_id" id="eventos_id" placeholder="Eventos Id" value="<?php echo $eventos_id; ?>" />
        </div>
	    <input type="hidden" name="idZonasFila" value="<?php echo $idZonasFila; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('zona') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>