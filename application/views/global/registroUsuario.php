<div id="content-wrapper" >
  <div class="container-fluid ">
    <div class="card card-form mx-auto mt-5 " style="width:75%;  bottom: 3rem;">
      <div class="card-header">Registro Tipo Usuario:</div>
      <div class="card-body ">
        <h2 style="margin-top:0px"></h2>
        <form action="<?php echo $action; ?>" method="post">
          <div class="form-group" style="-webkit-box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);
                                        -moz-box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);
                                        box-shadow: 10px 10px 5px 0px rgba(190,190,204,1);">
            <input name="file" type="file" />
          </div>
          <div id="capa" class="form-group"></div>
      	    <div class="form-group">
              <label for="varchar">Nombre <?php echo form_error('name') ?></label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
            </div>
            <div class="form-group">
              <label for="varchar">Apellidos <?php echo form_error('lastname') ?></label>
              <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Lastname" value="<?php echo $lastname; ?>" />
            </div>
	          <div class="form-group">
              <label for="int">Tel&eacute;fono <?php echo form_error('phone') ?></label>
              <input type="tel" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">Direcci&oacute;n <?php echo form_error('address') ?></label>
              <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $address; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">Ciudad <?php echo form_error('city') ?></label>
              <input type="text" class="form-control" name="city" id="city" placeholder="City" value="<?php echo $city; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">C&oacute;digo Postal <?php echo form_error('zipcode') ?></label>
              <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zipcode" value="<?php echo $zipcode; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">Correo <?php echo form_error('email') ?></label>
              <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">Contrase&ntilde;a <?php echo form_error('password') ?></label>
              <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
            </div>
	          <div class="form-group">
              <label for="int">Cumplea&ntilde;os <?php echo form_error('date') ?></label>
              <input type="date" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
            </div>
	          <div class="form-group">
              <label for="varchar">Tipo <?php echo form_error('tipo') ?></label>
              <select name="tipo" size="1" class="form-control" name="tipo" id="tipo" placeholder="tipo" >
                <option value="Usuario" <?php if ($tipo=="Usuario"){echo "selected='selected'";} ?> >Usuario</option>
              </select>
            </div>
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
	          <button type="submit" class="btn btn-primary"><?php echo "Aceptar"//$button ?></button>
	          <a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancelar</a>
	      </form>
      </div>
    </div>
  </div>
</div>
