    <div id="content-wrapper">

        <div class="container-fluid">



      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Inicio de sesi&oacute;n :</div>
        <div class="card-body">
          
          

     <form name="login" action="fichaLogueo" method="post">
              <div class='form-group'>
                 <div class="form-label-group">
                   <label   for="email"></label><!--DEBE SER EL CORREO-->
                   <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required="required" autofocus="autofocus">
                  </div> 
              </div>
              <div class='form-group'>
                  <div class="form-label-group">
                   <label   for="password"></label>
                   <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
                   </div> 
              </div>

               <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me">
                 Recordar la contrase&ntilde;a
                </label>
              </div>
            </div>

             <input class="btn btn-primary btn-block" name="loguin" type="submit" value="Aceptar">
     </form>

     <div class="text-center">                                           
            <a class="d-block small mt-3" href="<?= site_url('Users/create') ;?>">Registre su cuenta</a>
            <a class="d-block small" href="forgot-password.html">¿No recuerda la contrase&ntilde;a?</a>
          </div>

             </div>
           </div>
         </div>

        </div>
        <!-- /.container-fluid -->