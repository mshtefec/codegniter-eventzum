<div id="content-wrapper">
  <div class="container-fluid">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Seleccione el tipo de usuario:</div>
      <div class="card-body">
        <a type="button" href="<?= site_url('PantInicio/registroUsuario') ;?>" class="btn btn-primary btn-lg btn-block">Usuario</a>
        <a type="button" href="<?= site_url('PantInicio/registroPromotor') ;?>" class="btn btn-secondary btn-lg btn-block">Promotor</a>
      </div>
    </div>
  </div>
</div>