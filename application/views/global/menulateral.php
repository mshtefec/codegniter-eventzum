<div id="wrapper">
<ul class="sidebar navbar-nav">
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-fw fa-folder"></i>
      <span>Inicio</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <!-- <h6 class="dropdown-header">Provando :</h6>  -->
      <a class="dropdown-item" style="color: #909294" href="<?= site_url('home') ;?>">Home</a>
      <a class="dropdown-item" style="color: #909294" href="<?= site_url('login') ;?>">Inicio de sesi&oacute;n</a>
      <a class="dropdown-item" style="color: #909294" href="<?= site_url('registro') ;?>">Registrarse</a>
      <a class="dropdown-item" style="color: #909294" href="<?= site_url('recupero') ;?>">Perdi&oacute; la contrase&ntilde;a</a>
    </div>
  </li>
  
  <?php
  
  if ($this->session->userdata()) {
    $s = $this->session->userdata();
    
    if((isset ($s['role'])) and ($s['role'] == 2)) {
  ?>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Promotor</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Inicio :</h6>
        <a class="dropdown-item" href="<?= site_url('login') ;?>">Inicio de sesi&oacute;n</a>
        <a class="dropdown-item" href="#forgot-password.html">Perdi&oacute; la contrase&ntilde;a</a>
        <a class="dropdown-item" href="<?= site_url('Users/ModifDatos/'.$s['Id_Usuario'] );?>">Modificar datos</a>
        <a class="dropdown-item" href="<?= site_url('Users/ModifPasword/'.$s['Id_Usuario'] ) ;?>">Cambiar la contrase&ntilde;a</a>
        <a class="dropdown-item" href="<?= site_url('PantInicio/CerrarSession') ;?>">Cerrar sesi&oacute;n</a>
        <div class="dropdown-divider"></div>
        <div class="dropdown-divider"></div>
        <h6 class="dropdown-header">Tablas :</h6>
        <a class="dropdown-item" href="<?= site_url('Eventos/MisEventos/'.$s['Id_Usuario']) ;?>">Mis eventos</a>
        <a class="dropdown-item" href="<?= site_url('Eventos_categorias') ;?>">Categor&iacute;as de eventos</a>
        <a class="dropdown-item" href="<?= site_url('Eventos_subcategorias') ;?>"> Subcategor&iacute;as de eventos</a>
        <a class="dropdown-item" href="<?= site_url('Negocios') ;?>">Mis tiendas</a>
        <a class="dropdown-item" href="<?= site_url('Negocios_categorias') ;?>">Categor&iacute;as de tiendas</a>
        <a class="dropdown-item" href="<?= site_url('Negocios_subcategorias') ;?>">Subcategor&iacute;as de tiendas</a>
        <a class="dropdown-item" href="<?= site_url('Productos') ;?>">Mis productos</a>
        <a class="dropdown-item" href="<?= site_url('Producto_categoria') ;?>">Categor&iacute;as de productos</a>
        <a class="dropdown-item" href="<?= site_url('Producto_subcategoria') ;?>">Subcategor&iacute;as de productos</a>
        <a class="dropdown-item" href="<?= site_url('Estadio') ;?>">Estadios</a>
        <a class="dropdown-item" href="<?= site_url('Empresas/create') ;?>">Empresas</a>
      </div>
    </li>
  <?php  }   } ?>
  <?php
    if ($this->session->userdata('usuario')) {
      $s = $this->session->userdata('usuario');
      if((isset ($s['Tipo'])) and ($s['Tipo']=='Usuario')) {
  ?>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Usuario</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Inicio :</h6>
        <a class="dropdown-item" href="#forgot-password.html">Perdi&oacute; la contrase&ntilde;a</a>
        <a class="dropdown-item" href="<?= site_url('Users/ModifDatos/'.$s['Id_Usuario'] );?>">Modificar datos    </a>
        <a class="dropdown-item" href="<?= site_url('Users/ModifPasword/'.$s['Id_Usuario'] ) ;?>">Cambiar la contrase&ntilde;a</a>
        <a class="dropdown-item" href="/user/profile"> Mi cuenta</a>
        <a class="dropdown-item" href="<?= site_url('PantInicio/CerrarSession') ;?>">Cerrar sesi&oacute;n</a>
        <div class="dropdown-divider"></div>
      </div>
    </li>
  <?php }  } ?>
  <?php
    if ($this->session->userdata('usuario')) {
      $s = $this->session->userdata('usuario');
      if((isset ($s['Tipo'])) and ($s['Tipo']=='Administrador')) {
  ?>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Administrador</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Inicio :</h6>
        <a class="dropdown-item" href="#forgot-password.html">Perdi&oacute; la contrase&ntilde;a</a>
        <a class="dropdown-item" href="<?= site_url('UsersAdm/ModifDatos/'.$s['Id_Usuario'] );?>">Modificar datos    </a>
        <a class="dropdown-item" href="<?= site_url('UsersAdm/ModifPasword/'.$s['Id_Usuario'] ) ;?>">Cambiar la contrase&ntilde;a</a>
        <a class="dropdown-item" href="usersAdm/"> Usuarios</a>
        <a class="dropdown-item" href="<?= site_url('PantInicio/CerrarSession') ;?>">Cerrar sesi&oacute;n</a>
        <div class="dropdown-divider"></div>
        <h6 class="dropdown-header">Tablas :</h6>
        <a class="dropdown-item" href="<?= site_url('Eventos_categorias') ;?>">Categor&iacute;as de eventos</a>
        <a class="dropdown-item" href="<?= site_url('Eventos_subcategorias') ;?>"> Subcategor&iacute;a de eventos</a>
        <a class="dropdown-item" href="<?= site_url('Estadio') ;?>">Estadios</a>
        <a class="dropdown-item" href="<?= site_url('Empresas/create') ;?>">Empresas</a>
      </div>
    </li>
  <?php } } ?>
</ul>
