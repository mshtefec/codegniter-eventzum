
<!DOCTYPE html>
<html lang="es">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom fonts for this template-->
    <link href="<?= base_url();?>assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<?= base_url();?>assets/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url();?>assets/css/sb-admin.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/TabsEspeciales.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/CajasConSombras.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/dropzone.css" rel="stylesheet">     

    <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>

   


  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="<?= site_url('PantInicio') ;?>">Inicio</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>
    <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">


    </div>


      <ul class="navbar-nav .col- 12 ml-auto mr-0 mr-md-3 my-2 my-md-0  ">

         <?php if ($this->session->userdata('usuario')) {
              $s=   $this->session->userdata('usuario');
          ?>  <!-- verificando session activa -->
           <li class="nav-item dropdown">
           <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <img   src="<?php echo  $s['avatar']; ?>" width="80px" height="80px" alt="" class="rounded-circle shadow  bg-white  "  >
           </a>
           <div class="dropdown-menu" aria-labelledby="pagesDropdown" style="  font-size: 10px ">
           <!-- <h6 class="dropdown-header">Provando :</h6>  -->
           <?php echo $s['Nombre'] ?>    <br>
           <?php echo $s['Usuario'] ; ?>      <br>
            <a class="dropdown-item" style="color: #909294" href="<?= site_url('login') ;?>">Modificar datos</a>
            <a class="dropdown-item" style="color: #909294" href="<?= site_url('Users/create_action') ;?>">Cambiar contrase&ntilde;a</a>
            <a class="dropdown-item" style="color: #909294" href="<?= site_url('recupero') ;?>">Cerrar sesi&oacute;n</a>
            <a href="#" onclick="alert('<?php echo  $s['avatar']; ?>')">imagen</a>
           </div>
           </li>
            <?php } else{
            ?>


        <i><a class="navbar-brand mr-1" href="<?= site_url('login') ;?>">Iniciar sesi&oacute;n :</a> </i>
          <?php }
            ?>

      </ul>

    </nav>