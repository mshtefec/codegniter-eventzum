<div id="content-wrapper">
        <div class="container-fluid">
 <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<   cabecera del contenido >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
        <h2 style="margin-top:0px">Datos de pago empresas <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Empresa <?php echo form_error('empresa') ?></label>
            <input type="numeric" class="form-control" name="empresa" id="empresa" placeholder="Empresa" readonly="readonly" value="<?php echo $empresa; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Cuenta <?php echo form_error('cuenta') ?></label>
            <input type="text" class="form-control" name="cuenta" id="cuenta" placeholder="Cuenta" value="<?php echo $cuenta; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Banco <?php echo form_error('banco') ?></label>
            <input type="text" class="form-control" name="banco" id="banco" placeholder="Banco" value="<?php echo $banco; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo "Aceptar"//$button ?></button> 
	    <a href="<?php echo site_url('datos_de_pago_empresas') ?>" class="btn btn-default">Cancelar</a>
	</form>
 <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<< pie del contneido>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

        </div>
        <!-- /.container-fluid -->