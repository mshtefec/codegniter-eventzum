<div id="content-wrapper">
        <div class="container-fluid">
 <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<   cabecera del contenido >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
        <h2 style="margin-top:0px">Lista de datos de pago de empresas </h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('datos_de_pago_empresas/create'),'<i class="far fa-plus-square fa-2x"></i>', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('datos_de_pago_empresas/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('datos_de_pago_empresas'); ?>" class="btn btn-default">Reiniciar</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
        <th>No</th>
		<th>Empresa</th>
		<th>Cuenta</th>
		<th>Banco</th>
		<th>Acci&oacute;n</th>
            </tr><?php
            foreach ($datos_de_pago_empresas_data as $datos_de_pago_empresas)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $datos_de_pago_empresas->empresa ?></td>
			<td><?php echo $datos_de_pago_empresas->cuenta ?></td>
			<td><?php echo $datos_de_pago_empresas->banco ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('datos_de_pago_empresas/read/'.$datos_de_pago_empresas->id),'<i class="far fa-eye"></i> ');
				echo ' | ';
				echo anchor(site_url('datos_de_pago_empresas/update/'.$datos_de_pago_empresas->id),'<i class="far fa-edit"></i>');
				echo ' | ';
				echo anchor(site_url('datos_de_pago_empresas/delete/'.$datos_de_pago_empresas->id),'<i class="far fa-trash-alt"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Registros : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('datos_de_pago_empresas/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('datos_de_pago_empresas/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
<!--<<<<<<<<<<<<<<<<<<<<<<<<<<<< pie del contneido>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

        </div>
        <!-- /.container-fluid -->