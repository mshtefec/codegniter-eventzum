﻿# Host: localhost  (Version 5.5.5-10.1.26-MariaDB)
# Date: 2019-05-26 16:42:06
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "asientos"
#

DROP TABLE IF EXISTS `asientos`;
CREATE TABLE `asientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zona` varchar(45) DEFAULT NULL,
  `precio` decimal(6,2) DEFAULT NULL,
  `Fila` int(11) DEFAULT NULL,
  `Asiento` int(11) DEFAULT NULL,
  `Disponible` tinyint(1) DEFAULT NULL,
  `idEvento` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=907 DEFAULT CHARSET=latin1;

#
# Data for table "asientos"
#

INSERT INTO `asientos` VALUES (832,'Norte',34.00,1,1,1,12),(833,'Norte',34.00,1,2,0,12),(834,'Norte',34.00,1,3,0,12),(835,'Norte',34.00,1,4,1,12),(836,'Norte',34.00,1,5,0,12),(837,'Norte',34.00,1,6,1,12),(838,'Norte',34.00,1,7,1,12),(839,'Norte',34.00,1,8,1,12),(840,'Norte',34.00,1,9,1,12),(841,'Norte',34.00,1,10,1,12),(842,'Norte',34.00,1,11,1,12),(843,'Norte',34.00,1,12,1,12),(844,'Norte',34.00,1,13,1,12),(845,'Norte',34.00,1,14,1,12),(846,'Norte',34.00,1,15,1,12),(847,'Norte',34.00,1,16,1,12),(848,'Norte',34.00,1,17,1,12),(849,'Norte',34.00,1,18,1,12),(850,'Norte',34.00,1,19,1,12),(851,'Norte',34.00,1,20,1,12),(852,'Norte',34.00,1,21,1,12),(853,'Norte',34.00,1,22,1,12),(854,'Norte',34.00,1,23,1,12),(855,'Norte',34.00,1,24,1,12),(856,'Norte',34.00,1,25,1,12),(857,'Norte',34.00,1,26,1,12),(858,'Norte',34.00,1,27,1,12),(859,'Norte',34.00,1,28,1,12),(860,'Norte',34.00,1,29,1,12),(861,'Norte',34.00,2,31,1,12),(862,'Norte',34.00,2,32,1,12),(863,'Norte',34.00,2,33,1,12),(864,'Norte',34.00,2,34,1,12),(865,'Norte',34.00,2,35,1,12),(866,'Norte',34.00,2,36,1,12),(867,'Norte',34.00,2,37,1,12),(868,'Norte',34.00,2,38,1,12),(869,'Norte',34.00,2,39,1,12),(870,'Norte',34.00,3,41,1,12),(871,'Norte',34.00,3,42,1,12),(872,'Norte',34.00,3,43,1,12),(873,'Norte',34.00,3,44,1,12),(874,'Norte',34.00,3,45,1,12),(875,'Norte',34.00,3,46,1,12),(876,'Norte',34.00,3,47,1,12),(877,'Norte',34.00,3,48,1,12),(878,'Norte',34.00,3,49,1,12),(879,'Sur',32.00,1,1,1,12),(880,'Sur',32.00,1,2,0,12),(881,'Sur',32.00,1,3,0,12),(882,'Sur',32.00,1,4,1,12),(883,'Sur',32.00,1,5,1,12),(884,'Sur',32.00,1,6,1,12),(885,'Sur',32.00,1,7,1,12),(886,'Sur',32.00,1,8,1,12),(887,'Sur',32.00,1,9,1,12),(888,'Sur',32.00,1,10,1,12),(889,'Sur',32.00,1,11,1,12),(890,'Sur',32.00,1,12,1,12),(891,'Sur',32.00,1,13,1,12),(892,'Sur',32.00,1,14,1,12),(893,'Sur',32.00,1,15,1,12),(894,'Sur',32.00,1,16,1,12),(895,'Sur',32.00,1,17,1,12),(896,'Sur',32.00,1,18,1,12),(897,'Sur',32.00,1,19,1,12),(898,'Sur',32.00,3,31,1,12),(899,'Sur',32.00,3,32,1,12),(900,'Sur',32.00,3,33,1,12),(901,'Sur',32.00,3,34,1,12),(902,'Sur',32.00,3,35,1,12),(903,'Sur',32.00,3,36,1,12),(904,'Sur',32.00,3,37,1,12),(905,'Sur',32.00,3,38,1,12),(906,'Sur',32.00,3,39,1,12),(907,'Norte',66.00,1,1,1,1),(908,'Norte',66.00,1,2,1,1),(909,'Norte',66.00,1,3,1,1),(910,'Norte',66.00,1,4,1,1),(911,'Norte',66.00,1,5,1,1),(912,'Norte',66.00,1,6,1,1),(913,'Norte',66.00,1,7,1,1),(914,'Norte',66.00,1,8,1,1),(915,'Norte',66.00,1,9,1,1),(916,'Norte',66.00,1,10,1,1),(917,'Norte',66.00,1,11,1,1),(918,'Norte',66.00,1,12,1,1),(919,'Norte',66.00,1,13,1,1),(920,'Norte',66.00,1,14,1,1),(921,'Norte',66.00,1,15,1,1),(922,'Norte',66.00,1,16,1,1),(923,'Norte',66.00,1,17,1,1),(924,'Norte',66.00,1,18,1,1),(925,'Norte',66.00,1,19,1,1),(926,'Norte',66.00,1,20,1,1),(927,'Norte',66.00,1,21,1,1),(928,'Norte',66.00,1,22,1,1),(929,'Norte',66.00,1,23,1,1),(930,'Norte',66.00,1,24,1,1),(931,'Norte',66.00,1,25,1,1),(932,'Norte',66.00,1,26,1,1),(933,'Norte',66.00,1,27,1,1),(934,'Norte',66.00,1,28,1,1),(935,'Norte',66.00,1,29,1,1),(936,'Norte',66.00,2,31,1,1),(937,'Norte',66.00,2,32,1,1),(938,'Norte',66.00,2,33,1,1),(939,'Norte',66.00,2,34,1,1),(940,'Norte',66.00,2,35,1,1),(941,'Norte',66.00,2,36,1,1),(942,'Norte',66.00,2,37,1,1),(943,'Norte',66.00,2,38,1,1),(944,'Norte',66.00,2,39,1,1),(945,'Norte',66.00,3,41,1,1),(946,'Norte',66.00,3,42,1,1),(947,'Norte',66.00,3,43,1,1),(948,'Norte',66.00,3,44,1,1),(949,'Norte',66.00,3,45,1,1),(950,'Norte',66.00,3,46,1,1),(951,'Norte',66.00,3,47,1,1),(952,'Norte',66.00,3,48,1,1),(953,'Norte',66.00,3,49,1,1),(954,'Sur',77.00,1,1,1,1),(955,'Sur',77.00,1,2,1,1),(956,'Sur',77.00,1,3,1,1),(957,'Sur',77.00,1,4,1,1),(958,'Sur',77.00,1,5,1,1),(959,'Sur',77.00,1,6,1,1),(960,'Sur',77.00,1,7,1,1),(961,'Sur',77.00,1,8,1,1),(962,'Sur',77.00,1,9,1,1),(963,'Sur',77.00,1,10,1,1),(964,'Sur',77.00,1,11,1,1),(965,'Sur',77.00,1,12,1,1),(966,'Sur',77.00,1,13,1,1),(967,'Sur',77.00,1,14,1,1),(968,'Sur',77.00,1,15,1,1),(969,'Sur',77.00,1,16,1,1),(970,'Sur',77.00,1,17,1,1),(971,'Sur',77.00,1,18,1,1),(972,'Sur',77.00,1,19,1,1),(973,'Sur',77.00,3,31,1,1),(974,'Sur',77.00,3,32,1,1),(975,'Sur',77.00,3,33,1,1),(976,'Sur',77.00,3,34,1,1),(977,'Sur',77.00,3,35,1,1),(978,'Sur',77.00,3,36,1,1),(979,'Sur',77.00,3,37,1,1),(980,'Sur',77.00,3,38,1,1),(981,'Sur',77.00,3,39,1,1);

#
# Structure for table "carrito"
#

DROP TABLE IF EXISTS `carrito`;
CREATE TABLE `carrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT '0',
  `total` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "carrito"
#

INSERT INTO `carrito` VALUES (1,1,0.00),(2,2,0.00),(3,3,0.00);

#
# Structure for table "carrito_evento"
#

DROP TABLE IF EXISTS `carrito_evento`;
CREATE TABLE `carrito_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrito_id` int(11) DEFAULT '0',
  `evento_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

#
# Data for table "carrito_evento"
#

INSERT INTO `carrito_evento` VALUES (6,NULL,9),(8,1,8),(9,1,7),(10,1,9),(11,NULL,2);

#
# Structure for table "carrito_producto"
#

DROP TABLE IF EXISTS `carrito_producto`;
CREATE TABLE `carrito_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carrito_id` int(11) DEFAULT '0',
  `producto_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "carrito_producto"
#

INSERT INTO `carrito_producto` VALUES (1,1,1),(2,1,2),(3,1,1);

#
# Structure for table "datos_de_pago_empresas"
#

DROP TABLE IF EXISTS `datos_de_pago_empresas`;
CREATE TABLE `datos_de_pago_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` int(11) NOT NULL,
  `cuenta` varchar(128) NOT NULL,
  `banco` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "datos_de_pago_empresas"
#

INSERT INTO `datos_de_pago_empresas` VALUES (1,4,'8888889999','SANTANDER RIO'),(2,4,'877779999','COMAFI'),(3,5,'8888889999','banco nacion');

#
# Structure for table "empresa"
#

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `nif` varchar(9) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `correo` varchar(256) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `logo_file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "empresa"
#

INSERT INTO `empresa` VALUES (1,2,'001','Wayne Enterprice','Gotham','3624010101','wayne@enterprice.com',1,'uploads/empresas/logo_2.png','logo_2.png');

#
# Structure for table "entradas"
#

DROP TABLE IF EXISTS `entradas`;
CREATE TABLE `entradas` (
  `idAsientos` int(11) NOT NULL AUTO_INCREMENT,
  `Zona` varchar(45) DEFAULT NULL,
  `Fila` int(11) DEFAULT NULL,
  `NroAsiento` int(11) DEFAULT NULL,
  `Compras_id` int(11) NOT NULL,
  PRIMARY KEY (`idAsientos`),
  KEY `fk_Asientos_copy1_Compras1_idx` (`Compras_id`),
  CONSTRAINT `fk_Asientos_copy1_Compras1` FOREIGN KEY (`Compras_id`) REFERENCES `comprasentradas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "entradas"
#


#
# Structure for table "entradasvendidas"
#

DROP TABLE IF EXISTS `entradasvendidas`;
CREATE TABLE `entradasvendidas` (
  `idEntradasVendidas` int(11) NOT NULL,
  `NroAsiento` int(11) DEFAULT NULL,
  `EventStockEntradas_idZonasEstadio` int(11) NOT NULL,
  PRIMARY KEY (`idEntradasVendidas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "entradasvendidas"
#


#
# Structure for table "estadio"
#

DROP TABLE IF EXISTS `estadio`;
CREATE TABLE `estadio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `latitud` decimal(9,6) DEFAULT NULL,
  `longitud` decimal(9,6) DEFAULT NULL,
  `informacion` text,
  `plano_url` varchar(255) DEFAULT NULL,
  `plano_file` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "estadio"
#

INSERT INTO `estadio` VALUES (1,'Estadio de River',-27.483195,-59.009957,'asdfasdf asdfasdf asdfasd','uploads/estadios/planos/plano_1.jpg','plano_1.jpg',0),(2,'Estadio de Boca',0.000000,0.000000,'asdfasdfasdf','uploads/estadios/planos/plano_2.jpg','plano_2.jpg',0),(3,'Maracana',1.000000,1.000000,'1','uploads/estadios/planos/plano_Maracana.png','plano_Maracana.png',0);

#
# Structure for table "evento"
#

DROP TABLE IF EXISTS `evento`;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `categoria_id` int(11) NOT NULL DEFAULT '0',
  `subcategoria_id` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(64) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `direccion` varchar(256) NOT NULL,
  `ciudad` varchar(64) NOT NULL,
  `codigo_postal` varchar(10) NOT NULL,
  `correo` varchar(256) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `Informacion` text,
  `Estadio` varchar(45) NOT NULL,
  `Empresa` varchar(45) NOT NULL,
  `imagen_url` varchar(255) DEFAULT NULL,
  `imagen_file` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Data for table "evento"
#

INSERT INTO `evento` VALUES (1,4,8,85,'Pelea Tyson vs De la Holla',NULL,'Las Piedras 333','Posadas','2222','laspiedras@correo.com','3423423','2019-02-13','02:00:12','asdfasdfasdf','1','','uploads/eventos/evento_4.jpg','evento_4.jpg',1),(2,0,0,0,'Recital Ochentoso',NULL,'sauces 400','Resistencia','3500','fiesta@fiesta.com','23234234','2019-02-07','20:45:00','Buena noche para vilar y divertirse','2','2','uploads/eventos/guitarra.jpg','guitarra.jpg',1),(3,0,0,0,'musical',NULL,'FRONDICI 2334','Posadas','2222','laspiedras@correo.com','3423423','2019-02-21','13:03:00','Musical de rock nacional','Estadio de River','ACME','uploads/eventos/guitarra2.jpg','guitarra2.jpg',1),(4,0,0,0,'musical',NULL,'FRONDICI 2334','Resistencia','2222','fiesta@fiesta.com','23234234','2019-12-10','22:12:00','askdjflaskdf','Estadio de River','ACME','uploads/eventos/img000.jpg','img000.jpg',1),(5,0,0,0,'River Vs Banfield',NULL,'los alamos 444','Posadas','8884','laspiedras@correo.com','23234234','2019-03-15','01:04:00','un partido en la cancha de 11','Estadio de Boca','ACME','uploads/eventos/img003.jpg','img003.jpg',1),(6,0,0,0,'Boca River',NULL,'los alamos 444','Posadas','8884','laspiedras@correo.com','23234234','2019-03-15','01:04:00','un partido en la cancha de 11','Estadio de Boca','ACME','uploads/eventos/img012.jpg','img012.jpg',1),(7,0,0,0,'Concierto',NULL,'Sabedra 555','Córdoba','2222','fiesta@fiesta.com','23234234','2019-03-15','12:00:00','La Orquesta Philarmonica de Londres en una noche gala','Estadio de River','ACME','uploads/eventos/concierto.jpg','concierto.jpg',1),(8,0,0,0,'Bob Dylan en concierto',NULL,'Leopoldo Morew 444','Cordoba','324234','laspiedras@correo.com','44444','2019-03-13','12:00:00',NULL,'Estadio de Boca','ACME','uploads/eventos/bob_dylan.jpg','bob_dylan.jpg',1),(9,2,6,61,'Fundacion Escuela Teresiana',NULL,'Ciudad de Gotham','Gotham','3500','wayne@enterprice.com','3624010101','0000-00-00','13:00:00',NULL,'','','uploads/eventos/evento_2.jpg','evento_2.jpg',1),(10,4,8,85,'Super Pelea','el nuevo evento','zfgsdgfsdgf','dfgsdfgsd','343456','fsdfg@dfghf.com','3563456','2019-05-20','00:00:12','zdvcfsdf','2','','uploads/eventos/evento_4.','evento_4.',1),(11,4,5,85,'KKJKLJLK',NULL,'NJKNJKNK','RESISTENCIA','3500','adsjkf@hotmail.com','155616','2019-05-24','10:12:00',NULL,'1','','uploads/eventos/evento_4.jpg','evento_4.jpg',1),(12,4,5,22,'asdfasdf',NULL,'asdfasdf','asdfasd','23234','asdf@afda.com','234234','2019-05-19','12:00:00',NULL,'1','','uploads/eventos/evento_4.jpg','evento_4.jpg',1);

#
# Structure for table "evento_categoria"
#

DROP TABLE IF EXISTS `evento_categoria`;
CREATE TABLE `evento_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

#
# Data for table "evento_categoria"
#

INSERT INTO `evento_categoria` VALUES (1,'culturales',1),(2,'deportivos',1),(3,'artisticos',1),(4,'moda',1),(5,'informatica',1),(6,'politica',1),(7,'religion',1),(8,'musical',1);

#
# Structure for table "evento_participante"
#

DROP TABLE IF EXISTS `evento_participante`;
CREATE TABLE `evento_participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

#
# Data for table "evento_participante"
#

INSERT INTO `evento_participante` VALUES (1,'MIKE TYSON',5),(2,'Isabel Pantoja',6),(3,'EVANDER HOLIFIELD',5),(4,'Cacho Tirao',7),(5,'Bob Dylan',12),(6,'Ana Lucero',7);

#
# Structure for table "evento_subcategoria"
#

DROP TABLE IF EXISTS `evento_subcategoria`;
CREATE TABLE `evento_subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

#
# Data for table "evento_subcategoria"
#

INSERT INTO `evento_subcategoria` VALUES (20,'futbol',1),(21,'basquet',1),(22,'tenis',1),(50,'hardware',1),(51,'software',1),(60,'socialismo',1),(61,'radicalismo',1),(70,'cristianismo',1),(71,'catolisismo',1),(72,'ateismo',1),(73,'budaismo',1),(80,'blues',1),(81,'country',1),(82,'pop',1),(83,'metal',1),(84,'funk',1),(85,'hip hop',1);

#
# Structure for table "eventozona"
#

DROP TABLE IF EXISTS `eventozona`;
CREATE TABLE `eventozona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Precio` decimal(6,2) DEFAULT NULL,
  `idEvento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

#
# Data for table "eventozona"
#

INSERT INTO `eventozona` VALUES (47,'Norte',34.00,12),(48,'Sur',32.00,12),(49,'Norte',66.00,1),(50,'Sur',77.00,1);

#
# Structure for table "items"
#

DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sold` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `vip` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "items"
#


#
# Structure for table "perfil"
#

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT '0',
  `nombre` varchar(32) NOT NULL,
  `apellido` varchar(32) NOT NULL,
  `pais` varchar(32) NOT NULL DEFAULT '',
  `telefono` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `fecha_nac` varchar(255) DEFAULT NULL,
  `profesion` varchar(255) DEFAULT NULL,
  `trabajo` varchar(255) DEFAULT NULL,
  `avatar_url` varchar(255) DEFAULT NULL,
  `avatar_file` varchar(255) DEFAULT NULL,
  `web_facebook` varchar(255) DEFAULT NULL,
  `web_instagram` varchar(255) DEFAULT NULL,
  `web_behance` varchar(255) DEFAULT NULL,
  `web_dribbble` varchar(255) DEFAULT NULL,
  `web_linkedIn` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "perfil"
#

INSERT INTO `perfil` VALUES (1,1,'Artur','Fleck','Gotham','3624010101',NULL,NULL,NULL,NULL,'uploads/config/perfiles/perfil_1.jpg','perfil_1.jpg',NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'Bruce','Wayne','Gotham','3624010101',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,4,'Pato','Maidana','Argentina','23423423',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "producto"
#

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(32) NOT NULL,
  `descripcion` varchar(128) NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `iva` decimal(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `Celiaco` tinyint(1) NOT NULL,
  `Vegetariano` tinyint(1) DEFAULT NULL,
  `imagen_url` varchar(255) DEFAULT NULL,
  `imagen_file` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "producto"
#

INSERT INTO `producto` VALUES (1,1,2,3,'Pepitos','Masitas',25.00,0.00,12,0,NULL,'uploads/productos/pepitos.jpg','pepitos.jpg',1),(2,1,2,1,'Coca-Cola','1 1/2 lts',85.00,0.00,3,0,NULL,'uploads/productos/cocacola.jpg','cocacola.jpg',1);

#
# Structure for table "producto_categoria"
#

DROP TABLE IF EXISTS `producto_categoria`;
CREATE TABLE `producto_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "producto_categoria"
#

INSERT INTO `producto_categoria` VALUES (1,'Comestibles',1),(2,'Bebibles',1);

#
# Structure for table "producto_subcategoria"
#

DROP TABLE IF EXISTS `producto_subcategoria`;
CREATE TABLE `producto_subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "producto_subcategoria"
#

INSERT INTO `producto_subcategoria` VALUES (1,'Dulces',1),(2,'Salados',1),(3,'Amargo',1);

#
# Structure for table "role"
#

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "role"
#

INSERT INTO `role` VALUES (1,'usuario',1),(2,'promotor',1),(3,'expendedor',1),(4,'gestor',1),(5,'portero',1),(6,'superadmin',1);

#
# Structure for table "sucursal"
#

DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `tienda_id` int(11) DEFAULT '0',
  `evento_id` int(11) DEFAULT '0',
  `representante` varchar(64) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `ciudad` varchar(64) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "sucursal"
#

INSERT INTO `sucursal` VALUES (1,2,1,9,'Bruce Wayne','Estadio TAL','Gotham',1);

#
# Structure for table "tienda"
#

DROP TABLE IF EXISTS `tienda`;
CREATE TABLE `tienda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(128) NOT NULL,
  `NIF` varchar(9) NOT NULL,
  `representante` varchar(64) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `Telefono` varchar(12) NOT NULL,
  `Contrasenia` varchar(60) NOT NULL,
  `Fecha` date NOT NULL,
  `Evento` int(11) NOT NULL,
  `direccion` varchar(256) NOT NULL,
  `CodigoPostal` varchar(10) NOT NULL,
  `ciudad` varchar(64) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "tienda"
#

INSERT INTO `tienda` VALUES (1,2,'Wayne Online','','Bruce Wayne','','','','0000-00-00',0,'','','',1);

#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role_id` int(11) DEFAULT '1',
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'joker','joker@gmail.com','9facbf452def2d7efc5b5c48cdb837fa',1,1),(2,'batman','batman@gmail.com','ec0e2603172c73a8b644bb9456c1ff6e',2,1),(3,'superman','superman@gmail.com','84d961568a65073a3bcf0eb216b2a576',6,1),(4,'webmaster','webmaster@gmail.com','17c4520f6cfd1ab53d8745e84681eb49',6,1);

#
# Structure for table "zonasestadio"
#

DROP TABLE IF EXISTS `zonasestadio`;
CREATE TABLE `zonasestadio` (
  `idZonasFila` int(11) NOT NULL AUTO_INCREMENT,
  `ZonasEstadioDescripcion` varchar(45) DEFAULT NULL,
  `Fila` varchar(45) DEFAULT NULL,
  `AsientoInicial` int(11) DEFAULT NULL,
  `AsientoFinal` int(11) DEFAULT NULL,
  `Estadio_idEstadio` int(11) NOT NULL,
  PRIMARY KEY (`idZonasFila`),
  KEY `fk_ZonasEstadio_Estadio1_idx` (`Estadio_idEstadio`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

#
# Data for table "zonasestadio"
#

INSERT INTO `zonasestadio` VALUES (166,'Norte','1',1,30,1),(167,'Norte','2',31,40,1),(168,'Norte','3',41,50,1),(169,'Sur','1',1,20,1),(170,'Sur','2',21,30,0),(171,'Sur','3',31,40,1),(172,'centro','1',1,20,2),(173,'centro','2',21,30,2),(174,'centro','3',31,40,2);
